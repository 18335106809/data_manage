package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.RiskSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/11
 */
@Mapper
public interface RiskSourceDao {
    Page<RiskSource> getRiskSource(@Param("capsuleId") Long capsuleId, @Param("deviceType") String deviceType,
                                   @Param("plc") String plc, @Param("riskObject") String riskObject, @Param("responsible") String responsible);
    void insertRiskSource(RiskSource riskSource);
    void updateRiskSource(RiskSource riskSource);
    void deleteRiskSource(Integer id);
}
