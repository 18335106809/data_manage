package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Expert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/2/12
 */
@Mapper
public interface ExpertDao {
    Page<Expert> getAllExpert(@Param("sex") Integer sex, @Param("name") String name,
                              @Param("department") String department,
                              @Param("profession") String profession);
    Expert selectById(Integer id);
    void insertExpert(Expert expert);
    void updateExpert(Expert expert);
    void deleteExpert(Integer id);
}
