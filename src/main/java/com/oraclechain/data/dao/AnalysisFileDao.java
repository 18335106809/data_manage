package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AnalysisFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
@Mapper
public interface AnalysisFileDao {
    Page<AnalysisFile> getFiles(@Param("planName") String planName, @Param("fileName") String fileName,
                                @Param("uploader") String uploader, @Param("auditResult") String auditResult);
    void deleteFile(Integer id);
    void insertFile(AnalysisFile AnalysisFile);
    void updateFile(AnalysisFile AnalysisFile);
    AnalysisFile getById(Integer id);
}
