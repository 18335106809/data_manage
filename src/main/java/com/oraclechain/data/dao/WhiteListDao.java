package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.WhiteList;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@Mapper
public interface WhiteListDao {
    void insert(WhiteList whiteList);
    void update(WhiteList whiteList);
    void delete(Integer id);
    Page<WhiteList> getAll(WhiteList whiteList);
}
