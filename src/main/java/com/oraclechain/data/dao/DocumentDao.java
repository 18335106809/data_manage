package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Document;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/28
 */
@Mapper
public interface DocumentDao {
    Page<Document> getDocuments(@Param("type") Integer type);
    Document getById(Integer id);
    void insertDocument(Document document);
    void updateDocument(Document document);
    void deleteDocument(Integer id);
}
