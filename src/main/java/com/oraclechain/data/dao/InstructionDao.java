package com.oraclechain.data.dao;

import com.oraclechain.data.entity.Instruction;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/28
 */
@Mapper
public interface InstructionDao {
    void insertInstruction(Instruction instruction);
    Integer countTimes(@Param("spaceId") Long spaceId, @Param("capsuleId") Long capsuleId, @Param("deviceType") String deviceType,
                       @Param("startTime") String startTime, @Param("endTime") String endTime);
    void delete(Integer id);
}
