package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Inspection;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/6
 */
@Mapper
public interface InspectionDao {
    void insert(Inspection inspection);
    void update(Inspection inspection);
    void delete(Integer id);
    Page<Inspection> getData(Inspection inspection);
    List<Inspection> getAllData();
}
