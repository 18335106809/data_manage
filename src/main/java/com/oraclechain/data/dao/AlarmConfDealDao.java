package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmConfDeal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
@Mapper
public interface AlarmConfDealDao {
    List<AlarmConfDeal> getAllDeal();
    Page<AlarmConfDeal> getAlarmConfDeal(@Param("eventName") String eventName, @Param("keyWord") String keyWord, @Param("capsuleId") Long capsuleId);
    void insertAlarmConfDeal(AlarmConfDeal alarmConfDeal);
    void insertAlarmDeals(List<AlarmConfDeal> alarmConfDealList);
    void updateAlarmConfDeal(AlarmConfDeal alarmConfDeal);
    void deleteAlarmConfDeal(Integer id);
}
