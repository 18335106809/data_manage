package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.IntrusionRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@Mapper
public interface IntrusionRecordDao {
    void insert(IntrusionRecord intrusionRecord);
    void update(IntrusionRecord intrusionRecord);
    void delete(Integer id);
    Page<IntrusionRecord> getAll(IntrusionRecord intrusionRecord);
}
