package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmConfLevel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
@Mapper
public interface AlarmConfLevelDao {
    AlarmConfLevel getLevelByDealId(Integer dealId);
    Page<AlarmConfLevel> getAlarmConfLevel(@Param("level")Integer level, @Param("alarmEvent")String alarmEvent, @Param("device")String device);
    List<Integer> getAllLevel();
    void insertAlarmConfLevel(AlarmConfLevel alarmConfLevel);
    void updateAlarmConfLevel(AlarmConfLevel alarmConfLevel);
    void deleteAlarmConfLevel(Integer id);
}
