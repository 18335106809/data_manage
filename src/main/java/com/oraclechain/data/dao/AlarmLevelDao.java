package com.oraclechain.data.dao;

import com.oraclechain.data.entity.AlarmLevel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/17
 */
@Mapper
public interface AlarmLevelDao {
    void insert(AlarmLevel alarmLevel);
    void update(AlarmLevel alarmLevel);
    void delete(Integer id);
    List<AlarmLevel> getAll();
}
