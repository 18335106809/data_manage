package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Goods;
import com.oraclechain.data.entity.GoodsLibrary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
@Mapper
public interface GoodsDao {
    Page<Goods> getAllGoods(@Param("type") String type, @Param("libraryId") Integer libraryId, @Param("name") String name);
    Goods getById(Integer id);
    void insertGoods(Goods goods);
    void updateGoods(Goods goods);
    void deleteGoods(Integer id);

    List<GoodsLibrary> getAllGoodsLibrary();
    List<GoodsLibrary> getGoodsLibraryByPid(Integer pid);
    void insertGoodsLibrary(GoodsLibrary goodsLibrary);
    void updateGoodsLibrary(GoodsLibrary goodsLibrary);
    void deleteGoodsLibrary(Integer id);
}
