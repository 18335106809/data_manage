package com.oraclechain.data.dao;

import com.oraclechain.data.entity.AlarmSOP;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/29
 */
@Mapper
public interface AlarmSOPDao {
    void insertSOP(AlarmSOP alarmSOP);
    List<AlarmSOP> getSOPByAlarmId(@Param("alarmId") Integer alarmId);
}
