package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
@Mapper
public interface AlarmInfoDao {
    Page<AlarmInfo> getAllAlarm(@Param("deviceType")String deviceType, @Param("level")Integer level, @Param("alarmType")Integer alarmType, @Param("capsuleId")Long capsuleId,
                                @Param("segmentId")Long segmentId, @Param("startTime")String startTime, @Param("endTime")String endTime);
    AlarmInfo getAlarmById(@Param("id") Integer id);
    List<AlarmInfo> getAlarmByIdList(@Param("ids") List<Integer> ids, @Param("spaceId") Long spaceId,
                                     @Param("capsuleId") Long capsuleId, @Param("deviceType") String deviceType);
    Page<AlarmInfo> getAlarmByDeviceId(Long deviceId);
    Integer setAlarm(AlarmInfo alarm);
    Integer alarmTimes(@Param("deviceId") Long deviceId, @Param("spaceId") Long spaceId, @Param("deviceType") String deviceType,
                       @Param("level")Integer level, @Param("startTime") String startTime, @Param("endTime") String endTime);
    Integer alarmTimesByType(@Param("alarmType") Integer alarmType, @Param("startTime") String startTime, @Param("endTime") String endTime);
    void updateAlarm(AlarmInfo alarmInfo);
}
