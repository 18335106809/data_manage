package com.oraclechain.data.dao;

import com.oraclechain.data.entity.DictionaryData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/16
 */
@Mapper
public interface DictionaryDataDao {
    void insert(DictionaryData dictionaryData);
    void update(DictionaryData dictionaryData);
    void delete(Integer id);
    List<DictionaryData> select(DictionaryData dictionaryData);
}
