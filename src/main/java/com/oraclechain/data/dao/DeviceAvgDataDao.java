package com.oraclechain.data.dao;

import com.oraclechain.data.entity.DeviceDataReport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
@Mapper
public interface DeviceAvgDataDao {
    void insert(DeviceDataReport deviceAvgData);
    List<DeviceDataReport> select(DeviceDataReport deviceAvgData);
    List<DeviceDataReport> getData(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("date") String date,
                                   @Param("spaceId") Long spaceId, @Param("capsuleId") Long capsuleId,
                                   @Param("deviceType") String deviceType, @Param("dataKey") String dataKey);
    Double getDataByDay(@Param("date") String date, @Param("deviceId") Long deviceId, @Param("dataKey") String dataKey);
    List<DeviceDataReport> getDataByDayAndDataKey(@Param("date") String date, @Param("deviceId") Long deviceId, @Param("dataKeys") List<String> dataKeys);
    Double getMaxData(@Param("deviceId") Long deviceId, @Param("date") String date, @Param("dataKey") String dataKey);
}
