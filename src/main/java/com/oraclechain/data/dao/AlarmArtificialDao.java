package com.oraclechain.data.dao;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmArtificial;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/31
 */
@Mapper
public interface AlarmArtificialDao {
    void insert(AlarmArtificial alarmArtificial);
    void update(AlarmArtificial alarmArtificial);
    void delete(Integer id);
    Page<AlarmArtificial> select(AlarmArtificial alarmArtificial);
    Integer getCount(@Param("alarmLevel")Integer alarmLevel, @Param("alarmType") Integer alarmType,
                     @Param("startTime") String startTime, @Param("endTime") String endTime);
}
