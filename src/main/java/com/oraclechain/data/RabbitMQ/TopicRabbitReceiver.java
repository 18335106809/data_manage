package com.oraclechain.data.RabbitMQ;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.*;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * @author liuhualong
 * @Description: 监听MQ消息
 * @date 2020/1/6
 */
@Component
public class TopicRabbitReceiver {

    static Logger logger = LoggerFactory.getLogger(TopicRabbitReceiver.class);

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private AlarmInfoService alarmService;

    @Autowired
    private AlarmLevelService alarmLevelService;

    @Autowired
    private DeviceUpdateService deviceUpdateService;

    @Value("${device.parameter.breakdown}")
    private String breakdown;

    @Value("${device.parameter.open}")
    private String open;

    @Value("${device.parameter.auto}")
    private String auto;

    @Value("${device.parameter.turnOn}")
    private String turnOn;

    @Value("${device.parameter.angle}")
    private String angle;

    @Value("${device.type.hall-current-sensor}")
    private String hall;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    // 消费MQ中环境监控数据
    @RabbitListener(queues = TopicRabbitConfig.DEVICE)
    public void handleMessage1(Message message) {
        try {
            // 判断是否模拟故障的开关
            Boolean isMock = false;
            DeviceData deviceData = JSONObject.parseObject(new String(message.getBody()), DeviceData.class);
            Long deviceId = deviceData.getId();
            if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_MOCK_KEY + deviceId)) {
                isMock = true;
            }
            // 获取device的属性
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + deviceId, Device.class);
            JSONObject jsonObject = new JSONObject();
            JSONObject dataJson = JSONObject.parseObject(deviceData.getParam().toJSONString());
            DeviceCommon deviceCommon = new DeviceCommon(device);
            JSONObject thresholdJson = deviceCommon.getThresholdJson();
            for (String data : deviceCommon.getParams()) {
                if (StringUtils.isNotBlank(device.getType())) {
                    redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_PARAMETER_KEY + data, device.getType());
                }
                if (dataJson.containsKey(data)) {
                    // 判断探测器是否出现故障
                    if (data.contains(breakdown)) {
                        Integer value = dataJson.getInteger(data);
                        if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_STATUS_KEY + device.getId()) && value.equals(1)) {
                            redisTemplate.delete(RedisKeyConfig.DEVICE_STATUS_KEY + device.getId());
                            deviceUpdateService.updateDevice(new Device(deviceId, 1));
                        } else if (!redisTemplate.hasKey(RedisKeyConfig.DEVICE_STATUS_KEY + device.getId()) && value.equals(0)) {
                            redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_STATUS_KEY + device.getId(), "0");
                            deviceUpdateService.updateDevice(new Device(deviceId, 0));
                        }
                    }
                    // 相应的逻辑
                    double value = Double.valueOf(decimalFormat.format(dataJson.getDoubleValue(data)));
                    jsonObject.put(data, value);
                    // 将实时数据刷新到redis
                    if (!isMock) {
                        if (data.contains(open) || data.equals(auto) || data.contains(breakdown) || data.contains(turnOn) || data.contains(angle)) {
                            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data, String.valueOf((int) value));
                        } else {
                            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data, String.valueOf(value));
                        }
                    }
                    if (thresholdJson != null && thresholdJson.containsKey(data)) {
                        // 静力水准仪的特殊处理
                        if (device.getType().equals("structural")) {
                            Long lineDeviceId = 0L;
                            if (StringUtils.isNotBlank(device.getTheory())) {
                                lineDeviceId = Long.parseLong(device.getTheory());
                            }
                            if (lineDeviceId.equals(deviceId)) {
                                jsonObject.put(data, 0);
                                continue;
                            }
                            Object valueTmp = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + lineDeviceId, data);
                            if (valueTmp != null) {
                                Double lineValue = Double.parseDouble(valueTmp.toString());
                                value = value - lineValue;
                                jsonObject.put(data, value);
                            }
                        }
                        // 判断是否超出阈值
                        List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(data).toString(), Double.class);
                        if (doubleList.size() > 1 && (value < doubleList.get(0) || value > doubleList.get(1))) {
                            // 报警后该如何处理
                            // TODO: 2020/6/15
                            // 判断超过阈值的百分比，来定报警级别
                            Integer level = getAlarmLevel(device.getType(), value, doubleList);
                            alarmService.currentAlarm(device, value < doubleList.get(0) ? AlarmReason.BELOW_THRESHOLD.getName() : AlarmReason.ABOVE_THRESHOLD.getName(), isMock, data, String.valueOf(value), level);
                        } else {
                            alarmService.removeAlarm(device, data, isMock);
                        }
                    } else {
//                        logger.warn("device threshold is missing key:{},{}", data, device.getId());
                    }
                } else {
//                    logger.warn("data from mq is missing key:" + data + ", id:" + device.getId());
                }
            }
            device.setValue(jsonObject);
            device.setGatherTime(new Date().getTime());
            if (!deviceData.getMock() && !device.getType().equals(hall)) {
                this.mongoTemplateUtil.save(DeviceDisplay.toDisplay(device), MongoTemplateUtil.COLLECTION_DEVICE);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    // 消费MQ中环境监控数据
    @RabbitListener(queues = TopicRabbitConfig.DEVICE_SPECIAL)
    public void handleMessage(Message message) {
        try {
            DeviceData deviceData = JSONObject.parseObject(new String(message.getBody()), DeviceData.class);
            Long deviceId = deviceData.getId();
            // 获取device的属性
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + deviceId, Device.class);
            JSONObject jsonObject = new JSONObject();
            JSONObject dataJson = JSONObject.parseObject(deviceData.getParam().toJSONString());
            DeviceCommon deviceCommon = new DeviceCommon(device);
            JSONObject thresholdJson = deviceCommon.getThresholdJson();
            for (String data : deviceCommon.getParams()) {
                if (dataJson.containsKey(data)) {
                    double value = Double.valueOf(decimalFormat.format(dataJson.getDoubleValue(data)));
                    if (data.contains(open) || data.equals(auto) || data.contains(breakdown)  || data.contains(turnOn) || data.contains(angle)) {
                        redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data, String.valueOf((int) value));
                    } else {
                        redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data, String.valueOf(value));
                    }
                    if (thresholdJson != null && thresholdJson.containsKey(data)) {
                        List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(data).toString(), Double.class);
                        if (doubleList.size() > 1 && (value < doubleList.get(0) || value > doubleList.get(1))) {
                            // 报警后该如何处理
                            // TODO: 2020/6/15
                            // 判断超过阈值的百分比，来定报警级别
                            Integer level = getAlarmLevel(device.getType(), value, doubleList);
                            String reason = StringUtils.EMPTY;
                            if (data.equals(open)) {
                                reason = "发生入侵";
                            } else if (data.equals(turnOn)) {
                                reason = "井盖被拧开";
                            } else if (data.equals(angle)) {
                                reason = "井盖发生倾角";
                            }
                            alarmService.currentAlarm(device, reason, false, data, String.valueOf(value), level);
                        } else {
                            alarmService.removeAlarm(device, data, false);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public Integer getAlarmLevel(String deviceType, Double value, List<Double> doubleList) {
        Integer level = 3;
        Double  before = doubleList.get(0);
        Double next =  doubleList.get(1);
        List<AlarmLevel> alarmLevels = alarmLevelService.getLevelListByKey(deviceType);
        if (!CollectionUtils.isEmpty(alarmLevels)) {
            level = alarmLevels.get(0).getLevel();
        }
//        if (!CollectionUtils.isEmpty(alarmLevels)) {
//            for (AlarmLevel alarmLevel : alarmLevels) {
//                if (value < before) {
//                    if (value <= (before - (before * alarmLevel.getLowValue())) && value >= (before - (before * alarmLevel.getHighValue()))) {
//                        level = alarmLevel.getLevel();
//                    }
//                }
//                if (value > next) {
//                    if (value >= (next + (next * alarmLevel.getLowValue())) && value <= (next + (next * alarmLevel.getHighValue()))) {
//                        level = alarmLevel.getLevel();
//                    }
//                }
//            }
//        } else {
//            if (value < before) {
//                if (value < (before - (before * 0.3))) {
//                    level = 1;
//                } else if (value < (before - (before * 0.1))) {
//                    level = 2;
//                } else {
//                    level = 3;
//                }
//            }
//            if (value > next) {
//                if (value > (next + (next * 0.3))) {
//                    level = 1;
//                } else if (value > (next + (next * 0.1))) {
//                    level = 2;
//                } else {
//                    level = 3;
//                }
//            }
//        }
        return level;
    }

}
