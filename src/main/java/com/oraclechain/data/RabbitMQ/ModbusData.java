package com.oraclechain.data.RabbitMQ;

import com.digitalpetri.modbus.codec.Modbus;
import com.digitalpetri.modbus.master.ModbusTcpMaster;
import com.digitalpetri.modbus.master.ModbusTcpMasterConfig;
import com.digitalpetri.modbus.requests.*;
import com.digitalpetri.modbus.responses.*;
import io.netty.buffer.ByteBuf;
import io.netty.util.ReferenceCountUtil;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/14
 */
@Component
public class ModbusData {

    static ModbusTcpMaster modbusTcpMater;

    @PostConstruct
    public void initModbusMaster() {
        if (modbusTcpMater == null) {
            // 创建配置
//            ModbusTcpMasterConfig config = new ModbusTcpMasterConfig.Builder("localhost").setPort(502).build();
            ModbusTcpMasterConfig config = new ModbusTcpMasterConfig.Builder("192.168.0.15").setPort(502).build();
            modbusTcpMater = new ModbusTcpMaster(config);
        }
    }

    // 读取HoldingRegister数据
    public Number readHoldingRegisters(int address, int quantity, int unitId)
            throws InterruptedException, ExecutionException {
        Number result = null;
        CompletableFuture<ReadHoldingRegistersResponse> future = modbusTcpMater
                .sendRequest(new ReadHoldingRegistersRequest(address, quantity), unitId);
        ReadHoldingRegistersResponse readHoldingRegistersResponse = future.get();// 工具类做的同步返回.实际使用推荐结合业务进行异步处理
        if (readHoldingRegistersResponse != null) {
            ByteBuf buf = readHoldingRegistersResponse.getRegisters();
            result = buf.readFloat();
            ReferenceCountUtil.release(readHoldingRegistersResponse);
        }
        return result;
    }

    public void writeMultipleCoils(int address, int quantity,byte[] bytes,int unitId)
            throws InterruptedException, ExecutionException {
        WriteMultipleCoilsRequest request = new WriteMultipleCoilsRequest(address, quantity,bytes);
        CompletableFuture<WriteMultipleCoilsResponse> future = modbusTcpMater.sendRequest(request,unitId);
        WriteMultipleCoilsResponse response = future.get();
        if(response != null){
            ReferenceCountUtil.release(response);
        }
    }

    public void writeMultipleRegisters(int address, int quantity, byte[] bytes, int unitId)
            throws InterruptedException, ExecutionException {
        WriteMultipleRegistersRequest request = new WriteMultipleRegistersRequest(address, quantity,bytes);
        CompletableFuture<WriteMultipleRegistersResponse> future = modbusTcpMater.sendRequest(request,unitId);
//        WriteMultipleRegistersResponse response = future.get();
//        if(response != null){
//            ReferenceCountUtil.release(response);
//        }
    }

    // 读取InputRegisters模拟量数据
    public Number readInputRegisters(int address, int quantity, int unitId)
            throws InterruptedException, ExecutionException {
        Number result = null;
        CompletableFuture<ReadInputRegistersResponse> future = modbusTcpMater
                .sendRequest(new ReadInputRegistersRequest(address, quantity), unitId);
        ReadInputRegistersResponse readInputRegistersResponse = future.get();// 工具类做的同步返回.实际使用推荐结合业务进行异步处理
        if (readInputRegistersResponse != null) {
            ByteBuf buf = readInputRegistersResponse.getRegisters();
            result = buf.readFloat();
            ReferenceCountUtil.release(readInputRegistersResponse);
        }
        return result;
    }

    // 读取Coils开关量
    public ByteBuf readCoils(int address, int quantity, int unitId)
            throws InterruptedException, ExecutionException {
        ByteBuf result = null;
        CompletableFuture<ReadCoilsResponse> future = modbusTcpMater.sendRequest(new ReadCoilsRequest(address, quantity),
                unitId);
        ReadCoilsResponse readCoilsResponse = future.get();// 工具类做的同步返回.实际使用推荐结合业务进行异步处理
        if (readCoilsResponse != null) {
            ByteBuf buf = readCoilsResponse.getCoilStatus();
            result = buf;
            ReferenceCountUtil.release(readCoilsResponse);
        }
        return result;
    }

    // 读取readDiscreteInputs开关量
    public Boolean readDiscreteInputs(int address, int quantity, int unitId)
            throws InterruptedException, ExecutionException {
        Boolean result = null;
        CompletableFuture<ReadDiscreteInputsResponse> future = modbusTcpMater
                .sendRequest(new ReadDiscreteInputsRequest(address, quantity), unitId);
        ReadDiscreteInputsResponse discreteInputsResponse = future.get();// 工具类做的同步返回.实际使用推荐结合业务进行异步处理
        if (discreteInputsResponse != null) {
            ByteBuf buf = discreteInputsResponse.getInputStatus();
            result = buf.readBoolean();
            ReferenceCountUtil.release(discreteInputsResponse);
        }
        return result;
    }

    // 屎放资源
    public void release() {
        if (modbusTcpMater != null) {
            modbusTcpMater.disconnect();
        }
        Modbus.releaseSharedResources();
    }


}