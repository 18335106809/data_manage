package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.IntrusionRecord;
import com.oraclechain.data.service.IntrusionRecordService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@RestController
@RequestMapping("/intrusionRecord")
@Api(description = "防入侵相关操作")
public class IntrusionRecordController {

    static Logger logger = LoggerFactory.getLogger(IntrusionRecordController.class);

    @Autowired
    private IntrusionRecordService intrusionRecordService;

    @RequestMapping(value = "getAll", method = RequestMethod.POST)
    @ResponseBody
    public String getAll(@RequestBody IntrusionRecord intrusionRecord) {
        logger.info("get intrusion record data");
        if (null == intrusionRecord) {
            intrusionRecord = new IntrusionRecord();
        }
        if (null == intrusionRecord.getPageNum()) {
            intrusionRecord.setPageNum(1);
        }
        if (null == intrusionRecord.getPageSize()) {
            intrusionRecord.setPageSize(10);
        }
        try {
            Page<IntrusionRecord> intrusionRecords = intrusionRecordService.select(intrusionRecord);
            PageInfo<IntrusionRecord> pageInfo = new PageInfo<>(intrusionRecords);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get intrusion record data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody IntrusionRecord intrusionRecord) {
        logger.info("insert intrusion record data");
        try {
            intrusionRecordService.insert(intrusionRecord);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert intrusion record data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody IntrusionRecord intrusionRecord) {
        logger.info("update intrusion record data");
        try {
            intrusionRecordService.update(intrusionRecord);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update intrusion record data have error");
            return JSONObject.toJSONString(ResultUtil.error("更新数据失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody IntrusionRecord intrusionRecord) {
        logger.info("delete intrusion record data");
        if (null == intrusionRecord.getId()) {
            return JSONObject.toJSONString(ResultUtil.error("ID为空"));
        }
        try {
            intrusionRecordService.delete(intrusionRecord.getId());
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("delete intrusion record data have error");
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }
}
