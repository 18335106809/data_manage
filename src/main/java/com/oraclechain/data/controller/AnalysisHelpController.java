package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AnalysisFile;
import com.oraclechain.data.entity.AnalysisFileStatus;
import com.oraclechain.data.service.AnalysisFileService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
@RestController
@RequestMapping("/analysisHelp")
@Api(description = "报警分析辅助")
public class AnalysisHelpController {

    static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Value("${file.server.host}")
    private String fileHost;

    @Value("${file.server.store.path}")
    private String storePath;

    @Autowired
    private AnalysisFileService analysisFileService;

    @RequestMapping(value = "uploadOrUpdateFile", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadOrUpdateFile(@RequestParam(required = false) Integer id,
                                     @RequestParam(required = false) String uploader,
                                     @RequestParam(required = false) String planName,
                                     @RequestParam(required = false) String reviewer,
                                     @RequestParam(required = false) String auditResult,
                                     @RequestParam(required = false) MultipartFile file, HttpServletRequest request) {
        String fileName = null;
        String fileServerPath = null;
        String size = null;
        String path = null;
        try {
            if (file != null) {
                //获取文件名称
                fileName = file.getOriginalFilename();
                String uuid = UUID.randomUUID().toString();
                String filePath = storePath + uuid;
                logger.info("file upload for chat, fileName: " + fileName);
                path = filePath + "/" + fileName;
                File dest = new File(path);
                //2.上传文件保存路径
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                file.transferTo(dest);
                logger.info("file upload "+ fileName +" success");
                fileServerPath = "http://" + fileHost + "/" + uuid + "/" + fileName;
                size = ((int) (file.getSize() / 1024)) + "KB";
            }
            if (id != null) {
                analysisFileService.updateFile(new AnalysisFile(id, planName, fileName, size, fileServerPath, reviewer, new Date(), auditResult, uploader, path));
            } else {
                analysisFileService.insertFile(new AnalysisFile(planName, fileName, size, fileServerPath, reviewer, new Date(), AnalysisFileStatus.UNREVIEWED.getName(), uploader, path));
            }
            return JSONObject.toJSONString(ResultUtil.success("文件上传成功"));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("文件上传失败"));
        }
    }

    @RequestMapping(value = "selectFile", method = RequestMethod.POST)
    @ResponseBody
    public Object selectFile(@RequestBody Map<String, Object> map) {
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        String planName = ParameterUtil.getParameter(map, "planName", String.class);
        String fileName = ParameterUtil.getParameter(map, "fileName", String.class);
        String uploader = ParameterUtil.getParameter(map, "uploader", String.class);
        String auditResult = ParameterUtil.getParameter(map, "auditResult", String.class);
        try {
            Page<AnalysisFile> analysisFiles = analysisFileService.getFiles(planName, fileName, uploader, auditResult, page, limit);
            PageInfo<AnalysisFile> analysisFilePageInfo = new PageInfo<>(analysisFiles);
            return JSONObject.toJSONString(ResultUtil.success(analysisFilePageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "deleteFile", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteFile(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        try {
            AnalysisFile analysisFile = analysisFileService.getById(id);
            if (analysisFile != null) {
                File file = new File(analysisFile.getPath());
                analysisFileService.deleteFile(id);
                file.delete();
            }
            return JSONObject.toJSONString(ResultUtil.success("删除成功"));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除失败"));
        }
    }

}
