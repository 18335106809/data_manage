package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Inspection;
import com.oraclechain.data.entity.InspectionExcel;
import com.oraclechain.data.service.InspectionService;
import com.oraclechain.data.util.ExcelUtil;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/6
 */
@RestController
@RequestMapping("/inspection")
@Api(description = "巡检记录")
public class InspectionController {

    static Logger logger = LoggerFactory.getLogger(InspectionController.class);

    @Autowired
    private InspectionService inspectionService;

    @RequestMapping(value = "getData", method = RequestMethod.POST)
    @ResponseBody
    public String getData(@RequestBody Inspection inspection) {
        logger.info("get inspection data...");
        if (null == inspection) {
            inspection = new Inspection();
        }
        if (null == inspection.getPageNum()) {
            inspection.setPageNum(1);
        }
        if (null == inspection.getPageSize()) {
            inspection.setPageSize(10);
        }
        try {
            Page<Inspection> inspections = inspectionService.getData(inspection);
            PageInfo<Inspection> inspectionPageInfo = new PageInfo<>(inspections);
            return JSONObject.toJSONString(ResultUtil.success(inspectionPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get inspection data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody Inspection inspection) {
        logger.info("insert inspection data...");
        try {
            if (null == inspection.getInspectTime()) {
                inspection.setInspectTime(new Date());
            }
            inspectionService.insert(inspection);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert inspection data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody Inspection inspection) {
        logger.info("update inspection data...");
        try {
            if (null == inspection.getId()) {
                return JSONObject.toJSONString(ResultUtil.error("ID为空"));
            }
            inspectionService.update(inspection);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update inspection data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody Inspection inspection) {
        logger.info("delete inspection data");
        if (null == inspection.getId()) {
            return JSONObject.toJSONString(ResultUtil.error("ID为空"));
        }
        try {
            inspectionService.delete(inspection.getId());
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("delete inspection data have error");
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "export", method = RequestMethod.GET)
    @ResponseBody
    public String export(HttpServletResponse response) {
        logger.info("export inspection data");
        try {
            List<Inspection> inspectionList = inspectionService.getAllData();
            List<InspectionExcel> inspectionExcelList = new ArrayList<>();
            for (Inspection inspectionTmp : inspectionList) {
                InspectionExcel inspectionExcel = new InspectionExcel(inspectionTmp);
                inspectionExcelList.add(inspectionExcel);
            }
            ExcelUtil.exportExcel(inspectionExcelList, "巡检报告", "巡检报告", InspectionExcel.class, "巡检报告", response);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("export inspection data have error");
            return JSONObject.toJSONString(ResultUtil.error("导出数据失败"));
        }
    }
}
