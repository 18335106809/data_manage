package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.service.DeviceDataService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */

@RestController
@RequestMapping("/deviceData")
@Api(description = "设备数据相关操作")
public class DeviceDataController {

    static Logger logger = LoggerFactory.getLogger(DeviceDataController.class);

    @Autowired
    private DeviceDataService deviceDataService;

    @RequestMapping(value = "realTimeData", method = RequestMethod.POST)
    @ResponseBody
    public Object realTimeData(@RequestBody Map<String, Object> map) {
        List<Object> ids = ParameterUtil.getParameter(map, "ids", List.class);            // 设备集合
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);      // 舱体ID
        String type = ParameterUtil.getParameter(map, "type", String.class);            // 设备类型
        logger.info("获取实时设备数据");
        try {
            List<Device> deviceList = deviceDataService.realTimeData(ids, capsuleId, type);
            return JSONObject.toJSONString(ResultUtil.success(deviceList));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询实时数据失败"));
        }
    }

    @RequestMapping(value = "historyData", method = RequestMethod.POST)
    @ResponseBody
    public Object historyData(@RequestBody Map<String, Object> map) {
        Long id = ParameterUtil.getParameter(map, "id", Long.class);                    // 设备ID
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        logger.info("获取历史设备数据");
        try {
            List<DeviceDisplay> deviceList = deviceDataService.historyData(id, startTime, endTime);
            return JSONObject.toJSONString(ResultUtil.success(deviceList));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询历史数据失败"));
        }
    }

    @RequestMapping(value = "minuteData", method = RequestMethod.POST)
    @ResponseBody
    public Object minuteData(@RequestBody Map<String, Object> map) {
        Object capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Object deviceId = ParameterUtil.getParameter(map, "deviceId", Object.class);
        String type = ParameterUtil.getParameter(map, "type", String.class);
        Long time = ParameterUtil.getParameter(map, "time", Long.class);
        logger.info("获取一分钟内历史设备数据，capsuleId:" + capsuleId + " deviceType : " + type);
        try {
            List<DeviceDisplay> deviceList = deviceDataService.minuteData(capsuleId, type, deviceId, time);
            return JSONObject.toJSONString(ResultUtil.success(deviceList));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询一分钟历史数据失败"));
        }
    }

    @RequestMapping(value = "setData", method = RequestMethod.POST)
    @ResponseBody
    public Object setData(@RequestBody Map<String, Object> map) {
        Long id = ParameterUtil.getParameter(map, "id", Long.class);
        Object value = ParameterUtil.getParameter(map, "value", Object.class);
        String key = ParameterUtil.getParameter(map, "device", String.class);
        try {
            deviceDataService.setData(id, key, value);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("赋值数据失败"));
        }
    }

}
