package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.service.StructuralHealthService;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/19
 */
@RestController
@RequestMapping("/structural")
@Api(description = "结构健康")
public class StructuralHealthController {

    static Logger logger = LoggerFactory.getLogger(StructuralHealthController.class);

    @Autowired
    private StructuralHealthService structuralHealthService;

    @RequestMapping(value = "getData", method = RequestMethod.POST)
    @ResponseBody
    public Object getData() {
        try {
            logger.info("select all structural health data");
            return JSONObject.toJSONString(ResultUtil.success(structuralHealthService.getData()));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select all structural health data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }
}
