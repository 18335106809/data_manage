package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.service.DeviceAvgDataService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/18
 */
@RestController
@RequestMapping("/green")
@Api(description = "绿色节能")
public class GreenEnergyController {

    static Logger logger = LoggerFactory.getLogger(GreenEnergyController.class);

    @Autowired
    private DeviceAvgDataService deviceAvgDataService;

    @RequestMapping(value = "getDataById", method = RequestMethod.POST)
    @ResponseBody
    public String getDataById(@RequestBody Map<String, Object> map) {
        Long id = ParameterUtil.getParameter(map, "id", Long.class);
        try {
            logger.info("select all green energy data, id : " + id);
            return JSONObject.toJSONString(ResultUtil.success(deviceAvgDataService.getGreenDataHistory(id)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select all green energy data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "openGreen", method = RequestMethod.POST)
    @ResponseBody
    public String openGreen(@RequestParam(required = false) Integer id) {
        try {
            logger.info("open green mode by id:" + id);
            return JSONObject.toJSONString(ResultUtil.success("启动成功"));
        } catch (Exception e) {
            logger.error("open green energy by id failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("启动绿色节能失败"));
        }
    }

}
