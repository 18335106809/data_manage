package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/9
 */
@RestController
@RequestMapping("/redis")
@Api(description = "redis中获取数据")
public class RedisGetDataController {

    static Logger logger = LoggerFactory.getLogger(RedisGetDataController.class);

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @RequestMapping(value = "getDataById", method = RequestMethod.POST)
    @ResponseBody
    public String getDataById(@RequestBody Map<String, Object> map) {
        try {
            Long id = ParameterUtil.getParameter(map, "id", Long.class);
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + id, Device.class);
            return  JSONObject.toJSONString(ResultUtil.success(device));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }
}
