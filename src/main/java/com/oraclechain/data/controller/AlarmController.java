package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmInfo;
import com.oraclechain.data.entity.AlarmLevel;
import com.oraclechain.data.entity.AlarmType;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.*;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import com.oraclechain.data.util.TimeUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/6
 */
@RestController
@RequestMapping("/alarm")
@Api(description = "报警信息相关操作")
public class AlarmController {

    static Logger logger = LoggerFactory.getLogger(AlarmController.class);

    @Autowired
    private AlarmInfoService alarmService;

    @Autowired
    private AlarmConfLevelService alarmConfLevelService;

    @Autowired
    private MockDataService mockDataService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private DeviceDataService deviceDataService;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private DeviceAvgDataService deviceAvgDataService;

    @Autowired
    private AlarmLevelService alarmLevelService;

    static Random random = new Random();

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @RequestMapping(value = "getAllAlarm", method = RequestMethod.POST)
    @ResponseBody
    public String getAllAlarm(@RequestBody Map<String, Object> map) {
        try {
            Integer pageNum = ParameterUtil.getParameter(map, "pageNum", Integer.class);
            Integer pageSize = ParameterUtil.getParameter(map, "pageSize", Integer.class);
            String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
            String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
            Integer level = ParameterUtil.getParameter(map, "level", Integer.class);
            String type = ParameterUtil.getParameter(map, "type", String.class);
            String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
            Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
            Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
            logger.info("select all alarm data");
            Page<AlarmInfo> alarms = alarmService.getAllAlarm(pageNum, pageSize, deviceType, level, type, capsuleId, segmentId, startTime, endTime);
            PageInfo<AlarmInfo> pageInfo = new PageInfo<>(alarms);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select all alarm data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "updateAlarm", method = RequestMethod.POST)
    @ResponseBody
    public String updateAlarm(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        String hurt = ParameterUtil.getParameter(map, "hurt", String.class);
        String accidentReason = ParameterUtil.getParameter(map, "accidentReason", String.class);
        try {
            alarmService.updateAlarm(hurt, accidentReason, id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update alarm data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("更新数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmType", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmType() {
        try {
            JSONObject jsonObject = new JSONObject();
            List<String> alarmTypes = AlarmType.getAlarmType();
            jsonObject.put("type", alarmTypes);
            return JSONObject.toJSONString(ResultUtil.success(jsonObject));
        } catch (Exception e) {
            logger.error("获取报警类型失败");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmById", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmById(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        try {
            logger.info("select alarm data by id");
            AlarmInfo alarm = alarmService.getAlarmById(id);
            return JSONObject.toJSONString(ResultUtil.success(alarm));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select alarm data failed, id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmByDeviceId", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmByDeviceId(@RequestParam(required = false) Long deviceId,
                                     @RequestParam(defaultValue = "1", required = false) Integer pageNum,
                                     @RequestParam(defaultValue = "10", required = false) Integer pageSize) {
        try {
            logger.info("select alarm data by deviceId");
            Page<AlarmInfo> alarms = alarmService.getAlarmByDeviceId(deviceId, pageNum, pageSize);
            PageInfo<AlarmInfo> pageInfo = new PageInfo<>(alarms);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            logger.error("select alarm data failed, deviceId:" + deviceId, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "earlyWarn", method = RequestMethod.POST)
    @ResponseBody
    public String earlyWarnGetData(@RequestBody Map<String, Object> map) {
        List<Object> ids = ParameterUtil.getParameter(map, "ids", List.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        try {
            logger.info("select early warn data by cabinId : " + capsuleId + ",deviceType : " + deviceType);
            if (StringUtils.isEmpty(startTime)) {
                startTime = TimeUtil.getDates(7, true);
            }
            if (StringUtils.isEmpty(endTime)) {
                endTime = TimeUtil.getDates(1, true);
            }
            Object result = deviceAvgDataService.getDataByDay(ids, startTime, endTime);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select early warn data failed, capsuleId: " + capsuleId);
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "earlyWarnTable", method = RequestMethod.POST)
    @ResponseBody
    public String earlyWarnTable(@RequestBody Map<String, Object> map) {
        List<Object> ids = ParameterUtil.getParameter(map, "ids", List.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        try {
            logger.info("select early warn table data by cabinId : " + capsuleId);
            List<Device> deviceList = deviceDataService.realTimeData(ids, capsuleId, deviceType);
            List<JSONObject> result = new ArrayList<>();
            for (Device device : deviceList) {
                if (device.getType().trim().equals("firefighting")) {
                    continue;
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("deviceName", device.getName());
                jsonObject.put("value", device.getValue());
                jsonObject = redisGetDeviceService.setCapsule(jsonObject, device.getCapsule());
                jsonObject.put("deviceId", device.getId());
                if (alarmService.isAlarm(device.getId()) != null) {
                    jsonObject.put("isSafe", false);
                    String distanceTime = TimeUtil.getDateNext(random.nextInt(10), false);
                    jsonObject.put("alarmTime", distanceTime);
                    jsonObject.put("countDown", TimeUtil.getDistanceTime(null , distanceTime));
                } else {
                    jsonObject.put("isSafe", true);
                    jsonObject.put("alarmTime", "N/A");
                    jsonObject.put("countDown", "N/A");
                }
                DeviceCommon deviceCommon = new DeviceCommon(device);
                JSONObject thresholdJson = deviceCommon.getThresholdJson();
                JSONObject dangerJson = new JSONObject();
                for (String key : thresholdJson.keySet()) {
                    List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(key).toString(), Double.class);
                    thresholdJson.put(key, doubleList);
                    List<Double> dangerList = new ArrayList<>();
                    for (int i = 0; i < doubleList.size(); i++) {
                        Double value = doubleList.get(i);
                        if (i == 0) {
                            value = Double.valueOf(decimalFormat.format(value - (value / 2)));
                        } else {
                            value = Double.valueOf(decimalFormat.format(value + (value / 2)));
                        }
                        dangerList.add(value);
                    }
                    dangerJson.put(key, dangerList);
                }
                jsonObject.put("threshold", thresholdJson);
                jsonObject.put("danger", dangerJson);
                String distanceTime = TimeUtil.getDateNext(random.nextInt(10) + 5, false);
                jsonObject.put("dangerTime", distanceTime);
                result.add(jsonObject);
            }
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            logger.error("select early warn table data failed, capsuleId: " + capsuleId);
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "currentAlarm", method = RequestMethod.POST)
    @ResponseBody
    public String currentAlarm(@RequestBody Map<String, Object> map) {
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        Boolean isAid = false;
        try {
            logger.info("get current alarm list");
            if (!StringUtils.isEmpty(deviceType)) {
                isAid = true;
            }
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getCurrentAlarm(capsuleId, segmentId, deviceType, isAid)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get current alarm have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "currentAlarmDeviceData", method = RequestMethod.POST)
    @ResponseBody
    public String currentAlarmDeviceData(@RequestBody Map<String, Object> map) {
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        try {
            logger.info("get current alarm device data list");
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getCurrentAlarmDeviceData(capsuleId, segmentId, deviceType)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get current alarm device data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "currentAlarmBaseData", method = RequestMethod.POST)
    @ResponseBody
    public String currentAlarmBaseData(@RequestBody Map<String, Object> map) {
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        try {
            logger.info("get current alarm base data");
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getCurrentAlarmBaseData(capsuleId, segmentId, deviceType)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get current alarm base data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "historyAlarmCountData", method = RequestMethod.POST)
    @ResponseBody
    public String historyAlarmCountData(@RequestBody Map<String, Object> map) {
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String timeType = ParameterUtil.getParameter(map, "timeType", String.class);
        try {
            logger.info("get history alarm times");
            return JSONObject.toJSONString(ResultUtil.success(alarmService.historyAlarmTimes(null, deviceType, timeType)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get history alarm times have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "historyCountByLevel", method = RequestMethod.POST)
    @ResponseBody
    public String historyCountByLevel(@RequestBody Map<String, Object> map) {
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String timeType = ParameterUtil.getParameter(map, "timeType", String.class);
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        if (org.apache.commons.lang3.StringUtils.isBlank(timeType)) {
            timeType = "month";
        }
        logger.info("get history alarm count by level");
        try {
            if (timeType.equals("month") && StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                startTime = TimeUtil.beforeYearBegin();
                endTime = TimeUtil.nextMonth();
            } else if (timeType.equals("year") && StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                startTime = TimeUtil.getYear(4, true);
                endTime = TimeUtil.nextYear(TimeUtil.thisYearBegin());
            }
            return JSONObject.toJSONString(ResultUtil.success(alarmService.historyCountByLevel(deviceType, startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get history alarm count by level have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "historyAlarmData", method = RequestMethod.POST)
    @ResponseBody
    public String historyAlarmData(@RequestBody Map<String, Object> map) {
        Integer pageNum = ParameterUtil.getParameter(map, "pageNum", Integer.class);
        Integer pageSize = ParameterUtil.getParameter(map, "pageSize", Integer.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String timeType = ParameterUtil.getParameter(map, "timeType", String.class);
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
        if (org.apache.commons.lang3.StringUtils.isBlank(timeType)) {
            timeType = "month";
        }
        try {
            logger.info("get history alarm data");
            if (timeType.equals("month") && StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                startTime = TimeUtil.beforeYearBegin();
                endTime = TimeUtil.nextMonth();
            } else if (timeType.equals("year") && StringUtils.isEmpty(startTime) && StringUtils.isEmpty(endTime)) {
                startTime = TimeUtil.getYear(4, true);
                endTime = TimeUtil.nextYear(TimeUtil.thisYearBegin());
            }
            Page<AlarmInfo> alarms = alarmService.getAllAlarm(pageNum, pageSize, deviceType, null, null, capsuleId, segmentId, startTime, endTime);
            PageInfo<AlarmInfo> pageInfo = new PageInfo<>(alarms);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get history alarm data");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmSOP", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmSOP(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        logger.info("get alarm sop by alarmId : " + id);
        try {
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getSOPByAlarmId(id)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm sop by alarmId : " + id + " have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmMinuteData", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmMinuteData(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        Boolean isRealTime = ParameterUtil.getParameter(map, "isRealTime", Boolean.class);
        logger.info("get alarm minute data by alarmId : " + id + ", isReal : " + isRealTime);
        try {
            if (isRealTime == null) {
                isRealTime = true;
            }
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getAlarmMinuteData(id, isRealTime)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm minute data by alarmId : " + id + " have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmCountData", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmCountData(@RequestBody Map<String, Object> map) {
        Long deviceId = ParameterUtil.getParameter(map, "id", Long.class);
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        logger.info("get alarm count data by deviceId:" + deviceId);
        try {
            if (StringUtils.isEmpty(startTime)) {
                startTime = TimeUtil.getDates(9, true);
            }
            if (StringUtils.isEmpty(endTime)) {
                endTime = TimeUtil.getDates(0, false);
            }
            return JSONObject.toJSONString(ResultUtil.success(alarmService.getAlarmCountData(deviceId, startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm count data by deviceId : " + deviceId + " have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insertAlarmLevel", method = RequestMethod.POST)
    @ResponseBody
    public String insertAlarmLevel(@RequestBody AlarmLevel alarmLevel) {
        try {
            alarmLevelService.insert(alarmLevel);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert alarm level data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "getAlarmLevel", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmLevel() {
        try {
            List<AlarmLevel> alarmLevelList = alarmLevelService.getAll();
            return JSONObject.toJSONString(ResultUtil.success(alarmLevelList));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm level data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }


}
