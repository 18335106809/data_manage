package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.RabbitMQ.ModbusData;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/15
 */
@RestController
@RequestMapping("/modbus")
@Api(description = "模拟故障相关操作")
public class ModbusController {

    static Logger logger = LoggerFactory.getLogger(ModbusController.class);

    @Autowired
    private ModbusData modbusData;

    @RequestMapping(value = "getData", method = RequestMethod.POST)
    @ResponseBody
        public String createMockData() {
        try {
            System.out.println(modbusData.readHoldingRegisters(0, 4, 1));
            System.out.println(modbusData.readInputRegisters(0, 4,1));
            System.out.println(modbusData.readCoils(0, 1, 1));
            System.out.println(modbusData.readDiscreteInputs(0, 1, 1));
            System.out.println(modbusData.readDiscreteInputs(2, 1, 1));
            return JSONObject.toJSONString(ResultUtil.success("成功"));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("失败"));
        }
    }


}
