package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Document;
import com.oraclechain.data.service.DocumentService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Map;
import java.util.UUID;

/**
 * @author liuhualong
 * @Description:
 * @dat- 2020/2/6
 */
@RestController
@RequestMapping("/document")
@Api(description = "文件上传与下载")
public class FileController {

    static Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private DocumentService documentService;

    @Value("${file.server.host}")
    private String fileHost;

    @Value("${file.server.store.path}")
    private String storePath;

    @RequestMapping(value = "uploadOrUpdateFile", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadOrUpdateFile(@RequestParam(required = false) Integer id,
                                     @RequestParam(required = false) String uploader, @RequestParam(required = false) Integer type,
                                     @RequestParam(required = false) MultipartFile file, HttpServletRequest request) {
        String fileName = null;
        String path = null;
        String size = null;
        logger.info("上传或者修改档案，file : " + file + "uploader : " + uploader);
        try {
            //获取文件名称
            if (file != null) {
                fileName = file.getOriginalFilename();
                String uuid = UUID.randomUUID().toString();
                String filePath = storePath + uuid;
                logger.info("file upload, user: " + uploader + ", fileName: " + fileName);
                path = filePath + "/" + fileName;
                File dest = new File(path);
                //2.上传文件保存路径
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                size = ((int) (file.getSize() / 1024)) + "KB";
                file.transferTo(dest);
                logger.info("file upload "+ fileName +" success");
            }
            if (id != null) {
                documentService.updateDocument(new Document(id, fileName, uploader, path, type, size + "KB"));
            } else {
                documentService.insertDocument(new Document(fileName, uploader, path, type, size + "KB"));
            }
            return JSONObject.toJSONString(ResultUtil.success("文件上传成功"));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("文件上传失败"));
        }
    }

    @RequestMapping(value = "selectFile", method = RequestMethod.POST)
    @ResponseBody
    public Object selectFile(@RequestBody Map<String, Object> map) {
        Integer type = ParameterUtil.getParameter(map, "type", Integer.class);
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        try {
            Page<Document> pageDocument = documentService.getDocuments(type, page, limit);
            PageInfo<Document> documentPageInfo = new PageInfo<>(pageDocument);
            return JSONObject.toJSONString(ResultUtil.success(documentPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select document data failed, type:" + type, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "uploadForChat", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadForChat(@RequestParam(required = false) MultipartFile file, HttpServletRequest request) {
        if (file.isEmpty()) {
            return JSONObject.toJSONString(ResultUtil.error("图片不存在"));
        }
        //获取文件名称
        String fileName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString();
        String filePath = storePath + uuid;
        logger.info("file upload for chat, fileName: " + fileName);
//        String path = filePath + "\\" + fileName;
        String path = filePath + "/" + fileName;
        File dest = new File(path);
        try {
            //2.上传文件保存路径
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }

            file.transferTo(dest);
            logger.info("file upload "+ fileName +" success");
            String fileServerPath = "http://" + fileHost + "/" + uuid + "/" + fileName;
            return JSONObject.toJSONString(ResultUtil.success(fileServerPath));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("文件上传失败"));
        }
    }

    @RequestMapping(value = "deleteFile", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteFile(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        String filePath = ParameterUtil.getParameter(map, "filePath", String.class);
        try {
            documentService.deleteDocument(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("delete document data failed, id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "updateFile", method = RequestMethod.POST)
    @ResponseBody
    public Object updateFile(@RequestParam(required = false) Integer id,
                             @RequestParam(required = false) String filePath,
                             @RequestParam(required = false) String newName) {
        try {
            Document document = documentService.getById(id);
            if (!document.getFilePath().equals(filePath)) {
                return JSONObject.toJSONString(ResultUtil.error("修改数据失败"));
            }
            File file = new File(filePath);
            if (file.exists()) {
                String newPath = file.getParent() + "/" + newName;
                File dest = new File(newPath);
                file.renameTo(dest);
                document.setFileName(newName);
                document.setFilePath(newPath);
            }
            documentService.updateDocument(document);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update document data failed, id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("修改数据失败"));
        }
    }

    @RequestMapping(value = "downloadFile", method = RequestMethod.GET)
    @ResponseBody
    public Object downloadFile(@RequestParam(required = false) Integer id,
                             HttpServletResponse response) {
        try {
            Document document = documentService.getById(id);
            if (document == null) {
                return JSONObject.toJSONString(ResultUtil.error("文件已失效"));
            }
            String filePath = document.getFilePath();
            logger.info("file download, path: " + filePath);

            response.setHeader("content-type", "application/octet-stream");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(document.getFileName(), "UTF-8"));

            File file = new File(filePath);
            if (!file.exists()) {
                return JSONObject.toJSONString(ResultUtil.error("文件已不存在"));
            }
            //2、 读取文件--输入流
            InputStream input=new FileInputStream(file);
            //3、 写出文件--输出流
            OutputStream out = response.getOutputStream();

            byte[] buff =new byte[1024];
            int index=0;
            //4、执行 写出操作
            while((index= input.read(buff))!= -1){
                out.write(buff, 0, index);
                out.flush();
            }
            out.close();
            input.close();
            return JSONObject.toJSONString(ResultUtil.success("文件下载成功"));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("文件下载失败"));
        }
    }
}
