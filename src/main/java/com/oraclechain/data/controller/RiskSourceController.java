package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.RiskSource;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.service.RiskSourceService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/11
 */
@RestController
@RequestMapping("/riskSource")
@Api(description = "危险源相关操作")
public class RiskSourceController {

    static Logger logger = LoggerFactory.getLogger(AlarmController.class);

    @Autowired
    private RiskSourceService riskSourceService;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @RequestMapping(value = "getCapsule", method = RequestMethod.POST)
    @ResponseBody
    public String getCapsule(@RequestBody Map<String, Object> map) {
        Long pid = ParameterUtil.getParameter(map, "pid", Long.class);
        try {
            if (pid == null) {
                pid = -1L;
            }
            List<SpaceManage> spaceManageList = redisGetDeviceService.getSpaceTree(pid);
            return JSONObject.toJSONString(ResultUtil.success(spaceManageList));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getRiskSource", method = RequestMethod.POST)
    @ResponseBody
    public String getRiskSource(@RequestBody Map<String, Object> map) {
        Integer pageNum = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer pageSize = ParameterUtil.getParameter(map, "limit", Integer.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long spaceId = ParameterUtil.getParameter(map, "spaceId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String plc = ParameterUtil.getParameter(map, "plc", String.class);
        String riskObject = ParameterUtil.getParameter(map, "riskObject", String.class);
        String responsible = ParameterUtil.getParameter(map, "responsible", String.class);
        try {
            logger.info("select risk source");
            Page<RiskSource> riskSources = new Page<>();
            if (spaceId != null) {
                List<SpaceManage> spaceManageList = redisGetDeviceService.getSpaceTree(spaceId);
                for (SpaceManage spaceManage : spaceManageList) {
                    riskSources.addAll(riskSourceService.getRiskSource(spaceManage.getId(), deviceType, plc, riskObject, responsible, pageNum, pageSize));
                }
            } else {
                riskSources = riskSourceService.getRiskSource(capsuleId, deviceType, plc, riskObject, responsible, pageNum, pageSize);
            }
            for (RiskSource riskSource : riskSources) {
                SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + riskSource.getCapsuleId(), SpaceManage.class);
                if (spaceManage != null) {
                    riskSource.setCapsuleName(spaceManage.getName());
                }
            }
            PageInfo<RiskSource> pageInfo = new PageInfo<>(riskSources);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "addOrUpdateRiskSource", method = RequestMethod.POST)
    @ResponseBody
    public Object addOrUpdateRiskSource(@RequestBody RiskSource riskSource) {
        try {
            riskSource.setReportTime(new Date());
            if (riskSource.getId() != null) {
                riskSourceService.updateRiskSource(riskSource);
            } else {
                riskSourceService.insertRiskSource(riskSource);
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新数据失败"));
        }
    }

    @RequestMapping(value = "deleteRiskSource", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteDeal(@RequestBody Map<String, Integer> map) {
        try {
            Integer id = map.get("id");
            riskSourceService.deleteRiskSource(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }
}
