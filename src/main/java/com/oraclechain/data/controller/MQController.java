package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.entity.*;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/10
 */
@RestController
@RequestMapping("/mq")
@Api(description = "rabbitMQ相关操作")
public class MQController {

    static Logger logger = LoggerFactory.getLogger(MQController.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping(value = "mockData", method = RequestMethod.POST)
    @ResponseBody
    public String mockData(@RequestBody Map<String, Object> map) {
        Long id = ParameterUtil.getParameter(map, "id", Long.class);
        JSONObject jsonObject = ParameterUtil.getParameter(map, "mockData", JSONObject.class);
        try {
            logger.info("send to mq mock data");
            DeviceData deviceData = new DeviceData();
            deviceData.setId(id);
            deviceData.setParam(jsonObject);
            this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("send to mq oxygen data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("发送数据失败"));
        }
    }

}
