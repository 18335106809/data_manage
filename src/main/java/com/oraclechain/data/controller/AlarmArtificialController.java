package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmArtificial;
import com.oraclechain.data.entity.AlarmArtificialEnum;
import com.oraclechain.data.service.AlarmArtificialService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/31
 */
@RestController
@RequestMapping("/artificial")
@Api(description = "人工输入报警相关操作")
public class AlarmArtificialController {

    static Logger logger = LoggerFactory.getLogger(AlarmArtificialController.class);

    @Autowired
    private AlarmArtificialService alarmArtificialService;

    @RequestMapping(value = "getTypeList", method = RequestMethod.POST)
    @ResponseBody
    public String getTypeList() {
        logger.info("get alarm Artificial type list data");
        try {
            return JSONObject.toJSONString(ResultUtil.success(AlarmArtificialEnum.getAll()));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm Artificial type list data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getAll", method = RequestMethod.POST)
    @ResponseBody
    public String getAll(@RequestBody AlarmArtificial alarmArtificial) {
        logger.info("get alarm Artificial data");
        if (null == alarmArtificial) {
            alarmArtificial = new AlarmArtificial();
        }
        if (null == alarmArtificial.getPageNum()) {
            alarmArtificial.setPageNum(1);
        }
        if (null == alarmArtificial.getPageSize()) {
            alarmArtificial.setPageSize(10);
        }
        try {
            Page<AlarmArtificial> alarmArtificialList = alarmArtificialService.select(alarmArtificial);
            PageInfo<AlarmArtificial> pageInfo = new PageInfo<>(alarmArtificialList);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm Artificial data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody AlarmArtificial AlarmArtificial) {
        logger.info("insert alarm Artificial data");
        try {
            alarmArtificialService.insert(AlarmArtificial);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert alarm Artificial data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody AlarmArtificial AlarmArtificial) {
        logger.info("update alarm Artificial data");
        try {
            alarmArtificialService.update(AlarmArtificial);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update alarm Artificial data have error");
            return JSONObject.toJSONString(ResultUtil.error("更新数据失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody AlarmArtificial AlarmArtificial) {
        logger.info("delete alarm Artificial data");
        if (null == AlarmArtificial.getId()) {
            return JSONObject.toJSONString(ResultUtil.error("ID为空"));
        }
        try {
            alarmArtificialService.delete(AlarmArtificial.getId());
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("delete alarm Artificial data have error");
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "reportByTime", method = RequestMethod.POST)
    @ResponseBody
    public String reportByTime(@RequestBody Map<String, Object> map) {
        Integer alarmType = ParameterUtil.getParameter(map, "alarmType", Integer.class);
        String timeType = ParameterUtil.getParameter(map, "timeType", String.class);
        logger.info("get alarm Artificial report data by " + timeType);
        try {
            Object result = alarmArtificialService.reportByTime(alarmType, timeType);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm Artificial report data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "linkedDeviceData", method = RequestMethod.POST)
    @ResponseBody
    public String linkedDeviceData(@RequestBody Map<String, Object> map) {
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Integer alarmType = ParameterUtil.getParameter(map, "alarmType", Integer.class);
        try {
            logger.info("get linked device data list");
            Object result = alarmArtificialService.linkedDeviceData(capsuleId, alarmType);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get linked device data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }
}
