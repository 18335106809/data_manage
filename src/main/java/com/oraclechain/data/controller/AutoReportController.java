package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.AlarmInfo;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.service.AlarmInfoService;
import com.oraclechain.data.service.AutoReportService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import com.oraclechain.data.util.TimeUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/20
 */
@RestController
@RequestMapping("/autoReport")
@Api(description = "自动报表相关操作")
public class AutoReportController {

    static Logger logger = LoggerFactory.getLogger(AutoReportController.class);

    @Autowired
    private AlarmInfoService alarmService;

    @Autowired
    private AutoReportService autoReportService;

    @RequestMapping(value = "getAlarmToday", method = RequestMethod.POST)
    @ResponseBody
    public String getAlarmToday(@RequestBody Map<String, Object> map) {
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        Integer level = ParameterUtil.getParameter(map, "level", Integer.class);
        String type = ParameterUtil.getParameter(map, "type", String.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        Long segmentId = ParameterUtil.getParameter(map, "segmentId", Long.class);
        try {
            if (StringUtils.isBlank(startTime)) {
                startTime = TimeUtil.todayBegin();
            } else {
                startTime = TimeUtil.beginDay(startTime);
            }
            if (StringUtils.isBlank(endTime)) {
                endTime = TimeUtil.nextDay();
            } else {
                endTime = TimeUtil.nextDayBegin(endTime);
            }
            List<AlarmInfo> alarmInfoList = alarmService.getTodayAlarm(deviceType, level, type, capsuleId, segmentId, startTime, endTime);
            return JSONObject.toJSONString(ResultUtil.success(alarmInfoList));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get today alarm data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getDeviceData", method = RequestMethod.POST)
    @ResponseBody
    public String getDeviceData(@RequestBody Map<String, Object> map) {
        Object deviceId = ParameterUtil.getParameter(map, "deviceId", Long.class);
        Integer alarmId = ParameterUtil.getParameter(map, "alarmId", Integer.class);
        try {
            List<DeviceDisplay> result = autoReportService.getDeviceData(deviceId, alarmId);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get device data by alarmId and deviceId have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getEnvironmentStatus", method = RequestMethod.POST)
    @ResponseBody
    public String getEnvironmentStatus(@RequestBody Map<String, Object> map) {
        String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
        String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
        try {
            Object result = autoReportService.getDataByHour(startTime, endTime);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get environment status service have error");
            return JSONObject.toJSONString(ResultUtil.error("获取数据失败"));
        }
    }

}
