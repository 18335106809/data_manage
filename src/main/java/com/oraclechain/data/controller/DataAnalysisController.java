package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.DeviceDataReport;
import com.oraclechain.data.entity.DeviceDataResult;
import com.oraclechain.data.service.AlarmInfoService;
import com.oraclechain.data.service.DeviceAvgDataService;
import com.oraclechain.data.service.DeviceDataService;
import com.oraclechain.data.service.InstructionService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import com.oraclechain.data.util.TimeUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.parameters.P;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/19
 */
@RestController
@RequestMapping("/dataAnalysis")
@Api(description = "大数据分析")
public class DataAnalysisController {

    static Logger logger = LoggerFactory.getLogger(DataAnalysisController.class);

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @Autowired
    private InstructionService instructionService;

    @Autowired
    private AlarmInfoService alarmInfoService;

    @Autowired
    private DeviceAvgDataService deviceAvgDataService;

    @Autowired
    private DeviceDataService deviceDataService;

    @Value("${device.type.waterpump}")
    private String waterpump;

    @Value("${device.type.fan}")
    private String fan;

    @Value("${device.type.oxygen}")
    private String oxygen;

    @Value("${device.type.methane}")
    private String methane;

    @Value("${device.type.gauge}")
    private String gauge;

    @RequestMapping(value = "getDataById", method = RequestMethod.POST)
    @ResponseBody
    public String getDataById(@RequestBody Map<String, Object> map) {
        try {
            Long spaceId = ParameterUtil.getParameter(map, "id", Long.class);       // 舱的Id
            String startTime = ParameterUtil.getParameter(map, "startTime", String.class);
            String endTime = ParameterUtil.getParameter(map, "endTime", String.class);
            logger.info("select data analysis data by id : ");
            if (StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)) {
                return JSONObject.toJSONString(ResultUtil.error("起止时间不能为空"));
            }
            JSONObject jsonObject = new JSONObject();
            Random random = new Random();
            List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
            if (CollectionUtils.isEmpty(dates)) {
                return JSONObject.toJSONString(ResultUtil.success(jsonObject));
            }
            jsonObject.put("date", dates);
            jsonObject.put("irruptTimes", random.nextInt(200));
            jsonObject.put("irruptTimesAll", 200);
            List<Integer> alarms = new ArrayList<>();
            List<Integer> drainTimes = new ArrayList<>();
            List<Double> waterLevel = new ArrayList<>();
            List<Integer> windTimes = new ArrayList<>();
            List<Double> O2 = new ArrayList<>();
            List<Double> CH4 = new ArrayList<>();
            List<Double> average_O2 = new ArrayList<>();
            List<Double> average_CH4 = new ArrayList<>();
            List<DeviceDataReport> dataReport = null;
            for (String date : dates) {
                startTime = date;
                endTime = TimeUtil.nextDay(date);
                alarms.add(alarmInfoService.alarmTimes(null, spaceId, startTime, endTime));
                drainTimes.add(instructionService.countTimes(spaceId, null, waterpump, startTime, endTime));
                dataReport = deviceAvgDataService.getData(date, spaceId, null, gauge, null);
                if (dataReport.size() > 0 && dataReport.get(0) != null && dataReport.get(0).getDataMaxValue() != null) {
                    waterLevel.add(dataReport.get(0).getDataMaxValue());
                } else {
                    waterLevel.add(0.0);
                }
                windTimes.add(instructionService.countTimes(spaceId, null, fan, startTime, endTime));
                dataReport = deviceAvgDataService.getData(date, spaceId, null, oxygen, null);
                if (!CollectionUtils.isEmpty(dataReport)) {
                    O2.add(dataReport.get(0).getDataMaxValue() != null ? dataReport.get(0).getDataMaxValue() : 0);
                    average_O2.add(dataReport.get(0).getDataAvgValue() != null ? dataReport.get(0).getDataAvgValue() : 0);
                } else {
                    O2.add(0.0);
                    average_O2.add(0.0);
                }
                dataReport = deviceAvgDataService.getData(date, spaceId, null, methane, null);
                if (!CollectionUtils.isEmpty(dataReport)) {
                    CH4.add(dataReport.get(0).getDataMaxValue() != null ? dataReport.get(0).getDataMaxValue() : 0);
                    average_CH4.add(dataReport.get(0).getDataAvgValue() != null ? dataReport.get(0).getDataAvgValue() : 0);
                } else {
                    CH4.add(0.0);
                    average_CH4.add(0.0);
                }
            }
            jsonObject.put("alarm", alarms);
            jsonObject.put("drainTimes", drainTimes);
            jsonObject.put("waterLevel", waterLevel);
            jsonObject.put("windTimes", windTimes);
            jsonObject.put("O2", O2);
            jsonObject.put("CH4", CH4);
            jsonObject.put("average_O2", average_O2);
            jsonObject.put("average_CH4", average_CH4);
            if (Integer.parseInt(spaceId.toString()) == 1) {
                return JSONObject.toJSONString(ResultUtil.success(jsonObject));
            } else {
                return JSONObject.toJSONString(ResultUtil.success());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select data analysis data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getDateByWeek", method = RequestMethod.POST)
    @ResponseBody
    public String getDateByWeek(@RequestBody Map<String, Object> map) {
        Long id = ParameterUtil.getParameter(map, "id", Long.class);       // 舱的Id
        String dateKey = ParameterUtil.getParameter(map, "dataKey", String.class);
        try {
            List<JSONObject> result = new ArrayList<>();
            result.add(deviceAvgDataService.getDateByWeek(id, dateKey, 1));
            result.add(deviceAvgDataService.getDateByWeek(id, dateKey, 2));
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select data by week data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getDateByReal", method = RequestMethod.POST)
    @ResponseBody
    public String getDateByReal(@RequestBody Map<String, Object> map) {
        String dataKey = ParameterUtil.getParameter(map, "dataKey", String.class); // 数据key
        logger.info("select data by real time report, dataKey:" + dataKey);
        try {
            return JSONObject.toJSONString(ResultUtil.success(deviceDataService.getDateByReal(dataKey)));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select data by real time report failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

}
