package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Expert;
import com.oraclechain.data.service.ExpertService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/2/28
 */
@RestController
@RequestMapping("/expert")
@Api(description = "专家信息相关操作")
public class ExpertController {

    static Logger logger = LoggerFactory.getLogger(ExpertController.class);

    @Value("${file.server.host}")
    private String fileHost;

    @Value("${file.server.store.path}")
    private String storePath;

    @Autowired
    private ExpertService expertService;

    @RequestMapping(value = "selectAllExpert", method = RequestMethod.POST)
    @ResponseBody
    public String selectAllExpert(@RequestBody Map<String, Object> map) {
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        Integer sex = ParameterUtil.getParameter(map, "sex", Integer.class);
        String name = ParameterUtil.getParameter(map, "name", String.class);
        String department = ParameterUtil.getParameter(map, "department", String.class);
        String profession = ParameterUtil.getParameter(map, "profession", String.class);
        try {
            logger.info("select all expert data");
            Page<Expert> experts = expertService.getAllExpert(sex, name, department, profession, page, limit);
            PageInfo<Expert> expertPageInfo = new PageInfo<>(experts);
            return JSONObject.toJSONString(ResultUtil.success(expertPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insertOrUpdateExpert", method = RequestMethod.POST)
    @ResponseBody
    public String insertExpert(@RequestParam(required = false) Integer id,
                               @RequestParam(required = false) String number,
                               @RequestParam(required = false) String name,
                               @RequestParam(required = false) Integer sex,
                               @RequestParam(required = false) String department,
                               @RequestParam(required = false) String profession,
                               @RequestParam(required = false) String phoneNum,
                               @RequestParam(required = false) String specialty,
                               @RequestParam(required = false) String achievement,
                               @RequestParam(required = false) MultipartFile file) {
        try {
            logger.info("insert expert data");
            String picture = storeFile(file);
            if (id != null) {
                expertService.updateExpert(new Expert(id, number, name, sex, department, profession, phoneNum, specialty, achievement, picture));
            } else {
                expertService.insertExpert(new Expert(number, name, sex, department, profession, phoneNum, specialty, achievement, picture, new Date()));
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("insert expert data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    public String storeFile(MultipartFile file) {
        String result = StringUtils.EMPTY;
        if (file != null) {
            String fileName = file.getOriginalFilename();
            String uuid = UUID.randomUUID().toString();
            String filePath = storePath + uuid;
            logger.info("file upload for chat, fileName: " + fileName);
            String path = filePath + "/" + fileName;
            File dest = new File(path);
            try {
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                result = "http://" + fileHost + "/" + uuid + "/" + fileName;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @RequestMapping(value = "deleteExpert", method = RequestMethod.POST)
    @ResponseBody
    public String deleteExpert(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        try {
            logger.info("delete expert data by id:" + id);
            expertService.deleteExpert(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("delete expert data failed, id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }
}
