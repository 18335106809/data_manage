package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.DictionaryData;
import com.oraclechain.data.service.DictionaryDataService;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/16
 */
@RestController
@RequestMapping("/dictionary")
@Api(description = "plc信息相关操作")
public class DictionaryDataController {

    static Logger logger = LoggerFactory.getLogger(DictionaryDataController.class);

    @Autowired
    private DictionaryDataService dictionaryDataService;

    @RequestMapping(value = "select", method = RequestMethod.POST)
    @ResponseBody
    public String select(@RequestBody DictionaryData dictionaryData) {
        logger.info("获取字典内容");
        try {
            List<DictionaryData> result = dictionaryDataService.select(dictionaryData);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("获取字典内容失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody DictionaryData dictionaryData) {
        logger.info("更新字典内容");
        try {
            dictionaryDataService.update(dictionaryData);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新字典内容失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody DictionaryData dictionaryData) {
        logger.info("新增字典内容");
        try {
            dictionaryDataService.insert(dictionaryData);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("新增字典内容失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody DictionaryData dictionaryData) {
        try {
            if (dictionaryData.getId() != null) {
                logger.info("删除字典内容, by:" + dictionaryData.getId() );
                dictionaryDataService.delete(dictionaryData.getId());
                return JSONObject.toJSONString(ResultUtil.success());
            } else {
                return JSONObject.toJSONString(ResultUtil.error("没有字典id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除字典内容失败"));
        }
    }

    @RequestMapping(value = "updateCache", method = RequestMethod.POST)
    @ResponseBody
    public String updateCache() {
        logger.info("更新redis中的字典信息");
        try {
            dictionaryDataService.updateCache();
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新redis失败"));
        }
    }
}
