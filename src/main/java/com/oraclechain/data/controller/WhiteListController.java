package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.IntrusionRecord;
import com.oraclechain.data.entity.WhiteList;
import com.oraclechain.data.service.WhiteListService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@RestController
@RequestMapping("/whiteList")
@Api(description = "白名单相关操作")
public class WhiteListController {

    static Logger logger = LoggerFactory.getLogger(WhiteListController.class);

    @Autowired
    private WhiteListService whiteListService;

    @RequestMapping(value = "getAll", method = RequestMethod.POST)
    @ResponseBody
    public String getAll(@RequestBody WhiteList whiteList) {
        logger.info("get white list data");
        if (null == whiteList) {
            whiteList = new WhiteList();
        }
        if (null == whiteList.getPageNum()) {
            whiteList.setPageNum(1);
        }
        if (null == whiteList.getPageSize()) {
            whiteList.setPageSize(10);
        }
        try {
            Page<WhiteList> whiteLists = whiteListService.select(whiteList);
            PageInfo<WhiteList> pageInfo = new PageInfo<>(whiteLists);
            return JSONObject.toJSONString(ResultUtil.success(pageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get white list data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody WhiteList whiteList) {
        logger.info("insert white list data");
        try {
            whiteListService.insert(whiteList);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert white list data have error");
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody WhiteList whiteList) {
        logger.info("update white list data");
        try {
            whiteListService.update(whiteList);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update white list data have error");
            return JSONObject.toJSONString(ResultUtil.error("更新数据失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody WhiteList whiteList) {
        logger.info("delete white list data");
        if (null == whiteList.getId()) {
            return JSONObject.toJSONString(ResultUtil.error("ID为空"));
        }
        try {
            whiteListService.delete(whiteList.getId());
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("delete white list data have error");
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }
}
