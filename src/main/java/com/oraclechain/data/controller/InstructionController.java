package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.InstructionService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/24
 */
@RestController
@RequestMapping("/instruction")
@Api(description = "下发指令")
public class InstructionController {

    static Logger logger = LoggerFactory.getLogger(GreenEnergyController.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private InstructionService instructionService;

    @RequestMapping(value = "updateStatus", method = RequestMethod.POST)
    @ResponseBody
    public String updateStatus(@RequestBody Map<String, Object> map) {
        Object id = ParameterUtil.getParameter(map, "id", Object.class);
        String key = ParameterUtil.getParameter(map, "key", String.class);
        Integer value = ParameterUtil.getParameter(map, "value", Integer.class);
        try {
            logger.info("update device status by id:" + id);
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + id, key, String.valueOf(value));
            instructionService.singleInstruction(id, key, value);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update device status by id failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("修改设备状态失败"));
        }
    }

    @RequestMapping(value = "bulkStatus", method = RequestMethod.POST)
    @ResponseBody
    public String bulkStatus(@RequestBody Map<String, Object> map) {
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        String dataKey = ParameterUtil.getParameter(map, "dataKey", String.class);
        Integer status = ParameterUtil.getParameter(map, "status", Integer.class);
        try {
            logger.info("bulk update device status by deviceType:" + deviceType + ", capsule:" + capsuleId);
            instructionService.bulkInstruction(capsuleId, deviceType, dataKey, status);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("update device status by id failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("修改设备状态失败"));
        }
    }

    @RequestMapping(value = "reset", method = RequestMethod.POST)
    @ResponseBody
    public String reset(@RequestBody Map<String, Object> map) {
        Object id = ParameterUtil.getParameter(map, "id", Object.class);
        try {
            logger.info("(复位)reset device status by id:" + id);
            instructionService.alarmHandling(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("(复位)reset device status by id failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("复位失败"));
        }
    }
}
