package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Goods;
import com.oraclechain.data.entity.GoodsLibrary;
import com.oraclechain.data.entity.Parameter;
import com.oraclechain.data.service.GoodsService;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
@RestController
@RequestMapping("/goods")
@Api(description = "物资信息相关操作")
public class GoodsController {

    static Logger logger = LoggerFactory.getLogger(GoodsController.class);

    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = "getAllGoodsLibrary", method = RequestMethod.POST)
    @ResponseBody
    public String getAllGoodsLibrary() {
        try {
            logger.info("select all goods library data");
            List<GoodsLibrary> goodsLibraryList = getDatas(-1);
            return JSONObject.toJSONString(ResultUtil.success(goodsLibraryList));
        } catch (Exception e) {
            logger.error("select all goods library data failed", e.getMessage());
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    public List<GoodsLibrary> getDatas(Integer pid) {
        List<GoodsLibrary> goodsLibraryList = goodsService.getGoodsLibraryByPid(pid);
        if (!goodsLibraryList.isEmpty()) {
            for (GoodsLibrary goodsLibrary : goodsLibraryList) {
                List<GoodsLibrary> goodsLibraries = getDatas(goodsLibrary.getId());
                if (!goodsLibraries.isEmpty()) {
                    goodsLibrary.setGoodsLibraryList(goodsLibraries);
                }
            }
        }
        return goodsLibraryList;
    }

    @RequestMapping(value = "insertOrUpdateGoodsLibrary", method = RequestMethod.POST)
    @ResponseBody
    public String insertOrUpdateGoodsLibrary(@RequestBody GoodsLibrary goodsLibrary) {
        try {
            logger.info("insert goods library data");
            if (goodsLibrary.getId() != null) {
                goodsService.updateGoodsLibrary(goodsLibrary);
            } else {
                goodsService.insertGoodsLibrary(goodsLibrary);
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("insert goods library data failed", e.getMessage());
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "deleteGoodsLibrary", method = RequestMethod.POST)
    @ResponseBody
    public String deleteGoodsLibrary(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        try {
            goodsService.deleteGoodsLibrary(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "getAllGoods", method = RequestMethod.POST)
    @ResponseBody
    public String getAllGoods(@RequestBody Map<String, Object> map) {
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        String type = ParameterUtil.getParameter(map, "type", String.class);
        String name = ParameterUtil.getParameter(map, "name", String.class);
        Integer libraryId = ParameterUtil.getParameter(map, "libraryId", Integer.class);
        try {
            logger.info("select all goods data");
            Page<Goods> goodsList = goodsService.getAllGoods(type, libraryId, name, page, limit);
            PageInfo<Goods> goodsPageInfo = new PageInfo<>(goodsList);
            return JSONObject.toJSONString(ResultUtil.success(goodsPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select all goods data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getById", method = RequestMethod.POST)
    @ResponseBody
    public String getById(@RequestParam(required = false) Integer id) {
        try {
            logger.info("select goods data by id: " + id);
            Goods goods = goodsService.getById(id);
            return JSONObject.toJSONString(ResultUtil.success(goods));
        } catch (Exception e) {
            logger.error("select goods data by id failed,id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insertOrUpdateGoods", method = RequestMethod.POST)
    @ResponseBody
    public String insertGoods(@RequestBody Goods goods) {
        try {
            logger.info("insert goods data");
            if (goods.getId() != null) {
                goodsService.updateGoods(goods);
            } else {
                goods.setCreateTime(new Date());
                goodsService.insertGoods(goods);
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert goods data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("插入数据失败"));
        }
    }

    @RequestMapping(value = "deleteGoods", method = RequestMethod.POST)
    @ResponseBody
    public String deleteGoods(@RequestBody Map<String, Object> map) {
        Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
        try {
            logger.info("delete goods data by id:" + id);
            goodsService.deleteGoods(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("delete goods data failed, id:" + id, e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }
}
