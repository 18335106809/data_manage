package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmConfDeal;
import com.oraclechain.data.entity.AlarmConfDealExcel;
import com.oraclechain.data.entity.AlarmConfLevel;
import com.oraclechain.data.entity.AlarmLevelEnum;
import com.oraclechain.data.service.AlarmConfDealService;
import com.oraclechain.data.service.AlarmConfLevelService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ExcelUtil;
import com.oraclechain.data.util.PageInfo;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
@RestController
@RequestMapping("/alarmConf")
@Api(description = "预警配置相关操作")
public class AlarmConfController {

    static Logger logger = LoggerFactory.getLogger(AlarmConfController.class);

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private AlarmConfLevelService alarmConfLevelService;

    @Autowired
    private AlarmConfDealService alarmConfDealService;

    @RequestMapping(value = "getKeyWord", method = RequestMethod.POST)
    @ResponseBody
    public Object getKeyWord() {
        try {
            List<String> stringList = AlarmLevelEnum.getValues();
            return JSONObject.toJSONString(ResultUtil.success(stringList));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select keyWord data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "selectAlarmLevel", method = RequestMethod.POST)
    @ResponseBody
    public Object selectAlarmLevel(@RequestBody Map<String, Object> map) {
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        Integer level = ParameterUtil.getParameter(map, "level", Integer.class);
        String alarmEvent = ParameterUtil.getParameter(map, "alarmEvent", String.class);
        String device = ParameterUtil.getParameter(map, "device", String.class);
        try {
            Page<AlarmConfLevel> pageAlarmLevel = alarmConfLevelService.getAlarmConfLevel(page, limit, level, alarmEvent, device);
            PageInfo<AlarmConfLevel> alarmLevelPageInfo = new PageInfo<>(pageAlarmLevel);
            return JSONObject.toJSONString(ResultUtil.success(alarmLevelPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select alarm level data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "selectAlarmDeal", method = RequestMethod.POST)
    @ResponseBody
    public Object selectAlarmDeal(@RequestBody Map<String, Object> map) {
        Integer page = ParameterUtil.getParameter(map, "page", Integer.class);
        Integer limit = ParameterUtil.getParameter(map, "limit", Integer.class);
        String eventName = ParameterUtil.getParameter(map, "eventName", String.class);
        String keyWord = ParameterUtil.getParameter(map, "keyWord", String.class);
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        try {
            Page<AlarmConfDeal> pageAlarmDeal = alarmConfDealService.getAlarmConfDeal(page, limit, eventName, keyWord, capsuleId);
            PageInfo<AlarmConfDeal> alarmDealPageInfo = new PageInfo<>(pageAlarmDeal);
            return JSONObject.toJSONString(ResultUtil.success(alarmDealPageInfo));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("select alarm deal data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "addOrUpdateLevel", method = RequestMethod.POST)
    @ResponseBody
    public Object addOrUpdateLevel(@RequestBody AlarmConfLevel alarmConfLevel) {
        try {
            if (alarmConfLevel.getId() != null) {
                alarmConfLevelService.updateAlarmConfLevel(alarmConfLevel);
            } else {
                if (alarmConfLevel.getCreateTime() == null) {
                    alarmConfLevel.setCreateTime(new Date());
                }
                alarmConfLevelService.insertAlarmConfLevel(alarmConfLevel);
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "addOrUpdateDeal", method = RequestMethod.POST)
    @ResponseBody
    public Object addOrUpdateDeal(@RequestBody AlarmConfDeal alarmConfDeal) {
        try {
            if (alarmConfDeal.getId() != null) {
                alarmConfDealService.updateAlarmConfDeal(alarmConfDeal);
            } else {
                if (alarmConfDeal.getCreateTime() == null) {
                    alarmConfDeal.setCreateTime(new Date());
                }
                alarmConfDealService.insertAlarmConfDeal(alarmConfDeal);
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "deleteLevel", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteLevel(@RequestBody Map<String, Object> map) {
        try {
            Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
            alarmConfLevelService.deleteAlarmConfLevel(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "deleteDeal", method = RequestMethod.POST)
    @ResponseBody
    public Object deleteDeal(@RequestBody Map<String, Object> map) {
        try {
            Integer id = ParameterUtil.getParameter(map, "id", Integer.class);
            AlarmConfLevel alarmConfLevel = alarmConfLevelService.getLevelByDealId(id);
            if (alarmConfLevel != null) {
                return JSONObject.toJSONString(ResultUtil.error("此信息被使用，删除失败"));
            }
            alarmConfDealService.deleteAlarmConfDeal(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除数据失败"));
        }
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public void exportExcel(HttpServletResponse response) {
        try {
            long start = System.currentTimeMillis();
            List<AlarmConfDeal> alarmConfDealList = alarmConfDealService.getAllDeal();
            List<AlarmConfDealExcel> alarmConfDealExcels = redisGetDeviceService.turnToExcel(alarmConfDealList);
            ExcelUtil.exportExcel(alarmConfDealExcels, "报警处理配置", "报警处理配置", AlarmConfDealExcel.class, "报警处理配置", response);
            logger.info("导出excel所花时间：" + (System.currentTimeMillis() - start));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    @ResponseBody
    public Object importExcel(@RequestParam("file") MultipartFile file) {
        try {
            List<AlarmConfDealExcel> alarmConfDealExcelList = ExcelUtil.importExcel(file, AlarmConfDealExcel.class);
            List<AlarmConfDeal> alarmConfDealList = redisGetDeviceService.turnToDeal(alarmConfDealExcelList);
            alarmConfDealService.insertAlarmDeals(alarmConfDealList);
            return JSONObject.toJSONString(ResultUtil.success("导入数据成功"));
        } catch (Exception e) {
            logger.error("导入数据失败", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("导入数据失败"));
        }
    }

}
