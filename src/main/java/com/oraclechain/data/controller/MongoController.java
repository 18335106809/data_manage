package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/8
 */
@RestController
@RequestMapping("/mongo")
@Api(description = "Mongo相关操作")
public class MongoController {

    static Logger logger = LoggerFactory.getLogger(MongoController.class);

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @RequestMapping(value = "select", method = RequestMethod.POST)
    @ResponseBody
    public String selectData(@RequestParam(required = false) String deviceType, @RequestParam(required = false) Long deviceId) {
        try {
            Device deviceDisplay = new Device();
            if (StringUtils.isNotBlank(deviceType)) {
                deviceDisplay.setType(deviceType);
            }
            if (null != deviceId && deviceId > 0) {
                deviceDisplay.setId(deviceId);
            }
            List<JSONObject> devices = this.mongoTemplateUtil.findListByVo(deviceDisplay, null, null, JSONObject.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            return JSONObject.toJSONString(ResultUtil.success(devices));
        } catch (Exception e) {
            logger.error("select mongo camera data failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }
}
