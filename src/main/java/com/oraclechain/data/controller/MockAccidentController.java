package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oraclechain.data.entity.MockAccident;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.service.MockAccidentService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/20
 */
@RestController
@RequestMapping("/mock")
@Api(description = "模拟故障相关操作")
public class MockAccidentController {

    static Logger logger = LoggerFactory.getLogger(MockAccidentController.class);

    @Autowired
    private MockAccidentService mockAccidentService;

    @Autowired
    private ProviderClient providerClient;

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ResponseBody
    public String createMockData(@RequestBody Map<String, Object> map) {
        Object id = ParameterUtil.getParameter(map, "id", Object.class);
        List<MockAccident> mockAccidentList = ParameterUtil.getParameter(map, "mockData", List.class);
        ObjectMapper objectMapper = new ObjectMapper();
        List<MockAccident> list = objectMapper.convertValue(mockAccidentList, new TypeReference<List<MockAccident>>(){ });
        try {
            logger.info("mock accident data, size : " + list.size());
            mockAccidentService.mockAccident(list, id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("mock accident failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("模拟故障失败"));
        }
    }

    @RequestMapping(value = "shutDown", method = RequestMethod.POST)
    @ResponseBody
    public String shutDownMock(@RequestBody Map<String, Object> map) {
        Object id = ParameterUtil.getParameter(map, "id", Object.class);
        try {
            mockAccidentService.switchOn(id);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            logger.error("mock accident failed", e.getMessage());
            return JSONObject.toJSONString(ResultUtil.error("断开模拟故障失败"));
        }
    }

    @RequestMapping(value = "feign", method = RequestMethod.POST)
    @ResponseBody
    public String hiFeign(@RequestBody Map<String, Object> map) {
        return providerClient.getDeviceByCapsuleId(map);
    }
}
