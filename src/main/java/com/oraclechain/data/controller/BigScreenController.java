package com.oraclechain.data.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.service.BigScreenService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/26
 */
@RestController
@RequestMapping("/bigScreen")
@Api(description = "大屏展示数据")
public class BigScreenController {

    static Logger logger = LoggerFactory.getLogger(BigScreenController.class);

    @Autowired
    private BigScreenService bigScreenService;

    @Value("${device.type.oxygen}")
    private String oxygen;

    @Value("${device.type.gauge}")
    private String gauge;

    @Value("${device.type.co2}")
    private String co2;

    @Value("${device.type.hall-current-sensor}")
    private String hall_current_sensor;

    @RequestMapping(value = "methaneRealTime", method = RequestMethod.POST)
    @ResponseBody
    public String methaneRealTime() {
        try {
            Object result = bigScreenService.methaneRealTime();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get methane real time data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "atmosphericRealTime", method = RequestMethod.POST)
    @ResponseBody
    public String atmosphericRealTime(@RequestBody Map<String, Object> map) {
        Long spaceId = ParameterUtil.getParameter(map, "spaceId", Long.class);
        try {
            Object result = bigScreenService.atmosphericRealTime(spaceId);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get atmospheric real time data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "structuralRealTime", method = RequestMethod.POST)
    @ResponseBody
    public String structuralRealTime() {
        try {
            Object result = bigScreenService.structuralRealTime();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get structural real time data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "alarmTimesBySpace", method = RequestMethod.POST)
    @ResponseBody
    public String alarmTimesBySpace() {
        try {
            Object result = bigScreenService.alarmTimesBySpace();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm type by space data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "alarmType", method = RequestMethod.POST)
    @ResponseBody
    public String alarmType() {
        try {
            Object result = bigScreenService.alarmType();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get alarm type data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "oxygenAverageValue", method = RequestMethod.POST)
    @ResponseBody
    public String oxygenAverageValue() {
        try {
            Object result = bigScreenService.averageValue(oxygen);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get oxygen average value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "levelAverageValue", method = RequestMethod.POST)
    @ResponseBody
    public String levelAverageValue() {
        try {
            Object result = bigScreenService.averageValue(gauge);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get level average value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "levelDeviceData", method = RequestMethod.POST)
    @ResponseBody
    public String levelDeviceData() {
        try {
            Object result = bigScreenService.levelDeviceData(gauge);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get level device value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "CO2AverageValue", method = RequestMethod.POST)
    @ResponseBody
    public String CO2AverageValue() {
        try {
            Object result = bigScreenService.averageValue(co2);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get co2 average value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "deviceStatus", method = RequestMethod.POST)
    @ResponseBody
    public String deviceStatus() {
        try {
            Object result = bigScreenService.deviceStatus();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get device status value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "fanDeviceStatus", method = RequestMethod.POST)
    @ResponseBody
    public String fanDeviceStatus() {
        try {
            Object result = bigScreenService.fanDeviceStatus();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get fan device status data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "fanDeviceList", method = RequestMethod.POST)
    @ResponseBody
    public String fanDeviceList() {
        try {
            Object result = bigScreenService.fanDeviceList();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get fan device list value data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "energySave", method = RequestMethod.POST)
    @ResponseBody
    public String energySave() {
        try {
            Object result = bigScreenService.energySave(hall_current_sensor);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get energy save data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "energyInfo", method = RequestMethod.POST)
    @ResponseBody
    public String energyInfo() {
        try {
            Object result = bigScreenService.energyInfo(hall_current_sensor);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get energy info data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getDeviceTypeBySpaceId", method = RequestMethod.POST)
    @ResponseBody
    public String getDeviceTypeBySpaceId(@RequestBody Map<String, Object> map) {
        String deviceType = ParameterUtil.getParameter(map, "deviceType", String.class);
        Long spaceId = ParameterUtil.getParameter(map, "spaceId", Long.class);
        logger.info("get device list by spaceId and device type, spaceId:" + spaceId + ", deviceType:" + deviceType);
        try {
            Object result = bigScreenService.getDeviceTypeBySpaceId(deviceType, spaceId);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get device type by spaceId data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    /**
     * 获取舱下所有的设备类型
     * @return
     */
    @RequestMapping(value = "getDeviceTypeList", method = RequestMethod.POST)
    @ResponseBody
    public String getDeviceTypeList() {
        try {
            Object result = bigScreenService.getDeviceTypeList();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get device type list data have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

}
