package com.oraclechain.data.util;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
public class ParameterUtil {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T getParameter(Map<String, Object> map, String key, Class<T> clazz) {
        T t = null;
        if (map.get(key) != null) {
//            t = JSONObject.parseObject(map.get(key).toString(), clazz);
            t = objectMapper.convertValue(map.get(key), clazz);
        }
        return t;
    }
}
