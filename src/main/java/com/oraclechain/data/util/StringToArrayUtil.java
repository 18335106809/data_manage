package com.oraclechain.data.util;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author liuhualong
 * @Description: 将配置的设备参数数组字符串转换成数组
 * @date 2020/6/14
 */
public class StringToArrayUtil {

    public static List<String> toArray(String parameter) {
        List<String> result = new ArrayList<>();
        if (parameter.startsWith("[") && parameter.endsWith("]")) {
            parameter = parameter.replace("[", "").replace("]", "").replace("\"", "");
            result.addAll(Arrays.asList(parameter.split(",")));
        }
        return result;
    }

    public static List<Long> toLongArray(String parameter) {
        List<Long> result = new ArrayList<>();
        if (parameter.startsWith("[") && parameter.endsWith("]")) {
            parameter = parameter.replace("[", "").replace("]", "");
            String[] tem = parameter.split(",");
            for (String key : tem) {
                result.add(Long.parseLong(key));
            }
        }
        return result;
    }

}
