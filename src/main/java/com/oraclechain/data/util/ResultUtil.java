package com.oraclechain.data.util;

import java.io.Serializable;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/9
 */
public class ResultUtil<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;
    private String msg;
    private T result;

    private ResultUtil() {
        this.code = 0;
        this.msg = "success";
    }

    private ResultUtil(T data) {
        this.code = 0;
        this.msg = "success";
        this.result = data;
    }

    private ResultUtil(String msg) {
        this.code = 1;
        this.msg = msg;
    }

    /**
     * 成功时候的调用
     *
     * @return
     */
    public static <T> ResultUtil<T> success(T data) {
        return new ResultUtil<>(data);
    }

    /**
     * 成功时候的调用，没有result值
     *
     * @return
     */
    public static <T> ResultUtil<T> success() {
        return new ResultUtil<>();
    }

    /**
     * 失败时候的调用
     *
     * @return
     */
    public static <T> ResultUtil<T> error(String msg) {
        return new ResultUtil<>(msg);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

}
