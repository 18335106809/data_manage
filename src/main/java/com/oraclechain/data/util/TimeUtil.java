package com.oraclechain.data.util;

import io.netty.util.internal.StringUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/3
 */
public class TimeUtil {

    static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    static SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // 获取当前时间往后推的时间点
    public static String getDateNext(Integer hour, Boolean isBefore) {
        String result = StringUtil.EMPTY_STRING;
        try {
            Date currentTime = new Date();
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(currentTime);
            if (isBefore) {
                startDay.add(Calendar.HOUR_OF_DAY, -hour);
            } else {
                startDay.add(Calendar.HOUR_OF_DAY, hour);
            }
            startDay.add(Calendar.MINUTE, 23);
            Date newDate = startDay.getTime();
            String newDay = formatDate.format(newDate);
            result = newDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 获取当前时间往前推n天的时间点, isBefore判断往前推还是往后
    public static String getDates(Integer days, Boolean isBefore) {
        String result = StringUtil.EMPTY_STRING;
        try {
            Date currentTime = new Date();
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(currentTime);
            if (isBefore) {
                startDay.add(Calendar.DATE, -days);
            } else {
                startDay.add(Calendar.DATE, days);
            }
            Date newDate = startDay.getTime();
            String newDay = format.format(newDate);
            result = newDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getYear(Integer years, Boolean isBefore) {
        String result = StringUtil.EMPTY_STRING;
        try {
            Date currentTime = new Date();
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(currentTime);
            if (isBefore) {
                startDay.add(Calendar.YEAR, -years);
            } else {
                startDay.add(Calendar.YEAR, years);
            }
            startDay.set(Calendar.MONTH, 0);
            startDay.set(Calendar.DATE, 1);
            Date newDate = startDay.getTime();
            String newDay = format.format(newDate);
            result = newDay;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 取当前时间后的时间集合
    public static List<String> getBetweenDate(String end) {
        List<String> betweenList = new ArrayList<>();
        try {
            Date currentTime = new Date();
            betweenList = getBetweenDate(format.format(currentTime), end);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    public static List<String> getBetweenHour(String begin, String end) {
        List<String> betweenList = new ArrayList<>();
        try{
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(formatDate.parse(begin));
            if (startDay.getTimeInMillis() > formatDate.parse(end).getTime()) {
                return betweenList;
            }
            startDay.add(Calendar.HOUR_OF_DAY, -1);
            while(true){
                startDay.add(Calendar.HOUR_OF_DAY, 1);
                Date newDate = startDay.getTime();
                String newEnd=formatDate.format(newDate);
                betweenList.add(newEnd);
                if(end.equals(newEnd)){
                    break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    // 取两个时间点内的时间集合
    public static List<String> getBetweenDate(String begin, String end) {
        List<String> betweenList = new ArrayList<>();
        try{
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(format.parse(begin));
            if (startDay.getTimeInMillis() > format.parse(end).getTime()) {
                return betweenList;
            }
            startDay.add(Calendar.DATE, -1);
            while(true){
                startDay.add(Calendar.DATE, 1);
                Date newDate = startDay.getTime();
                String newEnd=format.format(newDate);
                betweenList.add(newEnd);
                if(end.equals(newEnd)){
                    break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    public static List<String> getBetweenMonth(String begin, String end) {
        List<String> betweenList = new ArrayList<>();
        try{
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(format.parse(begin));
            if (startDay.getTimeInMillis() > format.parse(end).getTime()) {
                return betweenList;
            }
            startDay.add(Calendar.MONTH, -1);
            while(true){
                startDay.add(Calendar.MONTH, 1);
                Date newDate = startDay.getTime();
                String newEnd=format.format(newDate);
                betweenList.add(newEnd);
                if(end.equals(newEnd)){
                    break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    public static List<String> getBetweenYear(String begin, String end) {
        List<String> betweenList = new ArrayList<>();
        try{
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(format.parse(begin));
            if (startDay.getTimeInMillis() > format.parse(end).getTime()) {
                return betweenList;
            }
            startDay.add(Calendar.YEAR, -1);
            while(true){
                startDay.add(Calendar.YEAR, 1);
                Date newDate = startDay.getTime();
                String newEnd=format.format(newDate);
                betweenList.add(newEnd);
                if(end.equals(newEnd)){
                    break;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    public static String getDistanceTime(String startTime, String endTime) {
        if (StringUtil.isNullOrEmpty(startTime)) {
            startTime = formatDate.format(new Date());
        }
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = formatDate.parse(startTime);
            two = formatDate.parse(endTime);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff ;
            if(time1<time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff/1000-day*24*60*60-hour*60*60-min*60);
            if (day > 0) {
                hour = hour + day * 24;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hour + ":" + min + ":" + sec;
    }

    public static Long beforeDayStart() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return currentTime.getTime();
    }

    public static Long beforeDayEnd() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return currentTime.getTime();
    }

    public static String beforeDay() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String nextDay() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String todayBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return formatDate.format(currentTime);
    }

    public static String beginDay(String startTime) throws Exception {
        Date time = format.parse(startTime);
        return formatDate.format(time);
    }

    public static String nextDayBegin(String startTime) throws Exception {
        Date time = format.parse(startTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(Calendar.DATE, 1);
        time = calendar.getTime();
        return formatDate.format(time);
    }

    public static String nowHour() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return formatDate.format(currentTime);
    }

    public static String nextMonth() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.MONTH, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String thisMonthBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.set(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String thisYearBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String nextMonthBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String beforeYearBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.YEAR, -1);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String beforeHalfYearBegin() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.MONTH, -5);
        calendar.set(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String nextMonth(String day) throws Exception {
        Date currentTime = format.parse(day);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.MONTH, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String nextDay(String day) throws Exception {
        Date currentTime = format.parse(day);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static String nextYear(String day) throws Exception {
        Date currentTime = format.parse(day);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.YEAR, 1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static Long str2Long(String time) throws Exception {
        Date date = formatDate.parse(time);
        return date.getTime();
    }

    public static Long strShort2Long(String time) throws Exception {
        Date date = format.parse(time);
        return date.getTime();
    }

    public static void main(String[] args) throws Exception {
        System.out.println(beginDay("2020-08-03"));
        System.out.println(nextDayBegin("2020-08-03"));
        System.out.println(beforeHalfYearBegin());
    }
}
