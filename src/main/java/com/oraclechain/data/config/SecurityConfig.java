package com.oraclechain.data.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/8
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().
                and()
                .csrf().disable()
                // 基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .anyRequest().authenticated();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/document/downloadFile")
                      .antMatchers("/document/uploadForChat")
                      .antMatchers("/modbus/*")
                      .antMatchers("/green/*")
                      .antMatchers("/alarm/*")
                      .antMatchers("/structural/*")
                      .antMatchers("/dataAnalysis/*")
                      .antMatchers("/alarmConf/export")
                      .antMatchers("/alarmConf/*")
                      .antMatchers("/analysisHelp/*")
                      .antMatchers("/expert/*")
                      .antMatchers("/goods/*")
                      .antMatchers("/riskSource/*")
                      .antMatchers("/mock/*")
                      .antMatchers("/mq/*")
                      .antMatchers("/deviceData/*")
                      .antMatchers("/instruction/*")
                      .antMatchers("/autoReport/*")
                      .antMatchers("/bigScreen/*")
                      .antMatchers("/intrusionRecord/*")
                      .antMatchers("/whiteList/*")
                      .antMatchers("/artificial/*")
                      .antMatchers("/dictionary/*")
                      .antMatchers("/inspection/*")
                      .antMatchers("/favicon.ico");
    }
}
