package com.oraclechain.data.service;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.AlarmConfDeal;
import com.oraclechain.data.entity.AlarmConfDealExcel;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/9
 */
public interface RedisGetDeviceService {
    <T> T getDataByKey(String key, Class<T> clazz);
    Device getDeviceById(Long id);
    SpaceManage getSpaceById(Long id);
    void updateSpaceNameMap();
    List<AlarmConfDealExcel> turnToExcel(List<AlarmConfDeal> alarmConfDealList);
    List<AlarmConfDeal> turnToDeal(List<AlarmConfDealExcel> alarmConfDealExcelList);
    List<SpaceManage> getSpaceTree(Long pid);
    JSONObject setCapsule(JSONObject jsonObject, Long capsuleId);
    Device setCapsuleInfo(Device device);
    List<Device> setCapsuleInfoList(List<Device> deviceList);
    List<Device> getAllDevice();
}