package com.oraclechain.data.service;

import com.corundumstudio.socketio.SocketIOClient;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/26
 */
public interface SocketIOService {

    // 启动服务
    void start() throws Exception;

    // 停止服务
    void stop();

    // 推动消息
    void pushMessage(String event, String message);

    List<SocketIOClient> getClients(String eventName);
}
