package com.oraclechain.data.service;

import com.oraclechain.data.entity.MockAccident;

import java.util.List;

/**
 * @author liuhualong
 * @Description:  模拟故障相关逻辑
 * @date 2020/4/14
 */
public interface MockAccidentService {
    void mockAccident(List<MockAccident> mockAccidents, Object id);
    void switchOn(Object id);
}
