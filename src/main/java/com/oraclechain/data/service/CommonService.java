package com.oraclechain.data.service;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/29
 */
public interface CommonService {
    List<JSONObject> getDeviceByType(String deviceType);
    List<Device> getDeviceReportList();
    Long getSpaceId(Long capsuleId);
}
