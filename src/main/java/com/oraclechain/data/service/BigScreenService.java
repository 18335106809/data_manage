package com.oraclechain.data.service;

import com.oraclechain.data.entity.device.Device;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/26
 */
public interface BigScreenService {
    Object methaneRealTime();
    Object atmosphericRealTime(Long spaceId);
    Object structuralRealTime();
    Object alarmTimesBySpace();
    Object alarmType();
    Object averageValue(String deviceType);
    Object levelDeviceData(String deviceType);
    Object deviceStatus();
    Object fanDeviceStatus();
    Object fanDeviceList();
    Object energySave(String deviceType);
    Object energyInfo(String deviceType);
    List<Device> feignDevice(String deviceType, Long spaceId);
    Object getDeviceTypeList();
    List<Device> getDeviceTypeBySpaceId(String deviceType, Long spaceId);
}
