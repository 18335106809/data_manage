package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmConfDeal;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
public interface AlarmConfDealService {
    List<AlarmConfDeal> getAllDeal();
    Page<AlarmConfDeal> getAlarmConfDeal(Integer pageNum, Integer pageSize, String eventName, String keyWord, Long capsuleId);
    void insertAlarmConfDeal(AlarmConfDeal alarmConfDeal);
    void insertAlarmDeals(List<AlarmConfDeal> alarmConfDealList);
    void updateAlarmConfDeal(AlarmConfDeal alarmConfDeal);
    void deleteAlarmConfDeal(Integer id);
}
