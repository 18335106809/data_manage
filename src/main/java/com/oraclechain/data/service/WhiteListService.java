package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.WhiteList;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
public interface WhiteListService {
    void delete(Integer id);
    void insert(WhiteList whiteList);
    void update(WhiteList whiteList);
    Page<WhiteList> select(WhiteList whiteList);
}
