package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AnalysisFile;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
public interface AnalysisFileService {
    Page<AnalysisFile> getFiles(String planName, String fileName, String uploader, String auditResult, Integer pageNum, Integer pageSize);
    void deleteFile(Integer id);
    void insertFile(AnalysisFile analysisFile);
    void updateFile(AnalysisFile analysisFile);
    AnalysisFile getById(Integer id);
}
