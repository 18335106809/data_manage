package com.oraclechain.data.service;

import com.oraclechain.data.entity.device.DeviceDisplay;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/20
 */
public interface AutoReportService {
    List<DeviceDisplay> getDeviceData(Object deviceId, Integer alarmId);
    Object getDataByHour(String startTime, String endTime);
}
