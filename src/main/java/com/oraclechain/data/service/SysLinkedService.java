package com.oraclechain.data.service;

/**
 * @author liuhualong
 * @Description: 系统联动相关接口
 * @date 2020/4/14
 */
public interface SysLinkedService {
    void sendOrderByAuto(Long deviceId, Integer status);
    void sendOrderByManual(Long deviceId);
    void updateStatus(Long deviceId);
}
