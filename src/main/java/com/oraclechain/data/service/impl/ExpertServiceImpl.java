package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.ExpertDao;
import com.oraclechain.data.entity.Expert;
import com.oraclechain.data.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/2/12
 */
@Service
public class ExpertServiceImpl implements ExpertService {

    @Autowired
    private ExpertDao expertDao;

    @Override
    public Page<Expert> getAllExpert(Integer sex, String name, String department, String profession, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return expertDao.getAllExpert(sex, name, department, profession);
    }

    @Override
    public Expert selectById(Integer id) {
        return expertDao.selectById(id);
    }

    @Override
    public void insertExpert(Expert expert) {
        expertDao.insertExpert(expert);
    }

    @Override
    public void updateExpert(Expert expert) {
        expertDao.updateExpert(expert);
    }

    @Override
    public void deleteExpert(Integer id) {
        expertDao.deleteExpert(id);
    }
}
