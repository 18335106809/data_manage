package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.CommonService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.service.StructuralHealthService;
import com.oraclechain.data.util.StringToArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/22
 */
@Service
public class StructuralHealthServiceImpl implements StructuralHealthService {

    static Logger logger = LoggerFactory.getLogger(StructuralHealthServiceImpl.class);

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private CommonService commonService;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Value("${device.type.structural}")
    private String structural;

    @Override
    public Object getData() {
        logger.info("get all Structural Health Data");
        JSONObject result = new JSONObject();
        List<String> deviceName = new ArrayList<>();
        List<JSONObject> realTime = new ArrayList<>();
        List<JSONObject> standardize = new ArrayList<>();
        List<JSONObject> difference = new ArrayList<>();
        List<JSONObject> capsule = new ArrayList<>();
        try {
            List<JSONObject> deviceList = commonService.getDeviceByType(structural);
            for (JSONObject json : deviceList) {
                Device device = JSONObject.parseObject(json.toJSONString(), Device.class);
                String parameter = device.getParameters();
                List<String> parameters = StringToArrayUtil.toArray(parameter);
                DeviceCommon deviceCommon = new DeviceCommon(device);
                JSONObject thresholdJson = deviceCommon.getThresholdJson();
                JSONObject valueJson = new JSONObject();
                JSONObject standardizeJson = new JSONObject();
                JSONObject differenceJson = new JSONObject();
                JSONObject capsuleJson = new JSONObject();
                capsuleJson = redisGetDeviceService.setCapsule(capsuleJson, device.getCapsule());
                for (String key : parameters) {
                    Object realValue = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), key);
                    Double value = null;
                    Double standardizeValue = null;
                    if (realValue != null) {
                        value = Double.parseDouble(realValue.toString());
                        valueJson.put(key, value);
                    }
                    if (thresholdJson.containsKey(key)) {
                        List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(key).toString(), Double.class);
                        if (key.equals("horizontal") && device.getTheory() != null) {
                            standardizeValue = Double.valueOf(device.getTheory());
                        } else {
                            standardizeValue = (doubleList.get(0) + doubleList.get(1)) / 2;
                        }
                        standardizeJson.put(key, standardizeValue);
                    }
                    if (value != null && standardizeValue != null) {
                        differenceJson.put(key, decimalFormat.format(Math.abs(value - standardizeValue)));
                    }
                }
                realTime.add(valueJson);
                deviceName.add(device.getCapsuleLocation() + device.getName());
                standardize.add(standardizeJson);
                difference.add(differenceJson);
                capsule.add(capsuleJson);
            }
            result.put("deviceName", deviceName);
            result.put("realTimeValue", realTime);
            result.put("standardize", standardize);
            result.put("difference", difference);
            result.put("capsule", capsule);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get Structural Health Data have error");
        }
        return null;
    }

}
