package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.entity.*;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.AlarmInfoService;
import com.oraclechain.data.service.MockAccidentService;
import com.oraclechain.data.service.RedisGetDeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author liuhualong
 * @Description: 模拟故障相关逻辑
 * @date 2020/4/14
 */
@Service
public class MockAccidentServiceImpl implements MockAccidentService {

    static Logger logger = LoggerFactory.getLogger(MockAccidentServiceImpl.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    static ConcurrentHashMap<Object, List<String>> mockDeviceMap = new ConcurrentHashMap<>();

    static ConcurrentHashMap<Object, ExecutorService> executorServiceMap = new ConcurrentHashMap<>();

    static ObjectMapper objectMapper = new ObjectMapper();

    @Async
    @Override
    public void mockAccident(List<MockAccident> mockAccidents, Object id) {
        try {
            logger.info("mock alarm begin...");
            if (mockAccidents.size() == 0) {
                return;
            }
            ExecutorService executorService = Executors.newFixedThreadPool(mockAccidents.size());
            executorServiceMap.put(id, executorService);
            List<String> deviceList = new ArrayList<>();
            for (MockAccident mockAccident : mockAccidents) {
                // 维护一个舱下模拟的设备ID集合，用于终止模拟时，消除redis中的模拟开关
                deviceList.add(mockAccident.getDeviceId());
                mockDeviceMap.put(id, deviceList);
                Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + mockAccident.getDeviceId(), Device.class);
                DeviceCommon deviceCommon = new DeviceCommon(device);
//                for (String dataKey : mockAccident.getAnalog().keySet()) {
//                    List<Double> thresholdList = objectMapper.convertValue(deviceCommon.getThresholdJson().get(dataKey), new TypeReference<List<Double>>(){ });
//                    Double mockValue = Double.valueOf(mockAccident.getAnalog().get(dataKey).toString());
                    // 判断设的模拟值是否超过阈值
//                    if (mockValue > thresholdList.get(0) && mockValue < thresholdList.get(1)) {
//                        logger.info("device mock accident did not exceed the threshold  : " + mockAccident.getDeviceId());
//                        continue;
//                    }
                    executorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            logger.info("mock accident to device : " + mockAccident.getDeviceId());
                            // 给设备一个模拟记录(模拟开关),从消息队列接收数据后只存数据，不做逻辑处理
                            redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_MOCK_KEY + mockAccident.getDeviceId(), "mock", 5, TimeUnit.MINUTES);
                            DeviceData deviceData = new DeviceData();
                            deviceData.setId(Long.valueOf(mockAccident.getDeviceId()));
                            deviceData.setParam(mockAccident.getAnalog());
                            deviceData.setMock(true);
                            if (mockAccident.getRadio() == 1) {
                                // 立即设置模拟值
                                logger.info(">>>>>>>>>>>>>>>设备" + mockAccident.getDeviceId() + "立即设置");
                                rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));

                            } else if (mockAccident.getRadio() == 2) {
                                // 缓慢设置
                                logger.info(">>>>>>>>>>>>>>>设备" + mockAccident.getDeviceId() + "开始缓慢设置");
                                for (int i = 0; i < mockAccident.getConsuming(); i++) {
                                    try {
                                        Thread.currentThread().sleep(1000L);
                                        rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
                                    } catch (InterruptedException e) {
                                        Thread.currentThread().destroy();
                                    }
                                }
                            }
                            if (mockAccident.getContinued()) {
                                try {
                                    logger.info(">>>>>>>>>>>>>>>>>>设备" + mockAccident.getDeviceId() + "模拟故障开始持续" );
                                    Thread.currentThread().sleep(1000L * mockAccident.getContinuedTime());
                                    redisTemplate.delete(RedisKeyConfig.DEVICE_MOCK_KEY + mockAccident.getDeviceId());
//                                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
                                } catch (InterruptedException e) {
                                    Thread.currentThread().destroy();
                                }
                            }
                        }
                    });
//                }
            }
            executorService.shutdown();
            while (true) {
                if (!executorServiceMap.containsKey(id) || executorService.isTerminated()) {
                    executorService.shutdownNow();
                    executorServiceMap.remove(id);
                    // 更新redis中相关的设备的开关
//                    removeMockIdFromRedis(id);
                    break;
                }
            }
            logger.info("mock accident is end");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    @Override
    public void switchOn(Object id) {
        logger.info("mock alarm shut down...");
        // 更新redis中相关的设备的开关
        removeMockIdFromRedis(id);
        if (null != executorServiceMap.get(id)) {
            executorServiceMap.get(id).shutdownNow();
            executorServiceMap.remove(id);
        }
    }

    public void removeMockIdFromRedis(Object id) {
        if (mockDeviceMap.containsKey(id)) {
            List<String> deviceList = mockDeviceMap.get(id);
            for (String deviceId : deviceList) {
                redisTemplate.delete(RedisKeyConfig.DEVICE_MOCK_KEY + deviceId);
            }
        }
    }
}
