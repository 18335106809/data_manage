package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.AlarmConfDeal;
import com.oraclechain.data.entity.AlarmConfDealExcel;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.RedisGetDeviceService;
import org.apache.commons.lang3.StringUtils;
import org.apache.coyote.ErrorState;
import org.apache.xmlbeans.xml.stream.Space;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/9
 */
@Service
public class RedisGetDeviceServiceImpl implements RedisGetDeviceService {

    static Map<Long, Device> deviceMap = new ConcurrentHashMap<>();

    static Map<Long, SpaceManage> spaceManageMap = new ConcurrentHashMap<>();

    static Map<String, SpaceManage> spaceManageNameMap = new ConcurrentHashMap<>();

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public <T> T getDataByKey(String key, Class<T> clazz) {
        String data = redisTemplate.opsForValue().get(key);
        return JSONObject.parseObject(data, clazz);
    }

    @Override
    public Device getDeviceById(Long id) {
        Device device = deviceMap.get(id);
        return device;
    }

    @Override
    public SpaceManage getSpaceById(Long id) {
        String data = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_SPACE_KEY + id);
        return JSONObject.parseObject(data, SpaceManage.class);
    }

    @Override
    public void updateSpaceNameMap() {
        Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_SPACE_KEY + "*");
        for (String key : keys) {
            SpaceManage spaceManage = getDataByKey(key, SpaceManage.class);
            spaceManageNameMap.put(spaceManage.getName(), spaceManage);
        }
    }

    @Override
    public List<AlarmConfDealExcel> turnToExcel(List<AlarmConfDeal> alarmConfDealList) {
        List<AlarmConfDealExcel> alarmConfDealExcelList = new ArrayList<>();
        for (AlarmConfDeal alarmConfDeal : alarmConfDealList) {
            SpaceManage spaceManage = getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmConfDeal.getCapsuleId(), SpaceManage.class);
            AlarmConfDealExcel alarmConfDealExcel = new AlarmConfDealExcel();
            alarmConfDealExcel.setCapsule(spaceManage == null ? null : spaceManage.getName());
            alarmConfDealExcel.setDevice(alarmConfDeal.getDevice());
            alarmConfDealExcel.setEventName(alarmConfDeal.getEventName());
            alarmConfDealExcel.setInstruction(alarmConfDeal.getInstruction());
            alarmConfDealExcel.setKeyWord(alarmConfDeal.getKeyWord());
            alarmConfDealExcel.setRunTime(alarmConfDeal.getRunTime());
            alarmConfDealExcelList.add(alarmConfDealExcel);
        }
        return alarmConfDealExcelList;
    }

    @Override
    public List<AlarmConfDeal> turnToDeal(List<AlarmConfDealExcel> alarmConfDealExcelList) {
        List<AlarmConfDeal> alarmConfDealList = new ArrayList<>();
        updateSpaceNameMap();
        for (AlarmConfDealExcel alarmConfDealExcel : alarmConfDealExcelList) {
            AlarmConfDeal alarmConfDeal = new AlarmConfDeal();
            alarmConfDeal.setCreateTime(new Date());
            alarmConfDeal.setDevice(alarmConfDealExcel.getDevice() == null ? null : alarmConfDealExcel.getDevice());
            if (alarmConfDealExcel.getCapsule() != null && spaceManageNameMap.containsKey(alarmConfDealExcel.getCapsule())) {
                alarmConfDeal.setCapsuleId(spaceManageNameMap.get(alarmConfDealExcel.getCapsule()).getId());
            } else {
                alarmConfDeal.setCapsuleId(null);
            }
            alarmConfDeal.setEventName(alarmConfDealExcel.getEventName() == null ? null : alarmConfDealExcel.getEventName());
            alarmConfDeal.setInstruction(alarmConfDealExcel.getInstruction() == null ? null : alarmConfDealExcel.getInstruction());
            alarmConfDeal.setKeyWord(alarmConfDealExcel.getKeyWord() == null ? null : alarmConfDealExcel.getKeyWord());
            alarmConfDeal.setRunTime(alarmConfDealExcel.getRunTime());
            alarmConfDealList.add(alarmConfDeal);
        }
        return alarmConfDealList;
    }

    @Override
    public List<SpaceManage> getSpaceTree(Long pid) {
        List<SpaceManage> result = new ArrayList<>();
        Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_SPACE_KEY + "*");
        Map<Long, List<SpaceManage>> longListMap = new ConcurrentHashMap<>();
        for (String key : keys) {
            SpaceManage spaceManage = getDataByKey(key, SpaceManage.class);
            if (longListMap.containsKey(spaceManage.getParentId())) {
                longListMap.get(spaceManage.getParentId()).add(spaceManage);
            } else {
                List<SpaceManage> spaceManages = new ArrayList<>();
                spaceManages.add(spaceManage);
                longListMap.put(spaceManage.getParentId(), spaceManages);
            }
        }
        return getDatas(longListMap, pid);
    }

    public List<SpaceManage> getDatas(Map<Long, List<SpaceManage>> longListMap, Long pid) {
        List<SpaceManage> spaceManageList = longListMap.get(pid);
        if (spaceManageList != null &&!spaceManageList.isEmpty()) {
            for (SpaceManage spaceManage : spaceManageList) {
                List<SpaceManage> spaceManageList1 = getDatas(longListMap, spaceManage.getId());
                if (spaceManageList1 != null && !spaceManageList1.isEmpty()) {
                    spaceManage.setSubSpaceManage(spaceManageList1);
                }
            }
        }
        return spaceManageList;
    }

    @Override
    public JSONObject setCapsule(JSONObject jsonObject, Long capsuleId) {
        SpaceManage spaceManage = getSpaceById(capsuleId);
        if (spaceManage != null) {
            jsonObject.put("capsuleName", spaceManage.getName());
            if (spaceManage.getParentId() != -1) {
                spaceManage = getSpaceById(spaceManage.getParentId());
                jsonObject.put("spaceName", spaceManage.getName());
            } else {
                jsonObject.put("spaceName", "");
            }
        } else {
            jsonObject.put("capsuleName", "");
        }
        return jsonObject;
    }

    @Override
    public Device setCapsuleInfo(Device device) {
        if (device.getCapsule() != null) {
            SpaceManage spaceManage = getSpaceById(device.getCapsule());
            if (spaceManage != null) {
                device.setCapsuleName(spaceManage.getName());
                device.setCapsuleNumber(spaceManage.getNumber());
                if (spaceManage.getParentId() != -1) {
                    spaceManage = getSpaceById(spaceManage.getParentId());
                    device.setSpaceId(spaceManage.getId());
                    device.setSpaceName(spaceManage.getName());
                } else {
                    device.setSpaceName("");
                }
            } else {
                device.setCapsuleName("");
            }
        }
        return device;
    }

    @Override
    public List<Device> setCapsuleInfoList(List<Device> deviceList) {
        for (Device device : deviceList) {
            setCapsuleInfo(device);
        }
        Collections.sort(deviceList);
        return deviceList;
    }

    @Override
    public List<Device> getAllDevice() {
        List<Device> result = new ArrayList<>();
        try {
            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            for (String key : keys) {
                Device device = getDataByKey(key, Device.class);
                result.add(device);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }
}
