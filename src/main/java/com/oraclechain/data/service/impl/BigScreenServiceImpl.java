package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.AlarmType;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.DeviceStatus;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.AlarmInfoService;
import com.oraclechain.data.service.BigScreenService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.StringToArrayUtil;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.ImplicitJoinColumnNameSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/26
 */
@Service
public class BigScreenServiceImpl implements BigScreenService {

    static Logger logger = LoggerFactory.getLogger(BigScreenServiceImpl.class);

    @Value("${device.type.methane}")
    private String methane;

    @Value("${device.type.atmospheric}")
    private String atmospheric;

    @Value("${device.type.structural}")
    private String structural;

    @Value("${device.type.oxygen}")
    private String oxygen;

    @Value("${device.type.gauge}")
    private String gauge;

    @Value("${device.type.fan}")
    private String fan;

    @Value("${device.type.single_fan}")
    private String single_fan;

    @Value("${device.type.two_fan}")
    private String two_fan;

    @Value("${device.type.induction_fan}")
    private String induction_fan;

    @Value("${device.type.light}")
    private String light;

    @Value("${device.type.cover}")
    private String cover;

    @Value("${instruction.open}")
    private String open;

    @Value("${device.type.firefighting}")
    private String firefighting;

    @Value("${device.type.waterpump}")
    private String waterpump;

    @Value("${device.hall-current-sensor.electricity}")
    private String electricity;

    @Value("${device.type.hall.ALLP}")
    private String ALLP;

    @Value("${daily.auto.report.device.type}")
    private String deviceKey2Value;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private ProviderClient providerClient;

    @Autowired
    private AlarmInfoService alarmInfoService;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    static Map<String, String> deviceType2Key = new ConcurrentHashMap<>();

    @Override
    public Object methaneRealTime() {
        return getDeviceDataByType(methane, null);
    }

    @Override
    public Object atmosphericRealTime(Long spaceId) {
//        List<SpaceManage> spaceManageList = feignSpace();
//        if (CollectionUtils.isEmpty(spaceManageList)) {
//            return null;
//        }
//        Integer index = new Random().nextInt(spaceManageList.size());
//        SpaceManage spaceManage = spaceManageList.get(index);
        List<Device> deviceList = getDeviceDataByType(atmospheric, spaceId);
        // 通过段进行分类
        Map<Long, List<Device>> mapByCapsuleId = new ConcurrentHashMap<>();
        for (Device device : deviceList) {
            if (device.getCapsule() != null) {
                List<Device> devices = new ArrayList<>();
                if (mapByCapsuleId.containsKey(device.getCapsule())) {
                    devices = mapByCapsuleId.get(device.getCapsule());
                }
                devices.add(device);
                mapByCapsuleId.put(device.getCapsule(), devices);
            }
        }
        // 计算每个段中温度、湿度的平均值
        List<JSONObject> result = new ArrayList<>();
        for (Map.Entry<Long, List<Device>> mapEntry : mapByCapsuleId.entrySet()) {
            JSONObject valueJsonBySpace = new JSONObject();
            List<Device> devices = mapEntry.getValue();
            Map<String, Double> mapByParameter = new ConcurrentHashMap<>();
            String spaceName = StringUtils.EMPTY;
            String capsuleName = StringUtils.EMPTY;
            for (Device device : devices) {
                if (StringUtils.isNotBlank(device.getCapsuleName())) {
                    spaceName = device.getSpaceName();
                    capsuleName = device.getCapsuleName();
                }
                List<String> parameterList = StringToArrayUtil.toArray(device.getParameters());
                JSONObject valueJson = JSONObject.parseObject(device.getValue().toString());
                for (String parameterKey : parameterList) {
                    if (mapByParameter.containsKey(parameterKey)) {
                        Double valueTmp = mapByParameter.get(parameterKey);
                        mapByParameter.put(parameterKey, valueTmp + Double.valueOf(valueJson.get(parameterKey).toString()));
                    } else {
                        mapByParameter.put(parameterKey, Double.valueOf(valueJson.get(parameterKey).toString()));
                    }
                }
            }
            JSONObject valueJson = new JSONObject();
            for (Map.Entry<String, Double> mapEntryValue : mapByParameter.entrySet()) {
                valueJson.put(mapEntryValue.getKey(), decimalFormat.format(mapEntryValue.getValue() / devices.size()));
            }
            valueJsonBySpace.put("capsuleId", mapEntry.getKey());
            valueJsonBySpace.put("value", valueJson);
            valueJsonBySpace.put("spaceName", spaceName);
            valueJsonBySpace.put("capsuleName", capsuleName);
            result.add(valueJsonBySpace);
        }
        return result;
    }

    @Override
    public Object structuralRealTime() {
        return getDeviceDataByType(structural, null);
    }

    @Override
    public Object alarmTimesBySpace() {
        List<JSONObject> feignSpaceList = ParameterUtil.getParameter(JSONObject.parseObject(providerClient.getSpaceManage()), "data", List.class);
        List<JSONObject> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(feignSpaceList)) {
            for (JSONObject jsonObject : feignSpaceList) {
                SpaceManage spaceManage = JSONObject.parseObject(jsonObject.toJSONString(), SpaceManage.class);
                JSONObject jsonBySpaceId = alarmInfoService.historyAlarmTimes(spaceManage.getId(), null, null);
                jsonBySpaceId.put("spaceId", spaceManage.getId());
                jsonBySpaceId.put("spaceName", spaceManage.getName());
                result.add(jsonBySpaceId);
            }
        }
        return result;
    }

    @Override
    public Object alarmType() {
        List<JSONObject> result = new ArrayList<>();
        String startTime = TimeUtil.beforeHalfYearBegin();
        String endTime = TimeUtil.nextDay();
        List<Integer> alarmTypeList = AlarmType.getAlarmTypeIndex();
        for (Integer alarmType : alarmTypeList) {
            JSONObject valueJson = new JSONObject();
            Integer count = alarmInfoService.alarmTimesByType(alarmType, startTime, endTime);
            valueJson.put("alarmType", AlarmType.getName(alarmType));
            valueJson.put("alarmTimes", count);
            result.add(valueJson);
        }
        return result;
    }

    public List<Device> getDeviceDataByType(String type, Long spaceId) {
        List<Device> result = new ArrayList<>();
        List<Device> deviceList = feignDevice(type, spaceId);
        deviceList = redisGetDeviceService.setCapsuleInfoList(deviceList);
        for (Device device : deviceList) {
            String parameter = device.getParameters();
            if (StringUtils.isBlank(parameter)) {
                continue;
            }
            JSONObject deviceValueJson = new JSONObject();
            List<String> parameterList = StringToArrayUtil.toArray(parameter);
            for (String paramTem : parameterList) {
                Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), paramTem);
                if (value != null) {
                    if (device.getType().trim().equals(firefighting) || device.getType().trim().contains(fan) || device.getType().trim().equals(waterpump)
                            || device.getType().trim().equals(light) || device.getType().trim().equals(cover)) {
                        deviceValueJson.put(paramTem, Integer.valueOf(value.toString()));
                    } else {
                        deviceValueJson.put(paramTem, Double.parseDouble(value.toString()));
                    }
                }
            }
            device.setValue(deviceValueJson);
            result.add(device);
        }
        return result;
    }

    @Override
    public Object averageValue(String deviceType) {
        JSONObject result = new JSONObject();
        List<Device> deviceList = getDeviceDataByType(deviceType, null);
        Double sum = 0D;
        Integer alarmSum = 0;
        if (CollectionUtils.isEmpty(deviceList)) {
            result.put("alarmSum", alarmSum);
            result.put("levelAverage", sum);
            return result;
        }
        for (Device device : deviceList) {
            String dataKey = deviceType2Key.get(deviceType);
            if (StringUtils.isNotBlank(dataKey)) {
                JSONObject valueJson = JSONObject.parseObject(device.getValue().toString());
                if (valueJson.containsKey(dataKey)) {
                    Double value = Double.valueOf(valueJson.get(dataKey).toString());
                    sum += value;
                }
            }
            if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + dataKey)) {
                alarmSum += 1;
            }
        }
        result.put("alarmSum", alarmSum);
        result.put("levelAverage", Double.valueOf(decimalFormat.format(sum / deviceList.size())));
        return result;
    }

    @Override
    public Object levelDeviceData(String deviceType) {
        String dataKey = deviceType2Key.get(deviceType);
        List<Device> deviceList = getDeviceDataByType(deviceType, null);
        for (Device device : deviceList) {
            if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + dataKey)) {
                device.setAlarm(true);
            } else {
                device.setAlarm(false);
            }
        }
        return deviceList;
    }

    @PostConstruct
    public void getDeviceType2Key() {
        String[] deviceTypes = deviceKey2Value.split(",");
        for (String key : deviceTypes) {
            String deviceType = key.substring(0, key.indexOf(":"));
            String dataKey = key.substring(key.indexOf(":") + 1);
            deviceType2Key.put(deviceType, dataKey);
        }
    }

    @Override
    public Object deviceStatus() {
        JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceStatus());
        JSONObject result = new JSONObject();
        Integer allDeviceNum = 0;
        Integer badDeviceNum = 0;
        Integer workingDeviceNum = 0;
        if (jsonObject.containsKey("code") && jsonObject.get("code").equals(0)) {
            List<JSONObject> deviceStatusList = ParameterUtil.getParameter(jsonObject, "data", List.class);
            for (JSONObject object : deviceStatusList) {
                DeviceStatus deviceStatus = JSONObject.parseObject(object.toString(), DeviceStatus.class);
                if (deviceStatus.getAllCount() != null) {
                    allDeviceNum += deviceStatus.getAllCount();
                }
                if (deviceStatus.getBadCount() != null) {
                    badDeviceNum += deviceStatus.getBadCount();
                }
                if (deviceStatus.getStartCount() != null) {
                    workingDeviceNum += deviceStatus.getStartCount();
                }
            }
        }
        result.put("allDeviceNum", allDeviceNum);
        result.put("badDeviceNum", badDeviceNum);
        result.put("workingDeviceNum", workingDeviceNum);
        return result;
    }

    @Override
    public Object fanDeviceStatus() {
        JSONObject result = new JSONObject();
        Integer isOpen = 0;
        Integer isClose = 0;
        List<Device> deviceList = new ArrayList<>();
        deviceList.addAll(feignDevice(single_fan, null));
        deviceList.addAll(feignDevice(two_fan, null));
        deviceList.addAll(feignDevice(induction_fan, null));
        for (Device device : deviceList) {
            DeviceCommon deviceCommon = new DeviceCommon(device);
            for (String data : deviceCommon.getParams()) {
                if (data.contains(open)) {
                    Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                    if (value != null && Integer.valueOf(value.toString()) == 1) {
                        isOpen += 1;
                    } else {
                        isClose += 1;
                    }
                }
            }
        }
        result.put("open", isOpen);
        result.put("close", isClose);
        return result;
    }

    @Override
    public Object fanDeviceList() {
        List<Device> deviceList = new ArrayList<>();
        deviceList.addAll(feignDevice(single_fan, null));
        deviceList.addAll(feignDevice(two_fan, null));
        deviceList.addAll(feignDevice(induction_fan, null));
        deviceList = redisGetDeviceService.setCapsuleInfoList(deviceList);
        for (Device device : deviceList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(open, 0);
            DeviceCommon deviceCommon = new DeviceCommon(device);
            for (String data : deviceCommon.getParams()) {
                if (data.contains(open)) {
                    Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                    if (value != null) {
                        jsonObject.put(open, Integer.valueOf(value.toString()));
                    }
                }
            }
            device.setValue(jsonObject);
        }
        return deviceList;
    }

    @Override
    public Object energySave(String deviceType) {
        List<Device> deviceList = getDeviceDataByType(deviceType, null);
        Double sum = 0.0;
        for (Device device : deviceList) {
            JSONObject valueJson = JSONObject.parseObject(device.getValue().toString());
            for (String key : valueJson.keySet()) {
                if (key.contains(electricity)) {
                    Double value = Double.valueOf(valueJson.get(key).toString());
                    sum += value;
                }
            }
        }
        if (deviceList.size() == 0) {
            return sum;
        } else {
            return Double.valueOf(decimalFormat.format(sum / deviceList.size()));
        }
    }

    @Override
    public Object energyInfo(String deviceType) {
        List<Device> deviceList = feignDevice(deviceType, null);
        try {
            for (Device device: deviceList) {
                List<DeviceDisplay> devices = new ArrayList<>();
                DeviceDisplay deviceDisplay = new DeviceDisplay();
                deviceDisplay.setDeviceId(device.getId());
                String startTime = TimeUtil.thisMonthBegin();
                String endTime = TimeUtil.nextDay();
                Long start = TimeUtil.strShort2Long(startTime);
                Long end = TimeUtil.strShort2Long(endTime);
                devices = mongoTemplateUtil.findListByVo(deviceDisplay, start, end, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
                JSONObject resultJson = new JSONObject();
                if (!CollectionUtils.isEmpty(devices)) {
                    Integer size = devices.size();
                    Double currentValue = 0.0;
                    DeviceDisplay deviceDisplay1 = devices.get(0);
                    JSONObject valueJson = JSONObject.parseObject(JSONObject.toJSONString(deviceDisplay1.getValue()));
                    for (String key : valueJson.keySet()) {
                        if (key.equals(ALLP)) {
                            Double value = Double.valueOf(valueJson.get(key).toString());
                            currentValue = value;
                        }
                    }
                    Double oldValue = 0.0;
                    DeviceDisplay deviceDisplay2 = devices.get(size - 1);
                    JSONObject valueJson1 = JSONObject.parseObject(JSONObject.toJSONString(deviceDisplay2.getValue()));
                    for (String key : valueJson1.keySet()) {
                        if (key.equals(ALLP)) {
                            Double value = Double.valueOf(valueJson1.get(key).toString());
                            oldValue = value;
                        }
                    }
                    Double differ = Math.abs(currentValue - oldValue);
                    resultJson.put("ALL_E", differ);
                } else {
                    resultJson.put("ALL_E", 0.0);
                }
                device.setValue(resultJson);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        List<Device> deviceList = getDeviceDataByType(deviceType, null);
//        for (Device device : deviceList) {
//            Double sum = 0.0;
//            JSONObject valueJson = JSONObject.parseObject(device.getValue().toString());
//            for (String key : valueJson.keySet()) {
//                if (key.contains(electricity)) {
//                    Double value = Double.valueOf(valueJson.get(key).toString());
//                    sum += value;
//                }
//            }
//            Double I_avg = Double.valueOf(decimalFormat.format(sum / 3));
//            valueJson.put("I_avg", I_avg);
//            device.setValue(valueJson);
//        }
        return deviceList;
    }

    @Override
    public List<Device> feignDevice(String deviceType, Long spaceId) {
        List<Device> result = new ArrayList<>();
        Map<String, Object> param = new HashMap<>();
        param.put("type", deviceType);
        try {
            JSONObject jsonObject;
            if (spaceId != null) {
                jsonObject = JSONObject.parseObject(providerClient.getDeviceBySpaceType(spaceId.toString(), deviceType));
            } else {
                jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
            }
            if (Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
                for (JSONObject deviceJson : deviceJSONList) {
                    Device device = JSONObject.parseObject(deviceJson.toJSONString(), Device.class);
                    result.add(device);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public List<SpaceManage> feignSpace() {
        List<SpaceManage> result = new ArrayList<>();
        try {
            JSONObject jsonObject = JSONObject.parseObject(providerClient.getSpaceManage());
            if (Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                List<JSONObject> spaceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
                for (JSONObject spaceJson : spaceJSONList) {
                    SpaceManage spaceManage = JSONObject.parseObject(spaceJson.toJSONString(), SpaceManage.class);
                    result.add(spaceManage);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public List<Long> feignQueryIdsBySpaceId(Long spaceId) {
        List<Long> result = new ArrayList<>();
        try {
            JSONObject jsonObject = JSONObject.parseObject(providerClient.querySpaceDeviceIds(spaceId.toString()));
            if (Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                List<Long> deviceIdList = JSONArray.parseArray(jsonObject.get("data").toString(), Long.class);
                result.addAll(deviceIdList);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    @Override
    public Object getDeviceTypeList() {
        List<SpaceManage> spaceManageList = feignSpace();
        for (SpaceManage spaceManage : spaceManageList) {
            if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_TYPE_LIST + spaceManage.getId())) {
                List<String> deviceTypes = redisTemplate.opsForList().range(RedisKeyConfig.DEVICE_TYPE_LIST + spaceManage.getId(), 0, -1);
                spaceManage.setDeviceTypeList(deviceTypes);
            } else {
                List<Long> deviceIds = feignQueryIdsBySpaceId(spaceManage.getId());
                Set<String> deviceTypeList = new HashSet<>();
                for (Long deviceId : deviceIds) {
                    Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + deviceId, Device.class);
                    if (device != null && StringUtils.isNotBlank(device.getType())) {
                        deviceTypeList.add(device.getType().trim());
                    }
                }
                List<String> deviceTypes = new ArrayList<>();
                deviceTypes.addAll(deviceTypeList);
                spaceManage.setDeviceTypeList(deviceTypes);
            }
        }
        return spaceManageList;
    }

    @Override
    public List<Device> getDeviceTypeBySpaceId(String deviceType, Long spaceId) {
        return getDeviceDataByType(deviceType, spaceId);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(TimeUtil.thisMonthBegin());
        System.out.println(TimeUtil.nextDay());
        System.out.println(TimeUtil.strShort2Long(TimeUtil.thisMonthBegin()));
        System.out.println(TimeUtil.str2Long("2020-12-01 00:00:00"));
    }
}
