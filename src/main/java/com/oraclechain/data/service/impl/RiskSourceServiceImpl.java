package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.RiskSourceDao;
import com.oraclechain.data.entity.RiskSource;
import com.oraclechain.data.service.RiskSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/11
 */
@Service
public class RiskSourceServiceImpl implements RiskSourceService {

    @Autowired
    private RiskSourceDao riskSourceDao;

    @Override
    public Page<RiskSource> getRiskSource(Long capsuleId, String deviceType, String plc, String riskObject,
                                          String responsible, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return riskSourceDao.getRiskSource(capsuleId, deviceType, plc, riskObject, responsible);
    }

    @Override
    public void insertRiskSource(RiskSource riskSource) {
        riskSourceDao.insertRiskSource(riskSource);
    }

    @Override
    public void updateRiskSource(RiskSource riskSource) {
        riskSourceDao.updateRiskSource(riskSource);
    }

    @Override
    public void deleteRiskSource(Integer id) {
        riskSourceDao.deleteRiskSource(id);
    }
}
