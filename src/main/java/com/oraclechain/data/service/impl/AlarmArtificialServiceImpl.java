package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.AlarmArtificialDao;
import com.oraclechain.data.entity.AlarmArtificial;
import com.oraclechain.data.entity.AlarmArtificialEnum;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.AlarmArtificialService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.weaver.patterns.HasThisTypePatternTriedToSneakInSomeGenericOrParameterizedTypePatternMatchingStuffAnywhereVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/31
 */
@Service
public class AlarmArtificialServiceImpl implements AlarmArtificialService {

    @Autowired
    private AlarmArtificialDao alarmArtificialDao;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Value("${device.alarm.artificial.type}")
    private String alarmType2Device;

    private static JSONObject jsonObject = new JSONObject();

    @Override
    public void insert(AlarmArtificial alarmArtificial) {
        alarmArtificialDao.insert(alarmArtificial);
    }

    @Override
    public void update(AlarmArtificial alarmArtificial) {
        alarmArtificialDao.update(alarmArtificial);
    }

    @Override
    public void delete(Integer id) {
        alarmArtificialDao.delete(id);
    }

    @Override
    public Page<AlarmArtificial> select(AlarmArtificial alarmArtificial) {
        PageHelper.startPage(alarmArtificial.getPageNum(), alarmArtificial.getPageSize());
        Page<AlarmArtificial> pageInfo = alarmArtificialDao.select(alarmArtificial);
        for (AlarmArtificial alarmArtificial1 : pageInfo) {
            SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmArtificial1.getCapsuleId(), SpaceManage.class);
            if (spaceManage != null) {
                alarmArtificial1.setCapsuleName(spaceManage.getName());
            }
            spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmArtificial1.getSpaceId(), SpaceManage.class);
            if (spaceManage != null) {
                alarmArtificial1.setSpaceName(spaceManage.getName());
            }
            alarmArtificial1.setAlarmTypeName(AlarmArtificialEnum.getName(alarmArtificial1.getAlarmLevel()));
        }
        return pageInfo;
    }

    @Override
    public Object reportByTime(Integer alarmType, String timeType) {
        JSONObject result = new JSONObject();
        try {
            if (StringUtils.isBlank(timeType)) {
                timeType = "month";
            }
            String startTime = StringUtils.EMPTY;
            String endTime = StringUtils.EMPTY;
            List<String> dates = new ArrayList<>();
            List<String> times = new ArrayList<>();
            List<Integer> values = new ArrayList<>();
            if ("month".equals(timeType)) {
                startTime = TimeUtil.beforeYearBegin();
                endTime = TimeUtil.thisMonthBegin();
                JSONObject jsonByType = new JSONObject();
                for (Integer level = 1; level <= 5; level++) {
                    Integer count = alarmArtificialDao.getCount(level, alarmType, null, null);
                    jsonByType.put(level.toString(), count);
                }
                result.put("pieChart", jsonByType);
                dates = TimeUtil.getBetweenMonth(startTime, endTime);
                for (String day : dates) {
                    endTime = TimeUtil.nextMonth(day);
                    times.add(day.substring(0, 7));
                    values.add(alarmArtificialDao.getCount(null, alarmType, day, endTime));
                }
            }
            if ("year".equals(timeType)) {
                startTime = TimeUtil.getYear(4, true);
                endTime = TimeUtil.thisYearBegin();
                JSONObject jsonByType = new JSONObject();
                for (Integer level = 1; level <= 5; level++) {
                    Integer count = alarmArtificialDao.getCount(level, alarmType, null, null);
                    jsonByType.put(level.toString(), count);
                }
                result.put("pieChart", jsonByType);
                dates = TimeUtil.getBetweenYear(startTime, endTime);
                for (String day : dates) {
                    endTime = TimeUtil.nextYear(day);
                    times.add(day.substring(0, 4));
                    values.add(alarmArtificialDao.getCount(null, alarmType, day, endTime));
                }
            }
            result.put("times", times);
            result.put("values", values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Object linkedDeviceData(Long capsuleId, Integer alarmType) {
        JSONObject result = new JSONObject();
        if (jsonObject.isEmpty()) {
            jsonObject = JSONObject.parseObject(alarmType2Device);
        }
        String alarmName = AlarmArtificialEnum.getType(alarmType).toString();
        if (!jsonObject.containsKey(alarmName)) {
            return result;
        }
        List<String> deviceTypeList = (List) jsonObject.get(alarmName);
        for (String deviceType : deviceTypeList) {
            Long currentTime = new Date().getTime() - (120 * 1000L);
            DeviceDisplay deviceDisplay = new DeviceDisplay();
            deviceDisplay.setType(deviceType);
            deviceDisplay.setCapsule(capsuleId);
            List<DeviceDisplay> deviceDataList = mongoTemplateUtil.findListByVo(deviceDisplay, currentTime, null, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            result.put(deviceType, deviceDataList);
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        JSONObject test = JSONObject.parseObject("{'FIRE':['atmospheric','oxygen','methane'],'RAINFALL':['gauge'],'SETTLEMENT':['structural']}");
        List<String> deviceTypeList = (List) test.get("FIRE");
        System.out.println(deviceTypeList);
    }
}

