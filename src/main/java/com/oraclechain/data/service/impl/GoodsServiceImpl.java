package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.GoodsDao;
import com.oraclechain.data.entity.Goods;
import com.oraclechain.data.entity.GoodsLibrary;
import com.oraclechain.data.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public Page<Goods> getAllGoods(String type, Integer libraryId, String name, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return goodsDao.getAllGoods(type, libraryId, name);
    }

    @Override
    public Goods getById(Integer id) {
        return goodsDao.getById(id);
    }

    @Override
    public void insertGoods(Goods goods) {
        goodsDao.insertGoods(goods);
    }

    @Override
    public void updateGoods(Goods goods) {
        goodsDao.updateGoods(goods);
    }

    @Override
    public void deleteGoods(Integer id) {
        goodsDao.deleteGoods(id);
    }

    @Override
    public List<GoodsLibrary> getAllGoodsLibrary() {
        return goodsDao.getAllGoodsLibrary();
    }

    @Override
    public List<GoodsLibrary> getGoodsLibraryByPid(Integer pid) {
        return goodsDao.getGoodsLibraryByPid(pid);
    }

    @Override
    public void insertGoodsLibrary(GoodsLibrary goodsLibrary) {
        goodsDao.insertGoodsLibrary(goodsLibrary);
    }

    @Override
    public void updateGoodsLibrary(GoodsLibrary goodsLibrary) {
        goodsDao.updateGoodsLibrary(goodsLibrary);
    }

    @Override
    public void deleteGoodsLibrary(Integer id) {
        goodsDao.deleteGoodsLibrary(id);
    }
}
