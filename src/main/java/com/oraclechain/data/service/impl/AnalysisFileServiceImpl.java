package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.AnalysisFileDao;
import com.oraclechain.data.entity.AnalysisFile;
import com.oraclechain.data.service.AnalysisFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
@Service
public class AnalysisFileServiceImpl implements AnalysisFileService {

    @Autowired
    private AnalysisFileDao analysisFileDao;

    @Override
    public Page<AnalysisFile> getFiles(String planName, String fileName, String uploader, String auditResult, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return analysisFileDao.getFiles(planName, fileName, uploader, auditResult);
    }

    @Override
    public void deleteFile(Integer id) {
        analysisFileDao.deleteFile(id);
    }

    @Override
    public void insertFile(AnalysisFile analysisFile) {
        analysisFileDao.insertFile(analysisFile);
    }

    @Override
    public void updateFile(AnalysisFile analysisFile) {
        analysisFileDao.updateFile(analysisFile);
    }

    @Override
    public AnalysisFile getById(Integer id) {
        return analysisFileDao.getById(id);
    }
}
