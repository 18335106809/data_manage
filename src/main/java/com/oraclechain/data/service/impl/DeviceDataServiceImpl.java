package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.DeviceMinuteEnum;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.DeviceDataService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.StringToArrayUtil;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.jaxb.SourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.crypto.NullCipher;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
@Service
public class DeviceDataServiceImpl implements DeviceDataService {

    static Logger logger = LoggerFactory.getLogger(DeviceDataServiceImpl.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private ProviderClient providerClient;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @Value("${device.type.waterpump}")
    private String waterpump;

    @Value("${device.type.fan}")
    private String fan;

    @Value("${device.type.light}")
    private String light;

    @Value("${device.type.smoke}")
    private String smoke;

    @Value("${device.type.cover}")
    private String cover;

    @Value("${device.type.hall-current-sensor}")
    private String hall;

    @Value("${device.type.hall.AV}")
    private String av;

    @Value("${device.type.hall.BV}")
    private String bv;

    @Value("${device.type.hall.CV}")
    private String cv;

    @Value("${device.type.hall.AI}")
    private String ai;

    @Value("${device.type.hall.BI}")
    private String bi;

    @Value("${device.type.hall.CI}")
    private String ci;

    @Value("${device.type.hall.AE}")
    private String ae;

    @Value("${device.type.hall.RE}")
    private String re;

    @Value("${device.type.hall.AP}")
    private String ap;

    @Value("${device.type.hall.BP}")
    private String bp;

    @Value("${device.type.hall.CP}")
    private String cp;

    @Value("${device.type.hall.ALLP}")
    private String allp;

    @Value("${device.type.hall.ALLE}")
    private String alle;

    @Override
    public List<Device> realTimeData(List<Object> ids, Long capsuleId, String type) {
        List<Device> devices = new ArrayList<>();
        for (Object id : ids) {
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + id, Device.class);
            if (device != null) {
                String parameter = device.getParameters();
                List<String> parameters = StringToArrayUtil.toArray(parameter);
                JSONObject jsonObject = new JSONObject();
                if (!device.getCapsuleGroup().equals("")) {
                    List<Long> capsuleIds = JSON.parseArray(device.getCapsuleGroup(), Long.class);
                    String capsuleLocation = "";
                    for (Long capsuleIdTemp : capsuleIds) {
                        SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + capsuleIdTemp, SpaceManage.class);
                        if (spaceManage != null && StringUtils.isNotBlank(spaceManage.getName())) {
                            capsuleLocation += ("," + spaceManage.getName());
                        }
                    }
                    if (capsuleLocation.length() > 0) {
                        capsuleLocation = capsuleLocation.substring(1).trim();
                    }
                    device.setCapsuleLocation(capsuleLocation);
                }
                for (String paramTem : parameters) {
                    Object realValue = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + id, paramTem);
                    if (realValue != null) {
                        if (device.getType().trim().contains(light) || device.getType().trim().contains(fan) || device.getType().trim().equals(waterpump)
                                || device.getType().trim().equals(cover)) {
                            jsonObject.put(paramTem, (int) Double.parseDouble(realValue.toString()));
                        } else {
                            jsonObject.put(paramTem, Double.parseDouble(realValue.toString()));
                        }
                    }
                    if (device.getType().trim().equals(smoke)) {
                        JSONObject json = JSONObject.parseObject(device.getThreshold());
                        if (json != null) {
                            jsonObject.put(paramTem, json.getBoolean(smoke));
                        } else {
                            jsonObject.put(paramTem, false);
                        }
                    }
                }
                if (device.getType().equals(hall)) {
                    setOtherData(device);
                } else {
                    device.setValue(jsonObject);
                }
                devices.add(device);
            }
        }
        redisGetDeviceService.setCapsuleInfoList(devices);
        return devices;
    }

    @Override
    public Device setOtherData(Device device) {
        if (device.getType().equals(hall)) {
            Float avValue = getValue(device.getId(), av);
            Float bvValue = getValue(device.getId(), bv);
            Float cvValue = getValue(device.getId(), cv);
            Float aiValue = getValue(device.getId(), ai);
            Float biValue = getValue(device.getId(), bi);
            Float ciValue = getValue(device.getId(), ci);
            Float aeValue = getValue(device.getId(), ae);
            Float reValue = getValue(device.getId(), re);
            Float apValue = getValue(avValue, aiValue, device.getId(), ap);
            Float bpValue = getValue(bvValue, biValue, device.getId(), bp);
            Float cpValue = getValue(cvValue, ciValue, device.getId(), cp);
            Float allPValue = (apValue == null ? 0 : apValue) + (bpValue == null ? 0 : bpValue) + (cpValue == null ? 0 : cpValue);
            Float allEValue = (aeValue == null ? 0 : aeValue) + (reValue == null ? 0 : reValue);
            if (allPValue != null && allPValue > 0) {
                redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), allp, allPValue.toString());
            }
            if (allEValue != null && allEValue > 0) {
                redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), alle, allEValue.toString());
            }
            DeviceCommon deviceCommon = new DeviceCommon(device);
            JSONObject jsonObject1 = new JSONObject();
            for (String data : deviceCommon.getParams()) {
                Object realValue = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                if (realValue != null) {
                    jsonObject1.put(data, Double.valueOf(realValue.toString()));
                }
            }
            device.setValue(jsonObject1);
        }
        return device;
    }

    public Float getValue(Float v, Float i, Long deviceId, String dataKey) {
        Float p = null;
        if (v != null  && i != null) {
            p = v * i;
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey, p.toString());
        }
        return p;
    }

    public Float getValue(Long deviceId, String dataKey) {
        if (redisTemplate.opsForHash().hasKey(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey)) {
            return Float.valueOf(redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey).toString());
        } else {
            return null;
        }
    }

    @Override
    public List<DeviceDisplay> historyData(Long id, String startTime, String endTime) {
        logger.info("查询deviceId: " + id + ", startTime: " + startTime + ", endTime: " + endTime);
        List<DeviceDisplay> result = new ArrayList<>();
        try {
            DeviceDisplay deviceDisplay = new DeviceDisplay();
            deviceDisplay.setDeviceId(id);
            Long start = TimeUtil.str2Long(startTime);
            Long end = TimeUtil.str2Long(endTime);
            List<DeviceDisplay> deviceList = mongoTemplateUtil.findListByVo(deviceDisplay, start, end, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            result.addAll(deviceList);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    @Override
    public List<DeviceDisplay> minuteData(Object capsuleId, String type, Object deviceId, Long time) {
        logger.info("查询一分钟之内" + type + "的数据");
        List<DeviceDisplay> result = new ArrayList<>();
        try {
            // 一分钟
            DeviceDisplay deviceDisplay = new DeviceDisplay();
            if (capsuleId != null) {
                deviceDisplay.setCapsule(Long.parseLong(capsuleId.toString()));
            }
            if (StringUtils.isNotBlank(type)) {
                deviceDisplay.setType(DeviceMinuteEnum.getDeviceType(type.trim()));
            }
            if (deviceId != null) {
                deviceDisplay.setDeviceId(Long.parseLong(deviceId.toString()));
            }
            Long currentTime = new Date().getTime() - (300 * 1000L);
            if (time != null && time > 0) {
                currentTime = time - (240 * 1000L);
            }
            List<DeviceDisplay> deviceList = mongoTemplateUtil.findListByVo(deviceDisplay, currentTime, time, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            result.addAll(deviceList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void setData(Long id, String key, Object value) {
        if (StringUtils.isNotBlank(key) && value != null) {
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + id, key, value.toString());
        }
    }

    @Override
    public Object getDateByReal(String dataKey) {
        List<JSONObject> result = new ArrayList<>();
        String deviceType = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_PARAMETER_KEY + dataKey);
        Map<String, Object> param = new HashMap<>();
        param.put("type", deviceType);
        if (StringUtils.isBlank(deviceType)) {
            return result;
        }
        List<JSONObject> feignSpaceList =  ParameterUtil.getParameter(JSONObject.parseObject(providerClient.getSpaceManage()), "data", List.class);
        if (CollectionUtils.isEmpty(feignSpaceList)) {
            return result;
        }
        for (JSONObject spaceManageJSON : feignSpaceList) {
            SpaceManage spaceManage = JSONObject.parseObject(spaceManageJSON.toJSONString(), SpaceManage.class);
            JSONObject jsonObject = new JSONObject();
            List<SpaceManage> subSpaceManageList = spaceManage.getSubSpaceManage();
            List<String> capsuleList = new ArrayList<>();
            List<Double> capsuleValue = new ArrayList<>();
            if (CollectionUtils.isEmpty(subSpaceManageList)) {
                continue;
            }
            for (SpaceManage spaceManage1 : subSpaceManageList) {
                capsuleList.add(spaceManage1.getName());
                param.put("capsule", spaceManage1.getId());
                JSONObject feignResult = JSONObject.parseObject(providerClient.getDeviceByType(param));
                List<JSONObject> deviceJSONList = ParameterUtil.getParameter(feignResult, "data", List.class);
                Double sum = 0.0;
                if (CollectionUtils.isEmpty(deviceJSONList)) {
                    capsuleValue.add(sum);
                } else {
                    for (JSONObject deviceJSON : deviceJSONList) {
                        Device device = JSONObject.parseObject(deviceJSON.toJSONString(), Device.class);
                        if (device != null) {
                            Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), dataKey);
                            if (value != null) {
                                sum += Double.valueOf(value.toString());
                            }
                        }
                    }
                    capsuleValue.add(Double.valueOf(decimalFormat.format(sum / deviceJSONList.size())));
                }
            }
            jsonObject.put("spaceName", spaceManage.getName());
            jsonObject.put("capsuleList", capsuleList);
            jsonObject.put("capsuleValue", capsuleValue);
            result.add(jsonObject);
        }
        return result;
    }
}
