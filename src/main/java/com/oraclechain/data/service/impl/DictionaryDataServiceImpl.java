package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.dao.DictionaryDataDao;
import com.oraclechain.data.entity.DictionaryData;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.DictionaryDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/16
 */
@Service
public class DictionaryDataServiceImpl implements DictionaryDataService {

    @Autowired
    private DictionaryDataDao dictionaryDataDao;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void insert(DictionaryData dictionaryData) {
        dictionaryDataDao.insert(dictionaryData);
        updateCache();
    }

    @Override
    public void update(DictionaryData dictionaryData) {
        dictionaryDataDao.update(dictionaryData);
        updateCache();
    }

    @Override
    public void delete(Integer id) {
        dictionaryDataDao.delete(id);
        updateCache();
    }

    @Override
    public List<DictionaryData> select(DictionaryData dictionaryData) {
        List<DictionaryData> result = new ArrayList<>();
        List<Object> values = redisTemplate.opsForHash().values(RedisKeyConfig.DEVICE_DATAKEY_DIC);
        if (CollectionUtils.isEmpty(values)) {
            for (Object value : values) {
                DictionaryData data = JSONObject.parseObject(value.toString(), DictionaryData.class);
                result.add(data);
            }
        } else {
            result.addAll(dictionaryDataDao.select(dictionaryData));
        }
        return result;
    }

    @Override
    public void updateCache() {
        DictionaryData dictionaryData = new DictionaryData();
        List<DictionaryData> list = dictionaryDataDao.select(dictionaryData);
        for (DictionaryData data : list) {
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_DATAKEY_DIC, data.getId().toString(), JSONObject.toJSONString(data));
        }
    }
}
