package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.DocumentDao;
import com.oraclechain.data.entity.Document;
import com.oraclechain.data.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/28
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentDao documentDao;

    @Override
    public Page<Document> getDocuments(Integer type, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return documentDao.getDocuments(type);
    }

    @Override
    public Document getById(Integer id) {
        return documentDao.getById(id);
    }

    @Override
    public void insertDocument(Document document) {
        documentDao.insertDocument(document);
    }

    @Override
    public void updateDocument(Document document) {
        documentDao.updateDocument(document);
    }

    @Override
    public void deleteDocument(Integer id) {
        documentDao.deleteDocument(id);
    }
}
