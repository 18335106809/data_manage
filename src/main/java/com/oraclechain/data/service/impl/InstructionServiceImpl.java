package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.dao.AlarmSOPDao;
import com.oraclechain.data.dao.InstructionDao;
import com.oraclechain.data.entity.AlarmSOP;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.Instruction;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.CommonService;
import com.oraclechain.data.service.InstructionService;
import com.oraclechain.data.util.ParameterUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/23
 */
@Service
public class InstructionServiceImpl implements InstructionService {

    static Logger logger = LoggerFactory.getLogger(InstructionServiceImpl.class);

    @Autowired
    private InstructionDao instructionDao;

    @Value("${instruction.control}")
    private String instructionControl;

    @Value("${instruction.auto}")
    private String auto;

    @Value("${instruction.open}")
    private String open;

    @Autowired
    private CommonService commonService;

    @Autowired
    private RedisGetDeviceServiceImpl redisGetDeviceService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AlarmSOPDao alarmSOPDao;

    @Autowired
    private ProviderClient providerClient;

    @Override
    public void alarmHandling(Object deviceId) {

    }

    @Override
    public void openInstruction(Long deviceId, Long capsuleId, String deviceType, String reason, Integer alarmId, Integer openOrClose) {
        // 根据报警设备所在区域获取对应SOP设备
        List<Device> deviceList = getInstructionDevice(capsuleId, deviceType);
        logger.info("--------------------" + deviceList);
        for (Device device : deviceList) {
            // 判断是自动还是手动
            DeviceCommon deviceCommon = new DeviceCommon(device);
            Integer isAuto = 0;
            Integer isOpen = 0;
            for (String key : deviceCommon.getParams()) {
                if (key.equals(auto)) {
                    isAuto = Integer.valueOf(redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), auto).toString());
                }
                if (key.equals(open)) {
                    isOpen = Integer.valueOf(redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), open).toString());
                }
            }
            if (isAuto == 1 && openOrClose == 1) {
                // 下发相应指令
                alarmSOPDao.insertSOP(new AlarmSOP(alarmId, "准备打开" + device.getName(), new Date()));

                // TODO: 2020/6/29 下发打开指令
                rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, JSONObject.toJSONString(device));
                // 修改SOP设备控制的环境健康设备的集合
                redisTemplate.opsForSet().add(RedisKeyConfig.DEVICE_INSTRUCTION_KEY + device.getId(), deviceId.toString());
                // 修改设备的状态
                redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), open, "1");
                // 下发指令完成
                alarmSOPDao.insertSOP(new AlarmSOP(alarmId, device.getName() + "已经打开", new Date()));
            }
            if (isAuto == 1 && isOpen == 1 && openOrClose != 1) {
                // 下发相应指令
                alarmSOPDao.insertSOP(new AlarmSOP(alarmId, "准备关闭" + device.getName(), new Date()));

                // TODO: 2020/8/10 如果有其它环境数据触发了该设备，需要判断下是否关闭该设备
                if (redisTemplate.opsForSet().isMember(RedisKeyConfig.DEVICE_INSTRUCTION_KEY + device.getId(), deviceId.toString())) {
                    redisTemplate.opsForSet().remove(RedisKeyConfig.DEVICE_INSTRUCTION_KEY + device.getId(), deviceId.toString());
                }
                if (redisTemplate.opsForSet().size(RedisKeyConfig.DEVICE_INSTRUCTION_KEY + device.getId()) > 0) {
                    // 还有其它环境设备依赖该SOP设备
                } else {
                    // TODO: 2020/6/29 下发关闭指令
                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_CLOSE, JSONObject.toJSONString(device));
                    // 修改设备的状态
                    redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), open, "0");
                    // 下发指令完成
                    alarmSOPDao.insertSOP(new AlarmSOP(alarmId, device.getName() + "已经关闭", new Date()));
                }
            }

            // 指令次数做记录
            Long spaceId = 0L;
            SpaceManage spaceManage = redisGetDeviceService.getSpaceById(device.getCapsule());
            if (spaceManage != null) {
                spaceId = spaceManage.getParentId();
            }
            Instruction instruction = new Instruction(device.getId(), device.getType(), device.getCapsule(), spaceId, new Date(), reason);
            instructionDao.insertInstruction(instruction);
        }

    }

    @Override
    public void closeInstruction(Long capsuleId, String deviceType, Integer alarmId) {
        List<Device> deviceList = getInstructionDevice(capsuleId, deviceType);
        alarmSOPDao.insertSOP(new AlarmSOP(alarmId, "已恢复正常", new Date()));
        alarmSOPDao.insertSOP(new AlarmSOP(alarmId, "准备关闭...", new Date()));
    }

    @Override
    public void insertInstruction(Instruction instruction) {
        instructionDao.insertInstruction(instruction);
    }

    @Override
    public Integer countTimes(Long spaceId, Long capsuleId, String deviceType, String startTime, String endTime) {
        return instructionDao.countTimes(spaceId, capsuleId, deviceType, startTime, endTime);
    }

    public List<Device> getInstructionDevice(Long capsuleId, String deviceType) {
        List<Device> deviceList = new ArrayList<>();
        String instructionType = StringUtils.EMPTY;
        JSONObject instructionJson = JSONObject.parseObject(instructionControl);
        for (String key : instructionJson.keySet()) {
            if (key.contains(deviceType)) {
                instructionType = instructionJson.getString(key);
            }
        }
        if (StringUtils.isEmpty(instructionType)) {
            return deviceList;
        }
        List<JSONObject> deviceJSON = commonService.getDeviceByType(instructionType);
        for (JSONObject json : deviceJSON) {
            Device instructionDevice = JSONObject.parseObject(json.toJSONString(), Device.class);
            if (instructionDevice.getCapsule().equals(capsuleId)) {
                deviceList.add(instructionDevice);
            }
        }
        return deviceList;
    }

    @Override
    public void bulkInstruction(Long capsuleId, String deviceType, String dataKey, Integer status) {
//        Map<String, Object> param = new HashMap<>();
//        param.put("type", deviceType);
//        param.put("capsule", capsuleId);
        try {
            JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceBySpaceType(capsuleId.toString(), deviceType));
            List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
            for (JSONObject json : deviceJSONList) {
                Device device = JSONObject.parseObject(json.toJSONString(), Device.class);
                redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), dataKey, String.valueOf(status));
                if (device != null) {
                    device.setOrderDataKey(dataKey);
                    if (status.equals(0)) {
                        rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_CLOSE, JSONObject.toJSONString(device));
                    } else if (status.equals(1)) {
                        rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, JSONObject.toJSONString(device));
                    }
                }
            }
        } catch (Exception e) {
            logger.error("批量下发指令出错");
            e.printStackTrace();
        }
    }

    @Override
    public void singleInstruction(Object deviceId, String key, Integer value) {
        try {
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + deviceId, Device.class);
            if (device != null) {
                device.setOrderDataKey(key);
                if (value.equals(0)) {
                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_CLOSE, JSONObject.toJSONString(device));
                } else if (value.equals(1)) {
                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, JSONObject.toJSONString(device));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下发单个指令出错，deviceId:" + deviceId);
        }
    }
}
