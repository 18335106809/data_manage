package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.dao.AlarmSOPDao;
import com.oraclechain.data.entity.AlarmSOP;
import com.oraclechain.data.entity.DeviceMinuteEnum;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.service.AutoReportService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/20
 */
@Service
public class AutoReportServiceImpl implements AutoReportService {

    @Autowired
    private AlarmSOPDao alarmSOPDao;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private ProviderClient providerClient;

    @Value("${daily.auto.report.device.type}")
    private String autoReportKey;

    @Override
    public List<DeviceDisplay> getDeviceData(Object deviceId, Integer alarmId) {
        List<DeviceDisplay> deviceList = new ArrayList<>();
        List<AlarmSOP> alarmSOPList = alarmSOPDao.getSOPByAlarmId(alarmId);
        if (alarmSOPList.size() > 2) {
            AlarmSOP alarmSOPLast = alarmSOPList.get(alarmSOPList.size() - 1);
            if (alarmSOPLast.getDescription().equals("报警解除")) {
                AlarmSOP alarmSOPStart = alarmSOPList.get(0);
                Long startTime = alarmSOPStart.getCreateTime().getTime() - (60 * 1000);
                Long endTime = alarmSOPLast.getCreateTime().getTime() + (60 * 1000);
                DeviceDisplay deviceDisplay = new DeviceDisplay();
                if (deviceId != null) {
                    deviceDisplay.setDeviceId(Long.parseLong(deviceId.toString()));
                }
                deviceList = mongoTemplateUtil.findListByVo(deviceDisplay, startTime, endTime, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            }
        }
        return deviceList;
    }

    @Override
    public Object getDataByHour(String startTime, String endTime) {
        // 判断起止时间是否为null, 如果为null则默认为当天
        if (StringUtils.isBlank(startTime)) {
            startTime = TimeUtil.todayBegin();
        }
        if (StringUtils.isBlank(endTime)) {
            endTime = TimeUtil.nowHour();
        }
        List<String> times = TimeUtil.getBetweenHour(startTime, endTime);
        String[] deviceTypes = autoReportKey.split(",");
        JSONObject result = new JSONObject();
        result.put("time", times);
        List<JSONObject> datas = new ArrayList<>();
        try {
            List<JSONObject> feignSpaceList = ParameterUtil.getParameter(JSONObject.parseObject(providerClient.getSpaceManage()), "data", List.class);
            if (!CollectionUtils.isEmpty(feignSpaceList)) {
                for (JSONObject jsonObject : feignSpaceList) {
                    SpaceManage spaceManage = JSONObject.parseObject(jsonObject.toJSONString(), SpaceManage.class);
                    List<SpaceManage> spaceManageList = spaceManage.getSubSpaceManage();
                    List<Long> capsuleIds = new ArrayList<>();
                    if (!CollectionUtils.isEmpty(spaceManageList)) {
                        for (SpaceManage spaceManage1 : spaceManageList) {
                            Long capsuleId = spaceManage1.getId();
                            capsuleIds.add(capsuleId);
                        }
                    }
                    if (CollectionUtils.isEmpty(capsuleIds)) {
                        continue;
                    }
                    for (String key : deviceTypes) {
                        String deviceType = key.substring(0, key.indexOf(":"));
                        // TODO: 2020/7/29 判断该舱下是否有此类型的设备
                        String dataKey = key.substring(key.indexOf(":") + 1);
                        String[] dataKeyArray = dataKey.split("/");
                        for (String data : dataKeyArray) {
                            JSONObject keyJSON = new JSONObject();
                            List<Double> values = new ArrayList<>();
                            for (String time : times) {
                                Long timestamp = TimeUtil.str2Long(time);
                                Long endTimestamp = timestamp + (60 * 1000);
                                Double value = mongoTemplateUtil.getAvg(capsuleIds, timestamp, endTimestamp, deviceType, data);
                                values.add(value);
                            }
                            keyJSON.put("spaceId", spaceManage.getId());
                            keyJSON.put("spaceName", spaceManage.getName());
                            keyJSON.put("dataKey", data);
                            keyJSON.put("values", values);
                            datas.add(keyJSON);
                        }
                    }
                }
            }
            result.put("data", datas);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        String type = "oxygen:oxygen,methane:methane,atmospheric:temperature/humidity,gauge:level";
        String[] keys = type.split(",");
        for (String key : keys) {
            String deviceType = key.substring(0, key.indexOf(":"));
            String dataKey = key.substring(key.indexOf(":") + 1);
            String[] s = dataKey.split("/");
            for (String ss : s) {
                System.out.println(ss);
            }
        }
    }
}
