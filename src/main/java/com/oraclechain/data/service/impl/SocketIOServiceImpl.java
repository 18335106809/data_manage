package com.oraclechain.data.service.impl;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.oraclechain.data.config.SocketIOConfig;
import com.oraclechain.data.service.MockDataService;
import com.oraclechain.data.service.SocketIOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/26
 */
@Service(value = "socketIOService")
public class SocketIOServiceImpl implements SocketIOService {

    static Logger logger = LoggerFactory.getLogger(SocketIOServiceImpl.class);

    // 用来存已连接的客户端
    private static Map<String, List<SocketIOClient>> clientMap = new ConcurrentHashMap<>();

    private static Map<String, Thread> threadMap = new ConcurrentHashMap<>();

    @Autowired
    private SocketIOServer socketIOServer;

    @Autowired
    private MockDataService mockDataService;

    /**
     * Spring IoC容器创建之后，在加载SocketIOServiceImpl Bean之后启动
     * @throws Exception
     */
    @PostConstruct
    private void autoStartup() throws Exception {
//        start();
    }

    /**
     * Spring IoC容器在销毁SocketIOServiceImpl Bean之前关闭,避免重启项目服务端口占用问题
     * @throws Exception
     */
    @PreDestroy
    private void autoStop() throws Exception  {
        stop();
    }

    @Override
    public void start() {
        // 监听客户端连接
        socketIOServer.addConnectListener(client -> {
            logger.info("-------------new socket connection");
            connectionClient(client);
        });

        // 监听客户端断开连接
        socketIOServer.addDisconnectListener(client -> {
            logger.info("-------------dis socket connection");
            disConnectionClient(client);
        });

        // 处理自定义的事件，与连接监听类似
//        socketIOServer.addEventListener(PUSH_EVENT, PushMessage.class, (client, data, ackSender) -> {
//            // TODO do something
//        });
        socketIOServer.start();
    }

    @Override
    public void stop() {
        if (socketIOServer != null) {
            socketIOServer.stop();
            socketIOServer = null;
        }
    }

    @Override
    public void pushMessage(String event, String message) {
        List<SocketIOClient> clientList = clientMap.get(event);
        for (SocketIOClient client : clientList) {
            client.sendEvent(event, message);
        }
    }

    /**
     * 此方法为获取client连接中的参数，可根据需求更改
     * @param client
     * @return
     */
    private void connectionClient(SocketIOClient client) {
        Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
        List<String> eventList = params.get("eventName");
        List<String> connectionList = params.get("connectionId");
        List<String> pipeList = params.get("pipeId");
        String eventName = "";
        String connectionId = "";
        Long pipeId = 0L;
        if (!CollectionUtils.isEmpty(eventList)) {
            eventName = eventList.get(0);
        }
        if (!CollectionUtils.isEmpty(connectionList)) {
            connectionId = connectionList.get(0);
        }
        if (!CollectionUtils.isEmpty(pipeList)) {
            pipeId = Long.parseLong(pipeList.get(0));
        }
        logger.info("---------------------pipeId : " + pipeId);
        if (!StringUtils.isEmpty(eventName) && !StringUtils.isEmpty(connectionId)) {
            if (clientMap.containsKey(eventName)) {
                clientMap.get(eventName).add(client);
            } else {
                List<SocketIOClient> clients = new ArrayList<>();
                clients.add(client);
                clientMap.put(eventName, clients);
            }
        }
        logger.info("============connectionId:" + connectionId);
        startThread(eventName, connectionId, pipeId);
    }

    public void startThread(String eventName, String connectionId, Long pipeId) {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    if(Thread.currentThread().isInterrupted()){
                        break;
                    }
                    pushMessage(eventName, mockDataService.switchData(eventName, pipeId));
                    try {
                        Thread.currentThread().sleep(5000l);
                    } catch (Exception e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        };
        thread.start();
        threadMap.put(connectionId, thread);
    }

    private void disConnectionClient(SocketIOClient client) {
        Map<String, List<String>> params = client.getHandshakeData().getUrlParams();
        List<String> eventList = params.get("eventName");
        String eventName = "";
        if (!CollectionUtils.isEmpty(eventList)) {
            eventName = eventList.get(0);
        }
        List<String> connectionList = params.get("connectionId");
        String connectionId = "";
        if (!CollectionUtils.isEmpty(connectionList)) {
            connectionId = connectionList.get(0);
        }
        logger.info("============dis connectionId:" + connectionId);
        if (!StringUtils.isEmpty(connectionId) && threadMap.containsKey(connectionId)) {
            threadMap.get(connectionId).interrupt();
            threadMap.remove(connectionId);
        }
        if (!StringUtils.isEmpty(eventName) && clientMap.containsKey(eventName)) {
            clientMap.get(eventName).remove(client);
            client.disconnect();
        }
    }

    @Override
    public List<SocketIOClient> getClients(String eventName) {
        if (clientMap.containsKey(eventName)) {
            return clientMap.get(eventName);
        } else {
            return new ArrayList<>();
        }
    }
}
