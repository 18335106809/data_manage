package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.dao.AlarmInfoDao;
import com.oraclechain.data.dao.AlarmSOPDao;
import com.oraclechain.data.entity.*;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.AlarmInfoService;
import com.oraclechain.data.service.InstructionService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
@Service
public class AlarmInfoServiceImpl implements AlarmInfoService {

    @Autowired
    private AlarmInfoDao alarmDao;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private InstructionService instructionService;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private AlarmSOPDao alarmSOPDao;

    @Autowired
    private ProviderClient providerClient;

    @Value("${device.type.structural}")
    private String structural;

    @Value("${device.type.atmospheric}")
    private String atmospheric;

    @Value("${device.type.oxygen}")
    private String oxygen;

    @Value("${device.type.methane}")
    private String methane;

    @Value("${device.type.gauge}")
    private String gauge;

    @Value("${device.type.atmospheric.temperature}")
    private String temperature;

    @Value("${device.type.atmospheric.humidity}")
    private String humidity;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @Override
    public Page<AlarmInfo> getAllAlarm(Integer pageNum, Integer pageSize, String deviceType, Integer level, String type, Long capsuleId, Long segmentId, String startTime, String endTime) {
        PageHelper.startPage(pageNum, pageSize);
        Integer alarmType = AlarmType.getIndex(type);
        Page<AlarmInfo> alarmInfoPage = alarmDao.getAllAlarm(deviceType, level, alarmType, capsuleId, segmentId, startTime, endTime);
        for (AlarmInfo alarmInfo : alarmInfoPage) {
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + alarmInfo.getDeviceId(), Device.class);
            if (device != null) {
                alarmInfo.setDeviceName(device.getName());
            }
            SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmInfo.getCapsuleId(), SpaceManage.class);
            if (spaceManage != null) {
                alarmInfo.setCapsuleName(spaceManage.getName());
            }
            SpaceManage spaceManage2 = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmInfo.getSegmentId(), SpaceManage.class);
            if (spaceManage != null) {
                alarmInfo.setSegmentName(spaceManage2.getName());
            }
            alarmInfo.setTypeName(AlarmType.getName(alarmInfo.getType()));
        }
        return alarmInfoPage;
    }

    @Override
    public List<AlarmInfo> getTodayAlarm(String deviceType, Integer level, String type, Long capsuleId, Long segmentId, String startTime, String endTime) {
        Integer alarmType = AlarmType.getIndex(type);
        Page<AlarmInfo> alarmInfoPage = alarmDao.getAllAlarm(deviceType, level, alarmType, capsuleId, segmentId, startTime, endTime);
        setAlarmInfo(alarmInfoPage);
        return alarmInfoPage;
    }

    @Override
    public AlarmInfo getAlarmById(Integer id) {
        AlarmInfo alarmInfo = alarmDao.getAlarmById(id);
        if (alarmInfo != null) {
            List<AlarmInfo> alarmInfoList = new ArrayList<>();
            alarmInfoList.add(alarmInfo);
            setAlarmInfo(alarmInfoList);
            return alarmInfoList.get(0);
        }
        return alarmInfo;
    }

    @Override
    public void updateAlarm(String hurt, String accidentReason, Integer alarmId) {
        AlarmInfo alarmInfo = new AlarmInfo();
        alarmInfo.setHurt(hurt);
        alarmInfo.setAccidentReason(accidentReason);
        alarmInfo.setId(alarmId);
        alarmDao.updateAlarm(alarmInfo);
    }

    @Override
    public List<AlarmInfo> getAlarmByIdList(List<Integer> ids, Long spaceId, Long capsuleId, String deviceType) {
        return alarmDao.getAlarmByIdList(ids, spaceId, capsuleId, deviceType);
    }

    @Override
    public Page<AlarmInfo> getAlarmByDeviceId(Long deviceId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return alarmDao.getAlarmByDeviceId(deviceId);
    }

    @Override
    public Integer setAlarm(Integer level, Long deviceId, Long capsuleId, Integer typeIndex, String status, String reason, String deviceType, String alarmKey, String alarmValue) {
        AlarmInfo alarm = new AlarmInfo();
        alarm.setLevel(level);
        alarm.setSegmentId(capsuleId);
        SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + capsuleId, SpaceManage.class);
        if (spaceManage != null) {
            capsuleId = spaceManage.getParentId();
        }
        alarm.setDeviceId(deviceId);
        alarm.setCapsuleId(capsuleId);
        alarm.setType(typeIndex);
        alarm.setAlarmTime(new Date());
        alarm.setStatus(status);
        alarm.setReason(reason);
        alarm.setDeviceType(deviceType);
        alarm.setAlarmValue(alarmValue);
        alarm.setAlarmKey(alarmKey);
        alarmDao.setAlarm(alarm);
        return alarm.getId();
    }

    /**
     * @param device      报警设备
     * @param alarmReason 报警原因
     * @param isMock      是否模拟报警
     * @param alarmKey    具体报警数值的key（比如温湿度传感器，确定是温度或者湿度报警）
     */
    @Override
    @Async
    public void currentAlarm(Device device, String alarmReason, Boolean isMock, String alarmKey, String alarmValue, Integer alarmLevel) {
        if (!redisTemplate.hasKey(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + alarmKey)) {
            // 往数据库里添加数据
            Integer alarmType = AlarmType.ENVIRONMENT.getIndex();
            if (isMock) {
                alarmType = AlarmType.MOCK.getIndex();
            } else if (device.getType().trim().equals(structural)) {
                alarmType = AlarmType.STRUCTURAL.getIndex();
            } else if (device.getType().trim().equals(atmospheric)) {
                alarmType = AlarmType.ATMOSPHERIC.getIndex();
            } else if (device.getType().trim().equals(oxygen) || device.getType().trim().equals(methane)) {
                alarmType = AlarmType.GAS.getIndex();
            } else if (device.getType().trim().equals(gauge)) {
                alarmType = AlarmType.LEVEL.getIndex();
            } else if (device.getType().trim().equals("irrupt")) {
                alarmType = AlarmType.IRRUPT.getIndex();
            }
            Integer alarmId = setAlarm(alarmLevel, device.getId(), device.getCapsule(), alarmType, null,
                    alarmReason, device.getType(), alarmKey, alarmValue);
            // 维护当前报警集合
            setCurrentAlarm(device.getId(), alarmKey, alarmId);
            // 下发相应应对指令
            instructionService.openInstruction(device.getId(), device.getCapsule(), device.getType(), alarmReason, alarmId, 1);
            // 如果報警level為1級，啓動聲光報警器
            if (alarmLevel.equals(1)) {
                sendAlarm();
            }
        }
    }

    public void setCurrentAlarm(Long deviceId, String alarmKey, Integer alarmId) {
        redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_ALARM_KEY + deviceId + "." + alarmKey, alarmId.toString());
    }

    @Override
    public void removeAlarm(Device device, String alarmKey, Boolean isMock) {
        if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + alarmKey) && !isMock) {
            Integer alarmId = Integer.valueOf(redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + alarmKey));
            alarmSOPDao.insertSOP(new AlarmSOP(alarmId, device.getName() + "的值已恢复正常", new Date()));
            redisTemplate.delete(RedisKeyConfig.DEVICE_ALARM_KEY + device.getId() + "." + alarmKey);
            // 下发相应设备关闭的指令
            instructionService.openInstruction(device.getId(), device.getCapsule(), device.getType(), null, alarmId, 0);
            alarmSOPDao.insertSOP(new AlarmSOP(alarmId, "报警解除", new Date()));
        }
    }

    @Override
    public Integer isAlarm(Long deviceId) {
        Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_ALARM_KEY + deviceId + "*");
        if (keys.size() > 0) {
            return 1;
        }
        return null;
    }

    @Override
    public List<AlarmInfo> getCurrentAlarm(Long spaceId, Long capsuleId, String deviceType, Boolean isAid) {
        Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_ALARM_KEY + "*");
        List<Integer> ids = new ArrayList<>();
        for (String key : keys) {
            Integer alarmId = Integer.valueOf(redisTemplate.opsForValue().get(key));
            ids.add(alarmId);
        }
        List<AlarmInfo> result = new ArrayList<>();
        if (!CollectionUtils.isEmpty(ids)) {
            result = getAlarmByIdList(ids, spaceId, capsuleId, deviceType);
        }
        setAlarmInfo(result);
        if (isAid) {
            List<AlarmInfo> aidResult = new ArrayList<>();
            Set<Long> capsuleSet = new HashSet<>();
            for (AlarmInfo alarmInfo : result) {
                if (!capsuleSet.contains(alarmInfo.getSegmentId())) {
                    capsuleSet.add(alarmInfo.getSegmentId());
                    aidResult.add(alarmInfo);
                }
            }
            return aidResult;
        }

        return result;
    }

    @Override
    public Object getCurrentAlarmDeviceData(Long spaceId, Long capsuleId, String deviceType) {
        List<AlarmInfo> alarmInfoList = getCurrentAlarm(spaceId, capsuleId, deviceType, false);
        JSONObject result = new JSONObject();
        for (AlarmInfo alarmInfo : alarmInfoList) {
            Long currentTime = new Date().getTime() - (60 * 1000L);
            DeviceDisplay deviceDisplay = new DeviceDisplay();
            deviceDisplay.setDeviceId(alarmInfo.getDeviceId());
            List<DeviceDisplay> deviceDataList = mongoTemplateUtil.findListByVo(deviceDisplay, currentTime, null, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            result.put(alarmInfo.getDeviceId().toString(), deviceDataList);
        }
        return result;
    }

    @Override
    public Object getCurrentAlarmBaseData(Long spaceId, Long capsuleId, String deviceType) {
        Map<String, Object> param = new HashMap<>();
        param.put("type", atmospheric);
        param.put("capsule", capsuleId);
        JSONObject result = new JSONObject();
        JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
        List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
        Double temperatureValue = 0.0;
        Double humidityValue = 0.0;
        Double deviceTypeValue = 0.0;
        Integer personValue = 0;
        for (JSONObject json : deviceJSONList) {
            Device device = JSONObject.parseObject(json.toJSONString(), Device.class);
            DeviceCommon deviceCommon = new DeviceCommon(device);
            for (String data : deviceCommon.getParams()) {
                Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                if (value != null) {
                    if (data.equals(temperature)) {
                        temperatureValue += Double.valueOf(value.toString());
                    }
                    if (data.equals(humidity)) {
                        humidityValue += Double.valueOf(value.toString());
                    }
                }
            }
        }
        if (deviceJSONList.size() > 0) {
            result.put(temperature, decimalFormat.format(temperatureValue / deviceJSONList.size()));
            result.put(humidity, decimalFormat.format(humidityValue / deviceJSONList.size()));
        } else {
            result.put(temperature, temperatureValue);
            result.put(humidity, humidityValue);
        }

        List<AlarmInfo> alarmInfoList = getCurrentAlarm(spaceId, capsuleId, deviceType, false);
        for (AlarmInfo alarmInfo : alarmInfoList) {
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + alarmInfo.getDeviceId(), Device.class);
            DeviceCommon deviceCommon = new DeviceCommon(device);
            for (String data : deviceCommon.getParams()) {
                Object value = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                if (value != null && data.equals(deviceType)) {
                    deviceTypeValue += Double.valueOf(value.toString());
                }
            }
        }
        if (alarmInfoList.size() > 0) {
            result.put(deviceType, decimalFormat.format(deviceTypeValue / alarmInfoList.size()));
        } else {
            result.put(deviceType, deviceTypeValue);
        }
        result.put("personValue", personValue);

        return result;
    }

    @Override
    public JSONObject historyAlarmTimes(Long spaceId, String deviceType, String timeType) {
        if (StringUtils.isBlank(timeType)) {
            timeType = "month";
        }
        String startTime = StringUtils.EMPTY;
        String endTime = StringUtils.EMPTY;
        List<String> dates = new ArrayList<>();
        JSONObject result = new JSONObject();
        List<String> times = new ArrayList<>();
        List<Integer> values = new ArrayList<>();
        try {
            if (timeType.equals("month") || timeType.equals("halfYear")) {
                if (timeType.equals("month")) {
                    startTime = TimeUtil.beforeYearBegin();
                } else {
                    startTime = TimeUtil.beforeHalfYearBegin();
                }
                endTime = TimeUtil.thisMonthBegin();
                dates = TimeUtil.getBetweenMonth(startTime, endTime);
                for (String day : dates) {
                    endTime = TimeUtil.nextMonth(day);
                    times.add(day.substring(0, 7));
                    values.add(alarmDao.alarmTimes(null, spaceId, deviceType, null, day, endTime));
                }
            }
            if (timeType.equals("year")) {
                startTime = TimeUtil.getYear(4, true);
                endTime = TimeUtil.thisYearBegin();
                dates = TimeUtil.getBetweenYear(startTime, endTime);
                for (String day : dates) {
                    endTime = TimeUtil.nextYear(day);
                    times.add(day.substring(0, 4));
                    values.add(alarmDao.alarmTimes(null, spaceId, deviceType, null, day, endTime));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.put("times", times);
        result.put("values", values);
        return result;
    }

    @Override
    public Object historyCountByLevel(String deviceType, String startTime, String endTime) {
        JSONObject result = new JSONObject();
        for (int i = 1; i <= 3; i++) {
            Integer value = alarmDao.alarmTimes(null, null, deviceType, i, startTime, endTime);
            result.put(i + "", value);
        }
        return result;
    }

    public void setAlarmInfo(List<AlarmInfo> alarmInfoList) {
        for (AlarmInfo alarmInfo : alarmInfoList) {
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + alarmInfo.getDeviceId(), Device.class);
            if (device != null) {
                alarmInfo.setDevice(device);
                alarmInfo.setDeviceName(device.getName());
            }
            SpaceManage spaceManage = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmInfo.getCapsuleId(), SpaceManage.class);
            if (spaceManage != null) {
                alarmInfo.setCapsuleName(spaceManage.getName());
            }
            SpaceManage spaceManage2 = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_SPACE_KEY + alarmInfo.getSegmentId(), SpaceManage.class);
            if (spaceManage2 != null) {
                alarmInfo.setSegmentName(spaceManage2.getName());
                alarmInfo.setCapsuleNumber(spaceManage2.getNumber());
            }
            alarmInfo.setTypeName(AlarmType.getName(alarmInfo.getType()));
        }
    }

    @Override
    public List<AlarmSOP> getSOPByAlarmId(Integer alarmId) {
        return alarmSOPDao.getSOPByAlarmId(alarmId);
    }

    @Override
    public Integer alarmTimes(Long deviceId, Long spaceId, String startTime, String endTime) {
        return alarmDao.alarmTimes(deviceId, spaceId, null, null, startTime, endTime);
    }

    @Override
    public Object getAlarmMinuteData(Integer id, Boolean isRealTime) {
        AlarmInfo alarmInfo = alarmDao.getAlarmById(id);
        List<DeviceDisplay> deviceList = new ArrayList<>();
        String dataKey = "";
        JSONObject result = new JSONObject();
        try {
            if (alarmInfo != null) {
                Long endTime = new Date().getTime();
                if (!isRealTime) {
                    endTime = alarmInfo.getAlarmTime().getTime();
                }
                Long currentTime = endTime - (300 * 1000L);
                if (StringUtils.isNotBlank(alarmInfo.getAlarmKey())) {
                    dataKey = alarmInfo.getAlarmKey();
                } else {
                    dataKey = alarmInfo.getDeviceType();
                }
                DeviceDisplay deviceDisplay = new DeviceDisplay();
                deviceDisplay.setDeviceId(alarmInfo.getDeviceId());
                deviceList = mongoTemplateUtil.findListByVo(deviceDisplay, currentTime, endTime, DeviceDisplay.class, null, MongoTemplateUtil.COLLECTION_DEVICE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.put("data", deviceList);
        result.put("dataKey", dataKey);
        return result;
    }

    @Override
    public Object getAlarmCountData(Long deviceId, String startTime, String endTime) {
        JSONObject result = new JSONObject();
        List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
        List<Integer> value = new ArrayList<>();
        result.put("date", dates);
        try {
            for (String date : dates) {
                startTime = date;
                endTime = TimeUtil.nextDay(date);
                value.add(alarmDao.alarmTimes(deviceId, null, null, null, startTime, endTime));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.put("value", value);
        return result;
    }

    @Override
    public Integer alarmTimesByType(Integer alarmType, String startTime, String endTime) {
        return alarmDao.alarmTimesByType(alarmType, startTime, endTime);
    }

    @Override
    @Async
    public void sendAlarm() {
        try {
            if (!redisTemplate.hasKey(RedisKeyConfig.DEVICE_TRIGGER_ALARM)) {
                TriggerAlarm triggerAlarm = new TriggerAlarm(true);
                rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_ALARM, TopicRabbitConfig.DEVICE_ALARM, JSONObject.toJSONString(triggerAlarm));
                redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_TRIGGER_ALARM, "open", 5, TimeUnit.SECONDS);
                Thread.currentThread().sleep(5000L);
                TriggerAlarm triggerAlarm1 = new TriggerAlarm(false);
                rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_ALARM, TopicRabbitConfig.DEVICE_ALARM, JSONObject.toJSONString(triggerAlarm1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
