package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.SpaceManage;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.CommonService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/29
 */
@Service
public class CommonServiceImpl implements CommonService {

    static Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);

    @Autowired
    private ProviderClient providerClient;

    @Value("${daily.avg.device.type}")
    private String deviceType;

    @Value("${device.type.single_fan}")
    private String single_fan;

    @Value("${device.type.two_fan}")
    private String two_fan;

    @Value("${device.type.induction_fan}")
    private String induction_fan;

    @Value("${device.type.fan}")
    private String fan;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Override
    public List<JSONObject> getDeviceByType(String deviceType) {
        logger.info("get device list by invoke feign, type : " + deviceType);
        List<JSONObject> deviceList = new ArrayList<>();
        try {
            if (deviceType.equals(fan)) {
                Map<String, Object> param = new HashMap<>();
                param.put("type", single_fan);
                JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
                deviceList.addAll(ParameterUtil.getParameter(jsonObject, "data", List.class));
                param.put("type", two_fan);
                jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
                deviceList.addAll(ParameterUtil.getParameter(jsonObject, "data", List.class));
                param.put("type", induction_fan);
                jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
                deviceList.addAll(ParameterUtil.getParameter(jsonObject, "data", List.class));
            } else {
                Map<String, Object> param = new HashMap<>();
                param.put("type", deviceType);
                JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
                deviceList.addAll(ParameterUtil.getParameter(jsonObject, "data", List.class));
            }
            return  deviceList;
        } catch (Exception e) {
            e.printStackTrace();
            return  deviceList;
        }
    }

    @Override
    public List<Device> getDeviceReportList() {
        logger.info("从redis中获取设备列表");
        List<Device> result = new ArrayList<>();
        try {
            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            for (String key : keys) {
                String data = redisTemplate.opsForValue().get(key);
                Device device = JSONObject.parseObject(data, Device.class);
                if (!StringUtils.isEmpty(device.getType()) && deviceType.contains(device.getType().trim())) {
                    result.add(device);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Long getSpaceId(Long capsuleId) {
        Long spaceId = -1L;
        SpaceManage spaceManage = redisGetDeviceService.getSpaceById(capsuleId);
        if (spaceManage != null && spaceManage.getParentId() != -1) {
            spaceId = spaceManage.getParentId();
        }
        return spaceId;
    }
}
