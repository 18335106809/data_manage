package com.oraclechain.data.service.impl;

import com.oraclechain.data.dao.AlarmLevelDao;
import com.oraclechain.data.entity.AlarmLevel;
import com.oraclechain.data.service.AlarmLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/17
 */
@Service
public class AlarmLevelServiceImpl implements AlarmLevelService {

    private static Map<String, List<AlarmLevel>> map = new ConcurrentHashMap<>();

    @Autowired
    private AlarmLevelDao alarmLevelDao;

    @Override
    public void insert(AlarmLevel alarmLevel) {
        alarmLevelDao.insert(alarmLevel);
        updateLevelMap();
    }

    @Override
    public void update(AlarmLevel alarmLevel) {
        alarmLevelDao.update(alarmLevel);
        updateLevelMap();
    }

    @Override
    public void delete(Integer id) {
        alarmLevelDao.delete(id);
        updateLevelMap();
    }

    @Override
    public List<AlarmLevel> getAll() {
        return alarmLevelDao.getAll();
    }

    @Override
    public List<AlarmLevel> getLevelListByKey(String key) {
        if (CollectionUtils.isEmpty(map)) {
            updateLevelMap();
        }
        return map.get(key);
    }

    public void updateLevelMap() {
        List<AlarmLevel> alarmLevelList = getAll();
        map = new ConcurrentHashMap<>();
        for (AlarmLevel alarmLevel : alarmLevelList) {
            String key = alarmLevel.getDeviceType();
            List<AlarmLevel> alarmLevels = new ArrayList<>();
            if (map.containsKey(key)) {
                alarmLevels = map.get(key);
            }
            alarmLevels.add(alarmLevel);
            map.put(key, alarmLevels);
        }
    }
}
