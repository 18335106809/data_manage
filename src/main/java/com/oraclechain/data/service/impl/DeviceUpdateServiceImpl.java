package com.oraclechain.data.service.impl;

import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.service.DeviceUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/26
 */
@Service
public class DeviceUpdateServiceImpl implements DeviceUpdateService {

    static Logger logger = LoggerFactory.getLogger(DeviceDataServiceImpl.class);

    @Autowired
    private ProviderClient providerClient;

    @Override
    @Async("asyncServiceExecutor")
    public void updateDevice(Device device) {
        logger.info("update device status, deviceId:" + device.getId() + " status:" + device.getStatus());
        try {
            providerClient.updateDevice(device);
        } catch (Exception e) {
            logger.error("update device feign request is failed");
        }
    }
}
