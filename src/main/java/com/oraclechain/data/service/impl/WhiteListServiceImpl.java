package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.WhiteListDao;
import com.oraclechain.data.entity.WhiteList;
import com.oraclechain.data.service.WhiteListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@Service
public class WhiteListServiceImpl implements WhiteListService {

    @Autowired
    private WhiteListDao whiteListDao;

    @Override
    public void delete(Integer id) {
        whiteListDao.delete(id);
    }

    @Override
    public void insert(WhiteList whiteList) {
        whiteListDao.insert(whiteList);
    }

    @Override
    public void update(WhiteList whiteList) {
        whiteListDao.update(whiteList);
    }

    @Override
    public Page<WhiteList> select(WhiteList whiteList) {
        PageHelper.startPage(whiteList.getPageNum(), whiteList.getPageSize());
        Page<WhiteList> whiteListPage = whiteListDao.getAll(whiteList);
        return whiteListPage;
    }
}
