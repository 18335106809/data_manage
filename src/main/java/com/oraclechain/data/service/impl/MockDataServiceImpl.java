package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.config.SocketIOConfig;
import com.oraclechain.data.entity.GreenEnergy;
import com.oraclechain.data.service.MockDataService;
import com.oraclechain.data.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sun.awt.SunHints;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/27
 */
@Service
public class MockDataServiceImpl implements MockDataService{

    static Logger logger = LoggerFactory.getLogger(MockDataServiceImpl.class);

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    static Random random = new Random();

    @Override
    public String mockGreenData() {
        logger.info("-------mock green data");
        List<Integer> powers = new ArrayList<>();
        List<Double> factors = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            int temp = random.nextInt(4000) + 1000;
            Double doubleTemp = random.nextDouble();
            powers.add(temp);
            String str = decimalFormat.format(doubleTemp);
            factors.add(Double.valueOf(str));
        }
        GreenEnergy greenEnergy = new GreenEnergy(330, 1000, 16, 100, 200, 4000, 3000, 10000, 0.7, 1, powers, factors);
        return JSONObject.toJSONString(greenEnergy);
    }

    @Override
    public String mockStructuralData(Long pipeId) {
        logger.info("-------mock structural data");
        List<String> horizontal = new ArrayList<>();
        List<Double> standardize = new ArrayList<>();
        List<Double> realTime = new ArrayList<>();
        List<Double> difference = new ArrayList<>();

        for (int i = 1; i < 116; i++) {
            horizontal.add("静力水准仪" + i);
            standardize.add(Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40) + 10)));
            realTime.add(Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40) + 10)));
            difference.add(Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(20) - 10)));
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("horizontal", horizontal);
        jsonObject.put("standardize", standardize);
        jsonObject.put("realTime", realTime);
        jsonObject.put("difference", difference);
        return jsonObject.toJSONString();
    }

    @Override
    public String mockFireFlightData(Long pipeId) {
        logger.info("-------mock fireFlight data by pipeId : " + pipeId);
        JSONObject jsonObject = new JSONObject();
        List<Integer> horizontal = new ArrayList<>();
        List<Double> temperature = new ArrayList<>();
        for(int i = 0; i<200; i++) {
            horizontal.add(i);
            temperature.add(Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(20) + 10)));
        }
        jsonObject.put("horizontal", horizontal);
        jsonObject.put("temperature", temperature);
        List<String> devices = new ArrayList<>();
        devices.add("报警灯1");
        devices.add("烟感探测仪1");
        devices.add("烟感探测仪2");
        devices.add("电气火灾探测仪");
        devices.add("消防电源");
        devices.add("手动报警按钮");
        devices.add("可燃气体传感器");
        devices.add("防火门");
        List<JSONObject> jsonObjectList = new ArrayList<>();
        for (int i = 0; i < 4 + random.nextInt(5); i++) {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("deviceName", devices.get(i));
            jsonObject1.put("distance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(10))));
            if (i == 0) {
                jsonObject1.put("isButton", true);
            }
            if (i == 7) {
                jsonObject1.put("isOpen", random.nextInt(2));
            }
            if (i == 0 || i == 7) {
                jsonObject1.put("isAuto", random.nextInt(2));
            }
            if (i > 0 && i < 7) {
                jsonObject1.put("isGas", random.nextInt(2));
            }
            jsonObjectList.add(jsonObject1);
        }
        jsonObject.put("device", jsonObjectList);

//        jsonObject.put("alarmLightDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(10))));
//        jsonObject.put("smoke1", random.nextInt(2));
//        jsonObject.put("smoke1Distance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(20))));
//        jsonObject.put("smoke2", random.nextInt(2));
//        jsonObject.put("smoke2Distance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(20))));
//        jsonObject.put("electric", random.nextInt(2));
//        jsonObject.put("electricDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40))));
//        jsonObject.put("fireFlight", random.nextInt(2));
//        jsonObject.put("fireFlightDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40))));
//        jsonObject.put("manualAlarm", random.nextInt(2));
//        jsonObject.put("manualAlarmDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40))));
//        jsonObject.put("fireGas", random.nextInt(2));
//        jsonObject.put("fireGasDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40))));
//        jsonObject.put("unFireDoorIsOpen", random.nextInt(2));
//        jsonObject.put("unFireDoorIsAuto", random.nextInt(2));
//        jsonObject.put("unFireDoorDistance", Double.valueOf(decimalFormat.format(random.nextDouble() + random.nextInt(40))));
        return jsonObject.toJSONString();
    }

    @Override
    public String switchData(String eventName, Long pipeId) {
        String result = null;
        switch (eventName) {
            case SocketIOConfig.GREEN_EVENT:
                result = mockGreenData();
                break;
            case SocketIOConfig.STRUCTURAL_EVENT:
                result = mockStructuralData(pipeId);
                break;
            case SocketIOConfig.FIREFLIGHT_EVENT:
                result = mockFireFlightData(pipeId);
                break;
        }
        return result;
    }

    @Override
    public JSONObject mockEarlyWarn(String deviceName, String startTime, String endTime) {
        JSONObject jsonObject = new JSONObject();
        List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
        jsonObject.put("deviceName", deviceName);
        jsonObject.put("times", dates);
        boolean isAlarm = random.nextInt(2) == 1 ? true : false;
        jsonObject.put("isAlarm", isAlarm);
        List<Double> doubles = new ArrayList<>();
        Integer baseNum = 13;
        for (int i = 0;i < dates.size(); i++) {
            if (isAlarm) {
                baseNum++;
            }
            doubles.add(Double.valueOf(decimalFormat.format(random.nextDouble() + baseNum)));
        }
        jsonObject.put("data", doubles);
        List<String> earlyTime = TimeUtil.getBetweenDate(endTime);
        jsonObject.put("earlyTimes", earlyTime);
        return jsonObject;
    }

    @Override
    public JSONObject mockEarlyWarnTable(String deviceName) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("capsuleName", "水信舱");
        jsonObject.put("pipeName", "防火段");
        jsonObject.put("deviceName", deviceName);
        jsonObject.put("deviceId", "p" + random.nextInt(100) + 1000);
        jsonObject.put("value", Double.valueOf(decimalFormat.format(random.nextDouble() + 20)));
        jsonObject.put("isSafe", random.nextInt(2) == 1 ? true : false);
        jsonObject.put("threshold", "20/60");
        if (!Boolean.parseBoolean(jsonObject.get("isSafe").toString())) {
            String distanceTime = TimeUtil.getDateNext(random.nextInt(7), false);
            jsonObject.put("alarmTime", distanceTime);
            jsonObject.put("countDown", TimeUtil.getDistanceTime(null , distanceTime));
        } else {
            jsonObject.put("alarmTime", "N/A");
            jsonObject.put("countDown", "N/A");
        }
        return jsonObject;
    }

    public static void main(String[] args) {
        for (int i = 1; i<= 100; i++) {
            System.out.println(Double.valueOf(decimalFormat.format(random.nextDouble())));
        }
    }
}
