package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.AlarmConfLevelDao;
import com.oraclechain.data.entity.AlarmConfLevel;
import com.oraclechain.data.service.AlarmConfLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
@Service
public class AlarmConfLevelServiceImpl implements AlarmConfLevelService {

    static Logger logger = LoggerFactory.getLogger(AlarmConfLevelServiceImpl.class);

    @Autowired
    private AlarmConfLevelDao alarmConfLevelDao;

    @Override
    public AlarmConfLevel getLevelByDealId(Integer dealId) {
        return null;
    }

    @Override
    public List<Integer> getAllLevel() {
        return alarmConfLevelDao.getAllLevel();
    }

    @Override
    public Page<AlarmConfLevel> getAlarmConfLevel(Integer pageNum, Integer pageSize, Integer level, String alarmEvent, String device) {
        PageHelper.startPage(pageNum, pageSize);
        return alarmConfLevelDao.getAlarmConfLevel(level, alarmEvent, device);
    }

    @Override
    public void insertAlarmConfLevel(AlarmConfLevel alarmConfLevel) {
        alarmConfLevelDao.insertAlarmConfLevel(alarmConfLevel);
    }

    @Override
    public void updateAlarmConfLevel(AlarmConfLevel alarmConfLevel) {
        alarmConfLevelDao.updateAlarmConfLevel(alarmConfLevel);
    }

    @Override
    public void deleteAlarmConfLevel(Integer id) {
        alarmConfLevelDao.deleteAlarmConfLevel(id);
    }
}
