package com.oraclechain.data.service.impl;

import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.service.SysLinkedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description: 发送指令的各种方式
 * @date 2020/4/14
 */
@Service
public class SysLinkedServiceImpl implements SysLinkedService {

    static Logger logger = LoggerFactory.getLogger(SysLinkedServiceImpl.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void sendOrderByAuto(Long deviceId, Integer status) {
        try {
            this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, deviceId);
        } catch (Exception e) {
            logger.error("send order by auto has error,{},{}", deviceId, status);
            e.printStackTrace();
        }
    }

    @Override
    public void sendOrderByManual(Long deviceId) {
        try {
            this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, deviceId);
        } catch (Exception e) {
            logger.error("send order by manual has error：{}", deviceId);
            e.printStackTrace();
        }
    }

    @Override
    public void updateStatus(Long deviceId) {
        try {
            this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_CLOSE, deviceId);
        } catch (Exception e) {
            logger.error("update device status has error: {}", deviceId);
            e.printStackTrace();
        }
    }
}
