package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.dao.DeviceAvgDataDao;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.DeviceDataReport;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.DeviceAvgDataService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
@Service
public class DeviceAvgDataServiceImpl implements DeviceAvgDataService {

    static Logger logger = LoggerFactory.getLogger(DeviceAvgDataServiceImpl.class);

    @Autowired
    private DeviceAvgDataDao deviceAvgDataDao;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${device.hall-current-sensor.power}")
    private String power;

    @Value("${device.hall-current-sensor.power-consumption}")
    private String electricityNum;

    @Value("${daily.avg.device.type}")
    private String deviceTypes;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    static Random random = new Random();

    @Override
    public void insertData(DeviceDataReport deviceAvgData) {
        deviceAvgDataDao.insert(deviceAvgData);
    }

    @Override
    public List<DeviceDataReport> getData(String date, Long spaceId, Long capsuleId, String deviceType, String dataKey) {
        return deviceAvgDataDao.getData(null, null, date, spaceId, capsuleId, deviceType, dataKey);
    }

    @Override
    public Object getDataByDay(List<Object> ids, String startTime, String endTime) {
        List<JSONObject> result = new ArrayList<>();
        List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
        List<String> earlyTimes = TimeUtil.getBetweenDate(TimeUtil.getDates(2, false));
        // 判断ids是否为null
        ids = isEmptyIds(ids);
        for (Object id : ids) {
            JSONObject jsonObject = new JSONObject();
            Device device = redisGetDeviceService.getDataByKey(RedisKeyConfig.DEVICE_KEY + id, Device.class);
            if (device == null) {
                continue;
            }
            device = redisGetDeviceService.setCapsuleInfo(device);
            DeviceCommon deviceCommon = new DeviceCommon(device);
            List<String> dataKeys =  deviceCommon.getParams();
            JSONObject thresholdJson = deviceCommon.getThresholdJson();
            JSONObject valueJson = new JSONObject();
            JSONObject dataKeyAvgJson = new JSONObject();
            for (String dataKey : dataKeys) {
                List<Double> valueList = new ArrayList<>();
                Double sum = 0.0;
                for (String date : dates) {
                    Double value = deviceAvgDataDao.getDataByDay(date, device.getId(), dataKey);
                    valueList.add(value != null ? value : 0);
                    sum += (value != null ? value : 0);
                }
                valueJson.put(dataKey, valueList);
                dataKeyAvgJson.put(dataKey, Double.valueOf(decimalFormat.format(sum / valueList.size())));
            }
            JSONObject earlyData = new JSONObject();
            Boolean isAlarm = false;
            for (String dateKey : dataKeyAvgJson.keySet()) {
                List<Double> earlyDataList = new ArrayList<>();
                Double avgData = dataKeyAvgJson.getDouble(dateKey);
                for (String earlyTime : earlyTimes) {
                    avgData += random.nextDouble();
                    earlyDataList.add(Double.valueOf(decimalFormat.format(avgData)));
                    if (thresholdJson != null && thresholdJson.containsKey(dateKey)) {
                        List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(dateKey).toString(), Double.class);
                        if (avgData < doubleList.get(0) || avgData > doubleList.get(1)) {
                            isAlarm = true;
                        }
                    }
                }
                earlyData.put(dateKey, earlyDataList);
            }
            jsonObject.put("isAlarm", isAlarm);
            jsonObject.put("earlyData", earlyData);
            jsonObject.put("data", valueJson);
            jsonObject.put("deviceName", device.getName());
            jsonObject.put("times", dates);
            jsonObject.put("earlyTimes", earlyTimes);
            jsonObject.put("deviceInfo", device);
            result.add(jsonObject);
        }
        return result;
    }

    public List<Object> isEmptyIds(List<Object> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            ids = new ArrayList<>();
            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            List<String> values = redisTemplate.opsForValue().multiGet(keys);
            for (String value : values) {
                Device device = JSONObject.parseObject(value, Device.class);
                if (device != null && !StringUtils.isEmpty(device.getType()) && !StringUtils.isEmpty(device.getType().trim()) && deviceTypes.contains(device.getType().trim())) {
                    ids.add(device.getId());
                }
            }
//            if (keys.size() > 0) {
//                List<String> keyList = new ArrayList<>(keys);
//                while (true) {
//                    Integer index = random.nextInt(keyList.size());
//                    String key = keyList.get(index);
//                    Device device = redisGetDeviceService.getDataByKey(key, Device.class);
//                    if (device != null && !StringUtils.isEmpty(device.getType().trim()) && deviceTypes.contains(device.getType().trim())) {
//                        ids.add(device.getId());
//                        break;
//                    }
//                }
//            }
        }
        return ids;
    }

    @Override
    public JSONObject getDateByWeek(Long spaceId, String dataKey, Integer week) {
        logger.info("get data by week : " + week);
        JSONObject jsonObject = new JSONObject();
        String startTime = TimeUtil.getDates(7, true);
        String endTime = TimeUtil.getDates(1, true);
        if (week == 1) {
            startTime = TimeUtil.getDates(7, true);
        } else if (week == 2) {
            startTime = TimeUtil.getDates(14, true);
        }
        List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
        List<Double> values = new ArrayList<>();
        try {
            for (String date : dates) {
                List<DeviceDataReport> dataReportList = deviceAvgDataDao.getData(null, null, date, spaceId, null, null, dataKey);
                if (CollectionUtils.isEmpty(dataReportList)) {
                    values.add(0.0);
                } else {
                    Double sum = 0.0;
                    for (DeviceDataReport deviceDataReport : dataReportList) {
                        sum += deviceDataReport.getDataAvgValue();
                    }
                    values.add(Double.valueOf(decimalFormat.format(sum / dataReportList.size())));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonObject.put("value", values);
        jsonObject.put("date", dates);
        jsonObject.put("week", week);
        return jsonObject;
    }

    @Override
    public JSONObject getGreenDataHistory(Long deviceId) {
        JSONObject jsonObject = new JSONObject();
        String startTime = TimeUtil.getDates(30, true);
        String endTime = TimeUtil.getDates(1, true);
        List<String> dataKeys = new ArrayList<>();
        dataKeys.add(power);
        dataKeys.add(electricityNum);
        List<String> dates = TimeUtil.getBetweenDate(startTime, endTime);
        List<Double> powerList = new ArrayList<>();
        List<Double> electricityList = new ArrayList<>();
        for (String date : dates) {
            List<DeviceDataReport> object = deviceAvgDataDao.getDataByDayAndDataKey(date, deviceId, dataKeys);
            if (CollectionUtils.isEmpty(object)) {
                powerList.add(0.0);
                electricityList.add(0.0);
            } else {
                for (DeviceDataReport report : object) {
                    if (report.getDataKey() != null) {
                        if (report.getDataKey().equals(power)) {
                            powerList.add(report.getDataAvgValue() != null ? report.getDataAvgValue() : 0);
                        } else if (report.getDataKey().equals(electricityNum)) {
                            electricityList.add(report.getDataAvgValue() != null ? report.getDataAvgValue() : 0);
                        } else {
                            powerList.add(0.0);
                            electricityList.add(0.0);
                        }
                    }
                }
            }
        }
        jsonObject.put("dates", dates);
        jsonObject.put("powerList", powerList);
        jsonObject.put("electricityList", electricityList);
        return jsonObject;
    }

    @Override
    public Double getMaxData(Long deviceId, String date, String dataKey) {
        return deviceAvgDataDao.getMaxData(deviceId, date, dataKey);
    }

    public static void main(String[] args) {
        for (int i = 0; i <= 99; i++) {
            System.out.println(random.nextInt(0));
        }
    }
}
