package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.InspectionDao;
import com.oraclechain.data.entity.Inspection;
import com.oraclechain.data.service.InspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/6
 */
@Service
public class InspectionServiceImpl implements InspectionService {

    @Autowired
    private InspectionDao inspectionDao;

    @Override
    public void insert(Inspection inspection) {
        inspectionDao.insert(inspection);
    }

    @Override
    public void update(Inspection inspection) {
        inspectionDao.update(inspection);
    }

    @Override
    public void delete(Integer id) {
        inspectionDao.delete(id);
    }

    @Override
    public Page<Inspection> getData(Inspection inspection) {
        PageHelper.startPage(inspection.getPageNum(), inspection.getPageSize());
        return inspectionDao.getData(inspection);
    }

    @Override
    public List<Inspection> getAllData() {
        return inspectionDao.getAllData();
    }
}
