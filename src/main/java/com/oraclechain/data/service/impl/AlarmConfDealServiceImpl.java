package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.AlarmConfDealDao;
import com.oraclechain.data.entity.AlarmConfDeal;
import com.oraclechain.data.service.AlarmConfDealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
@Service
public class AlarmConfDealServiceImpl implements AlarmConfDealService {

    @Autowired
    private AlarmConfDealDao alarmConfDealDao;

    @Override
    public List<AlarmConfDeal> getAllDeal() {
        return alarmConfDealDao.getAllDeal();
    }

    @Override
    public Page<AlarmConfDeal> getAlarmConfDeal(Integer pageNum, Integer pageSize, String eventName, String keyWord, Long capsuleId) {
        PageHelper.startPage(pageNum, pageSize);
        return alarmConfDealDao.getAlarmConfDeal(eventName, keyWord, capsuleId);
    }

    @Override
    public void insertAlarmConfDeal(AlarmConfDeal alarmConfDeal) {
        alarmConfDealDao.insertAlarmConfDeal(alarmConfDeal);
    }

    @Override
    public void insertAlarmDeals(List<AlarmConfDeal> alarmConfDealList) {
        alarmConfDealDao.insertAlarmDeals(alarmConfDealList);
    }

    @Override
    public void updateAlarmConfDeal(AlarmConfDeal alarmConfDeal) {
        alarmConfDealDao.updateAlarmConfDeal(alarmConfDeal);
    }

    @Override
    public void deleteAlarmConfDeal(Integer id) {
        alarmConfDealDao.deleteAlarmConfDeal(id);
    }
}
