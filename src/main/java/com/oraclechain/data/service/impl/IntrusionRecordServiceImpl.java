package com.oraclechain.data.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.oraclechain.data.dao.IntrusionRecordDao;
import com.oraclechain.data.entity.IntrusionRecord;
import com.oraclechain.data.service.IntrusionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
@Service
public class IntrusionRecordServiceImpl implements IntrusionRecordService {

    @Autowired
    private IntrusionRecordDao intrusionRecordDao;

    @Override
    public void delete(Integer id) {
        intrusionRecordDao.delete(id);
    }

    @Override
    public void insert(IntrusionRecord intrusionRecord) {
        intrusionRecordDao.insert(intrusionRecord);
    }

    @Override
    public void update(IntrusionRecord intrusionRecord) {
        intrusionRecordDao.update(intrusionRecord);
    }

    @Override
    public Page<IntrusionRecord> select(IntrusionRecord intrusionRecord) {
        PageHelper.startPage(intrusionRecord.getPageNum(), intrusionRecord.getPageSize());
        Page<IntrusionRecord> intrusionRecords = intrusionRecordDao.getAll(intrusionRecord);
        return intrusionRecords;
    }
}
