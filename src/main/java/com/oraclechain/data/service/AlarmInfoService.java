package com.oraclechain.data.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmInfo;
import com.oraclechain.data.entity.AlarmSOP;
import com.oraclechain.data.entity.device.Device;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
public interface AlarmInfoService {
    Page<AlarmInfo> getAllAlarm(Integer pageNum, Integer pageSize, String deviceType, Integer level, String type, Long capsuleId, Long segmentId, String startTime, String endTime);
    List<AlarmInfo> getTodayAlarm(String deviceType, Integer level, String type, Long capsuleId, Long segmentId, String startTime, String endTime);
    AlarmInfo getAlarmById(Integer id);
    void updateAlarm(String hurt, String accidentReason, Integer alarmId);
    List<AlarmInfo> getAlarmByIdList(List<Integer> ids, Long spaceId, Long capsuleId, String deviceType);
    Page<AlarmInfo> getAlarmByDeviceId(Long deviceId, Integer pageNum, Integer pageSize);
    Integer setAlarm(Integer level, Long deviceId, Long capsuleId, Integer typeIndex, String status, String reason, String deviceType, String alarmKey, String alarmValue);
    void currentAlarm(Device device, String alarmReason, Boolean isMock, String alarmKey, String alarmValue, Integer alarmLevel);
    void removeAlarm(Device device, String alarmKey, Boolean isMock);
    Integer isAlarm(Long deviceId);
    List<AlarmInfo> getCurrentAlarm(Long spaceId, Long capsuleId, String deviceType, Boolean isAid);
    Object getCurrentAlarmDeviceData(Long spaceId, Long capsuleId, String deviceType);
    Object getCurrentAlarmBaseData(Long spaceId, Long capsuleId, String deviceType);
    JSONObject historyAlarmTimes(Long spaceId, String deviceType, String timeType);
    Object historyCountByLevel(String deviceType, String startTime, String endTime);
    List<AlarmSOP> getSOPByAlarmId(Integer alarmId);
    Integer alarmTimes(Long deviceId, Long spaceId, String startTime, String endTime);
    Object getAlarmMinuteData(Integer id, Boolean isRealTime);
    Object getAlarmCountData(Long deviceId, String startTime, String endTime);
    Integer alarmTimesByType(Integer alarmType, String startTime, String endTime);
    void sendAlarm();
}
