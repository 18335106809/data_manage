package com.oraclechain.data.service;

/**
 * @author liuhualong
 * @Description: 给设备下发指令的固定模式。eg:风扇、水泵、灯
 * @date 2020/6/14
 */
public interface FixedModeService {
    void sendInstruction(Long deviceId, Boolean open);
}
