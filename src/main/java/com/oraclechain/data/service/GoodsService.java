package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Goods;
import com.oraclechain.data.entity.GoodsLibrary;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
public interface GoodsService {
    Page<Goods> getAllGoods(String type, Integer libraryId, String name, Integer pageNum, Integer pageSize);
    Goods getById(Integer id);
    void insertGoods(Goods goods);
    void updateGoods(Goods goods);
    void deleteGoods(Integer id);

    List<GoodsLibrary> getAllGoodsLibrary();
    List<GoodsLibrary> getGoodsLibraryByPid(Integer pid);
    void insertGoodsLibrary(GoodsLibrary goodsLibrary);
    void updateGoodsLibrary(GoodsLibrary goodsLibrary);
    void deleteGoodsLibrary(Integer id);
}
