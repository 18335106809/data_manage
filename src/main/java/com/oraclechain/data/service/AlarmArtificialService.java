package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmArtificial;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/31
 */
public interface AlarmArtificialService {
    void insert(AlarmArtificial alarmArtificial);
    void update(AlarmArtificial alarmArtificial);
    void delete(Integer id);
    Page<AlarmArtificial> select(AlarmArtificial alarmArtificial);
    Object reportByTime(Integer alarmType, String timeType);
    Object linkedDeviceData(Long capsuleId, Integer alarmType);
}
