package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Expert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuhualong
 * @Description:  Expert相关逻辑
 * @date 2020/2/12
 */
public interface ExpertService {
    Page<Expert> getAllExpert(Integer sex, String name, String department, String profession, Integer pageNum, Integer pageSize);
    Expert selectById(Integer id);
    void insertExpert(Expert expert);
    void updateExpert(Expert expert);
    void deleteExpert(Integer id);
}
