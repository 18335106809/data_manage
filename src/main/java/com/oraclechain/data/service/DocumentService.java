package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Document;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/28
 */
public interface DocumentService {
    Page<Document> getDocuments(Integer type, Integer pageNum, Integer pageSize);
    Document getById(Integer id);
    void insertDocument(Document document);
    void updateDocument(Document document);
    void deleteDocument(Integer id);
}
