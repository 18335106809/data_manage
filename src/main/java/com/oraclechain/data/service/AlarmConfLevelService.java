package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.AlarmConfLevel;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
public interface AlarmConfLevelService {
    AlarmConfLevel getLevelByDealId(Integer dealId);
    List<Integer> getAllLevel();
    Page<AlarmConfLevel> getAlarmConfLevel(Integer pageNum, Integer pageSize, Integer level, String alarmEvent, String device);
    void insertAlarmConfLevel(AlarmConfLevel alarmConfLevel);
    void updateAlarmConfLevel(AlarmConfLevel alarmConfLevel);
    void deleteAlarmConfLevel(Integer id);
}
