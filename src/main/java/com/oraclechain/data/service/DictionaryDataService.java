package com.oraclechain.data.service;

import com.oraclechain.data.entity.DictionaryData;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/16
 */
public interface DictionaryDataService {
    void insert(DictionaryData dictionaryData);
    void update(DictionaryData dictionaryData);
    void delete(Integer id);
    List<DictionaryData> select(DictionaryData dictionaryData);
    void updateCache();
}
