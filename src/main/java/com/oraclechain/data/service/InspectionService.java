package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.Inspection;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/6
 */
public interface InspectionService {
    void insert(Inspection inspection);
    void update(Inspection inspection);
    void delete(Integer id);
    Page<Inspection> getData(Inspection inspection);
    List<Inspection> getAllData();
}
