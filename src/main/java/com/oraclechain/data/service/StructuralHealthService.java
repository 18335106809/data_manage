package com.oraclechain.data.service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/22
 */
public interface StructuralHealthService {
    Object getData();
}
