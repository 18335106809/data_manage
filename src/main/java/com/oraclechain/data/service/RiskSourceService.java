package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.RiskSource;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/11
 */
public interface RiskSourceService {
    Page<RiskSource> getRiskSource(Long capsuleId, String deviceType,
                                   String plc, String riskObject, String responsible, Integer pageNum, Integer pageSize);
    void insertRiskSource(RiskSource riskSource);
    void updateRiskSource(RiskSource riskSource);
    void deleteRiskSource(Integer id);
}
