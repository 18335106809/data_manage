package com.oraclechain.data.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/27
 */
public interface MockDataService {
    String mockGreenData();
    String mockStructuralData(Long pipeId);
    String mockFireFlightData(Long pipeId);
    String switchData(String eventName, Long pipeId);
    JSONObject mockEarlyWarn(String deviceName, String startTime, String endTime);
    JSONObject mockEarlyWarnTable(String deviceName);
}
