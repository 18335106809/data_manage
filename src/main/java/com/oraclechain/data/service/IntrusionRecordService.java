package com.oraclechain.data.service;

import com.github.pagehelper.Page;
import com.oraclechain.data.entity.IntrusionRecord;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/29
 */
public interface IntrusionRecordService {
    void delete(Integer id);
    void insert(IntrusionRecord intrusionRecord);
    void update(IntrusionRecord intrusionRecord);
    Page<IntrusionRecord> select(IntrusionRecord intrusionRecord);
}
