package com.oraclechain.data.service;

import com.oraclechain.data.entity.device.Device;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/26
 */
public interface DeviceUpdateService {
    void updateDevice(Device device);
}
