package com.oraclechain.data.service;

import com.oraclechain.data.entity.Instruction;
import com.oraclechain.data.entity.device.Device;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/23
 */
public interface InstructionService {
    void alarmHandling(Object deviceId);
    void openInstruction(Long deviceId, Long capsuleId, String deviceType, String reason, Integer alarmId, Integer openOrClose);
    void closeInstruction(Long capsuleId, String deviceType, Integer alarmId);

    void insertInstruction(Instruction instruction);
    Integer countTimes(Long spaceId, Long capsuleId, String deviceType, String startTime, String endTime);
    void singleInstruction(Object deviceId, String key, Integer value);
    void bulkInstruction(Long capsuleId, String deviceType, String dataKey, Integer status);
}
