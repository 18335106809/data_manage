package com.oraclechain.data.service;

import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
public interface DeviceDataService {
    List<Device> realTimeData(List<Object> ids, Long capsuleId, String type);
    List<DeviceDisplay> historyData(Long id, String startTime, String endTime);
    List<DeviceDisplay> minuteData(Object capsuleId, String type, Object deviceId, Long time);
    void setData(Long id, String key, Object value);
    Object getDateByReal(String dataKey);
    Device setOtherData(Device device);
}
