package com.oraclechain.data.service;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.DeviceDataReport;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
public interface DeviceAvgDataService {
    void insertData(DeviceDataReport deviceAvgData);
    List<DeviceDataReport> getData(String date, Long spaceId, Long capsuleId, String deviceType, String dataKey);
    Object getDataByDay(List<Object> ids, String startTime, String endTime);
    JSONObject getDateByWeek(Long spaceId, String dataKey, Integer week);
    JSONObject getGreenDataHistory(Long deviceId);
    Double getMaxData(Long deviceId, String date, String dataKey);
}
