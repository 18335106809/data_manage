package com.oraclechain.data.service;

import com.oraclechain.data.entity.AlarmLevel;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/17
 */
public interface AlarmLevelService {
    void insert(AlarmLevel alarmLevel);
    void update(AlarmLevel alarmLevel);
    void delete(Integer id);
    List<AlarmLevel> getAll();
    List<AlarmLevel> getLevelListByKey(String key);
}
