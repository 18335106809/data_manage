package com.oraclechain.data.redis;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/14
 */
public class RedisKeyConfig {

    public static final String DEVICE_KEY = "device:";
    public static final String DEVICE_PARAMETER_KEY = "device.parameter:";
    public static final String DEVICE_SPACE_KEY= "spaceManage:";
    public static final String DEVICE_HEALTH_KEY = "device.health.";
    public static final String DEVICE_REALTIME_DATA = "device.real.time.data:";
    public static final String DEVICE_MOCK_KEY = "device.mock:";
    public static final String DEVICE_ALARM_KEY = "device.alarm:";
    public static final String DEVICE_INSTRUCTION_KEY = "device.instruction:";
    public static final String DEVICE_STATUS_KEY = "device.status:";
    public static final String DEVICE_DATAKEY_DIC = "device.dataKey.dic";
    public static final String DEVICE_TRIGGER_ALARM = "device.trigger.alarm";
    public static final String DEVICE_TYPE_LIST = "device.type.list:";

}
