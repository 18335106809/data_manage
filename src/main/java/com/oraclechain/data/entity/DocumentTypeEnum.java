package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/7
 */
public enum DocumentTypeEnum {
    SURVEY("勘察", 1), PLAN("规划", 2), BUILD("建设", 3), ACCEPTING("验收", 4), OPERATION("运维", 5);
    private String name;
    private int index;

    private DocumentTypeEnum(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(int index) {
        for (DocumentTypeEnum documentTypeEnum : DocumentTypeEnum.values()) {
            if (documentTypeEnum.index == index) {
                return documentTypeEnum.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
