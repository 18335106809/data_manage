package com.oraclechain.data.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
public enum AlarmLevelEnum {
    IF("IF"),FOR("FOR"),BREAK("BREAK"),CONTINUE("CONTINUE"),CALL("CALl"),VERIFY("VERIFY"),EXPR("EXPR"),SEND("SEND"),ASK("ASK"),MSG("MSG"),GOTO("GOTO");
    private String name;

    private AlarmLevelEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<String> getValues() {
        List<String> strings = new ArrayList<>();
        for (AlarmLevelEnum alarmLevelEnum : AlarmLevelEnum.values()) {
            strings.add(alarmLevelEnum.name);
        }
        return strings;
    }

}