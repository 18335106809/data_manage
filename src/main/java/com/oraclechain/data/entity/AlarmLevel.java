package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/17
 */
public class AlarmLevel {

    private Integer id;
    private String percentage;
    private Integer level;
    private String deviceType;
    private Float lowValue;
    private Float highValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Float getHighValue() {
        return highValue;
    }

    public void setHighValue(Float highValue) {
        this.highValue = highValue;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Float getLowValue() {
        return lowValue;
    }

    public void setLowValue(Float lowValue) {
        this.lowValue = lowValue;
    }

    public static void main(String[] args) {
        Double value = 33.33;
        Float level = 0.00f;
        System.out.println(value * level);
    }
}
