package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/16
 */
public class DictionaryData {
    private Integer id;
    private String type;
    private String name;
    private String notes;

    public DictionaryData() {
    }

    public DictionaryData(Integer id, String type, String name, String notes) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.notes = notes;
    }

    public DictionaryData(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
