package com.oraclechain.data.entity;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
public class GoodsLibrary {
    private Integer id;
    private Integer parentId;
    private String name;
    private String address;
    private String supervisor;

    private List<GoodsLibrary> goodsLibraryList;

    public GoodsLibrary() {
    }

    public GoodsLibrary(Integer parentId, String name, String address, String supervisor) {
        this.parentId = parentId;
        this.name = name;
        this.address = address;
        this.supervisor = supervisor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public List<GoodsLibrary> getGoodsLibraryList() {
        return goodsLibraryList;
    }

    public void setGoodsLibraryList(List<GoodsLibrary> goodsLibraryList) {
        this.goodsLibraryList = goodsLibraryList;
    }
}
