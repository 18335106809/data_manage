package com.oraclechain.data.entity;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description: 由人工录入的报警对应的几种报警类型
 * @date 2020/8/31
 */
public enum AlarmArtificialEnum {
    FIRE("火灾", 1), RAINFALL("雨水倒灌", 2), SETTLEMENT("管廊沉降", 3);
    private String name;
    private Integer index;

    private AlarmArtificialEnum(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public static String getName(Integer index) {
        for (AlarmArtificialEnum alarmArtificialEnum : AlarmArtificialEnum.values()) {
            if (alarmArtificialEnum.index == index) {
                return alarmArtificialEnum.name;
            }
        }
        return null;
    }

    public static AlarmArtificialEnum getType(Integer index) {
        for (AlarmArtificialEnum alarmArtificialEnum : AlarmArtificialEnum.values()) {
            if (alarmArtificialEnum.index == index) {
                return alarmArtificialEnum;
            }
        }
        return null;
    }

    public static Integer getIndex(String name) {
        for (AlarmArtificialEnum alarmArtificialEnum : AlarmArtificialEnum.values()) {
            if (alarmArtificialEnum.name.equals(name)) {
                return alarmArtificialEnum.index;
            }
        }
        return null;
    }

    public static Object getAll() {
        Map<String, Integer> result = new ConcurrentHashMap<>();
        for (AlarmArtificialEnum alarmArtificialEnum : AlarmArtificialEnum.values()) {
            result.put(alarmArtificialEnum.name, alarmArtificialEnum.index);
        }
        return result;
    }
}
