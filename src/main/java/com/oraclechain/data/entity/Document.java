package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/28
 */
public class Document {
    private Integer id;
    private String fileName;
    private Date createTime;
    private String uploader;
    private String filePath;
    private Integer type;
    private String size;

    private Document(){}

    public Document(String fileName, String uploader, String filePath, Integer type, String size) {
        this.fileName = fileName;
        this.uploader = uploader;
        this.filePath = filePath;
        this.type = type;
        this.createTime = new Date();
        this.size = size;
    }

    public Document(Integer id, String fileName, String uploader, String filePath, Integer type, String size) {
        this.id = id;
        this.fileName = fileName;
        this.uploader = uploader;
        this.filePath = filePath;
        this.type = type;
        this.createTime = new Date();
        this.size = size;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
