package com.oraclechain.data.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/6
 */
public class InspectionExcel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Excel(name = "序号", orderNum = "0", width = 30)
    private Integer id;

    @Excel(name = "巡检类型", orderNum = "1", width = 30)
    private String type;

    @Excel(name = "巡检日期", orderNum = "2", width = 30)
    private Date inspectTime;

    @Excel(name = "巡检范围", orderNum = "3", width = 30)
    private String inspectRange;

    @Excel(name = "发现问题", orderNum = "4", width = 30)
    private String problem;

    @Excel(name = "处理", orderNum = "5", width = 30)
    private String handle;

    @Excel(name = "巡检人", orderNum = "6", width = 30)
    private String inspector;

    @Excel(name = "反馈", orderNum = "7", width = 30)
    private String feedback;

    @Excel(name = "备注", orderNum = "8", width = 30)
    private String remark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public InspectionExcel() {
    }

    public InspectionExcel(Integer id, String type, Date inspectTime, String inspectRange, String problem, String handle, String inspector, String feedback, String remark) {
        this.id = id;
        this.type = type;
        this.inspectTime = inspectTime;
        this.inspectRange = inspectRange;
        this.problem = problem;
        this.handle = handle;
        this.inspector = inspector;
        this.feedback = feedback;
        this.remark = remark;
    }

    public InspectionExcel(Inspection inspection) {
        this.id = inspection.getId();
        this.type = inspection.getType();
        this.inspectTime = inspection.getInspectTime();
        this.inspectRange = inspection.getInspectRange();
        this.problem = inspection.getProblem();
        this.handle = inspection.getHandle();
        this.inspector = inspection.getInspector();
        this.feedback = inspection.getFeedback();
        this.remark = inspection.getRemark();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getInspectTime() {
        return inspectTime;
    }

    public void setInspectTime(Date inspectTime) {
        this.inspectTime = inspectTime;
    }

    public String getInspectRange() {
        return inspectRange;
    }

    public void setInspectRange(String inspectRange) {
        this.inspectRange = inspectRange;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
