package com.oraclechain.data.entity;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangyang
 * @since 2020-04-10
 */
public class Parameter {

    private Integer id;
    /**
     * 类型
     */
    private String type;

    private String name;
    /**
     * 设备属性
     */
    private String attribute;
    /**
     * 采集数据
     */
    private String gatheredData;
    /**
     * 协议
     */
    private String parameter;

    private Date createTime;

    private Date updateTime;

    private String[] attributes;        // 属性数组

    private String[] gatheredDatas;     // 数据字段数组

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getGatheredData() {
        return gatheredData;
    }

    public void setGatheredData(String gatheredData) {
        this.gatheredData = gatheredData;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String[] getAttributes() {
        return attributes;
    }

    public void setAttributes(String[] attributes) {
        this.attributes = attributes;
    }

    public String[] getGatheredDatas() {
        return gatheredDatas;
    }

    public void setGatheredDatas(String[] gatheredDatas) {
        this.gatheredDatas = gatheredDatas;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
