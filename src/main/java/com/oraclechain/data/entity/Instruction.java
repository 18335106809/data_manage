package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/28
 */
public class Instruction {

    private Integer id;         // id
    private Long deviceId;      // 设备id
    private String deviceType;  // 设备类型
    private Long capsuleId;     // 段id
    private Long spaceId;       // 舱id
    private Date createTime;    // 下发指令时间
    private String reason;      // 下发指令的原因

    public Instruction() {
    }

    public Instruction(Long deviceId, String deviceType, Long capsuleId, Long spaceId, Date createTime, String reason) {
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.capsuleId = capsuleId;
        this.spaceId = spaceId;
        this.createTime = createTime;
        this.reason = reason;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
