package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
public class AnalysisFile {
    private Integer id;
    private String planName;
    private String fileName;
    private String fileSize;
    private String filePath;
    private String reviewer;
    private Date createTime;
    private String auditResult;
    private String uploader;
    private String path;

    public AnalysisFile() {
    }

    public AnalysisFile(Integer id, String planName, String fileName, String fileSize, String filePath, String reviewer, Date createTime, String auditResult, String uploader, String path) {
        this.id = id;
        this.planName = planName;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filePath = filePath;
        this.reviewer = reviewer;
        this.createTime = createTime;
        this.auditResult = auditResult;
        this.uploader = uploader;
        this.path = path;
    }

    public AnalysisFile(String planName, String fileName, String fileSize, String filePath, String reviewer, Date createTime, String auditResult, String uploader, String path) {
        this.planName = planName;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filePath = filePath;
        this.reviewer = reviewer;
        this.createTime = createTime;
        this.auditResult = auditResult;
        this.uploader = uploader;
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAuditResult() {
        return auditResult;
    }

    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
