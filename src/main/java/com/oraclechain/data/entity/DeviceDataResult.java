package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
public class DeviceDataResult {

    private Double avg;

    private Double max;

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getAvg() {
        return avg;
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }
}
