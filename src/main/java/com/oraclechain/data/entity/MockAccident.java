package com.oraclechain.data.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/14
 */
public class MockAccident {

    private String deviceId;          // 设备ID
    private JSONObject analog;      // 模拟数据JSON
    private Integer radio;          // 立即设置还是缓慢设置
    private Integer consuming;      // 缓慢设置耗时多少秒
    private Boolean continued;      // 是否持续
    private Integer continuedTime;  // 如果是持续，持续多少秒

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public JSONObject getAnalog() {
        return analog;
    }

    public void setAnalog(JSONObject analog) {
        this.analog = analog;
    }

    public Integer getRadio() {
        return radio;
    }

    public void setRadio(Integer radio) {
        this.radio = radio;
    }

    public Integer getConsuming() {
        return consuming;
    }

    public void setConsuming(Integer consuming) {
        this.consuming = consuming;
    }

    public Boolean getContinued() {
        return continued;
    }

    public void setContinued(Boolean continued) {
        this.continued = continued;
    }

    public Integer getContinuedTime() {
        return continuedTime;
    }

    public void setContinuedTime(Integer continuedTime) {
        this.continuedTime = continuedTime;
    }
}
