package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:专家信息
 * @date 2020/2/12
 */
public class Expert {

    private Integer id;
    private String number;
    private String name;
    private Integer sex;
    private String department;
    private String profession;
    private String phoneNum;
    private String specialty;
    private String achievement;
    private String picture;
    private Date createTime;

    public Expert() {
    }

    public Expert(Integer id, String number, String name, Integer sex, String department, String profession, String phoneNum, String specialty, String achievement, String picture) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.sex = sex;
        this.department = department;
        this.profession = profession;
        this.phoneNum = phoneNum;
        this.specialty = specialty;
        this.achievement = achievement;
        this.picture = picture;
    }

    public Expert(String number, String name, Integer sex, String department, String profession, String phoneNum, String specialty, String achievement, String picture, Date createTime) {
        this.number = number;
        this.name = name;
        this.sex = sex;
        this.department = department;
        this.profession = profession;
        this.phoneNum = phoneNum;
        this.specialty = specialty;
        this.achievement = achievement;
        this.picture = picture;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
