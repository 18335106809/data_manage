package com.oraclechain.data.entity;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.util.StringToArrayUtil;

import java.util.List;

/**
 * @author liuhualong
 * @Description: 设备共通属性
 * @date 2020/1/11
 */
public class DeviceCommon extends Device{

    private List<String> params;
    private JSONObject thresholdJson;

    public DeviceCommon(Device device) {
        this.params = StringToArrayUtil.toArray(device.getParameters());
        this.thresholdJson = JSONObject.parseObject(device.getThreshold());
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    public JSONObject getThresholdJson() {
        return thresholdJson;
    }

    public void setThresholdJson(JSONObject thresholdJson) {
        this.thresholdJson = thresholdJson;
    }

}
