package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
public class Goods {
    private Integer id;
    private Integer libraryId;
    private String name;
    private String type;
    private Integer num;
    private Date InOutTime;
    private String unit;
    private String operator;
    private Date createTime;

    private GoodsLibrary goodsLibrary;

    public Goods() {
    }

    public Goods(Integer libraryId, String name, String type, Integer num, Date inOutTime, String unit, String operator, Date createTime) {
        this.libraryId = libraryId;
        this.name = name;
        this.type = type;
        this.num = num;
        this.InOutTime = inOutTime;
        this.unit = unit;
        this.operator = operator;
        this.createTime = createTime;
    }

    public Goods(Integer id, Integer libraryId, String name, String type, Integer num, Date inOutTime, String unit, String operator, Date createTime) {
        this.id = id;
        this.libraryId = libraryId;
        this.name = name;
        this.type = type;
        this.num = num;
        this.InOutTime = inOutTime;
        this.unit = unit;
        this.operator = operator;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Integer libraryId) {
        this.libraryId = libraryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getInOutTime() {
        return InOutTime;
    }

    public void setInOutTime(Date inOutTime) {
        this.InOutTime = inOutTime;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public GoodsLibrary getGoodsLibrary() {
        return goodsLibrary;
    }

    public void setGoodsLibrary(GoodsLibrary goodsLibrary) {
        this.goodsLibrary = goodsLibrary;
    }
}
