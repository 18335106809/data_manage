package com.oraclechain.data.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
public class AlarmConfDeal implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;                 // id
    private String eventName;           // 事件名称
    private String keyWord;             // 关键字
    private String instruction;         // 指令
    private Long capsuleId;             // 所属舱体
    private String device;              // 报警设备
    private Date createTime;            // 创建时间
    private Date runTime;               // 执行时间
    private Integer runResult;          // 运行结果

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getRunTime() {
        return runTime;
    }

    public void setRunTime(Date runTime) {
        this.runTime = runTime;
    }

    public Integer getRunResult() {
        return runResult;
    }

    public void setRunResult(Integer runResult) {
        this.runResult = runResult;
    }
}
