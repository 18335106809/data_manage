package com.oraclechain.data.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/5
 */
public class AlarmConfLevel implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;             // id
    private Integer level;          // 级别
    private String alarmEvent;      // 报警事件
    private String keyWord;         // 关键字
    private Long capsuleId;         // 所属舱体
    private String device;          // 报警设备
    private Integer dealId;         // 采取措施
    private Date createTime;        // 创建时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getAlarmEvent() {
        return alarmEvent;
    }

    public void setAlarmEvent(String alarmEvent) {
        this.alarmEvent = alarmEvent;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
