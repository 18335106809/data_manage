package com.oraclechain.data.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/9
 */
public class AlarmConfDealExcel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Excel(name = "事件", orderNum = "0", width = 15)
    private String eventName;           // 事件名称

    @Excel(name = "关键字", orderNum = "1", width = 15)
    private String keyWord;             // 关键字

    @Excel(name = "指令", orderNum = "2", width = 15)
    private String instruction;         // 指令

    @Excel(name = "所属舱体", orderNum = "3", width = 15)
    private String capsule;

    @Excel(name = "报警设备", orderNum = "4", width = 15)
    private String device;

    @Excel(name = "执行时间", orderNum = "5", width = 15)
    private Date runTime;

    public AlarmConfDealExcel() {
    }

    public AlarmConfDealExcel(String eventName, String keyWord, String instruction, String capsule, String device, Date runTime) {
        this.eventName = eventName;
        this.keyWord = keyWord;
        this.instruction = instruction;
        this.capsule = capsule;
        this.device = device;
        this.runTime = runTime;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getCapsule() {
        return capsule;
    }

    public void setCapsule(String capsule) {
        this.capsule = capsule;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Date getRunTime() {
        return runTime;
    }

    public void setRunTime(Date runTime) {
        this.runTime = runTime;
    }

}
