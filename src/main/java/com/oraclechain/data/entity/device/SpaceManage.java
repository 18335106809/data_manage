package com.oraclechain.data.entity.device;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author zhangyang
 * @since 2020-03-02
 */
public class SpaceManage {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 名称
     */
    private String name;

    /**
     * 空间组
     */
    private String spaceGroup;

    /**
     * 位置
     */
    private String location;

    private Integer xAxis ;

    private Integer yAxis ;

    private Integer zAxis ;

    private Integer number;

    /**
     * 长度
     **/
    private String spaceLength;

    private List<Device> children;

    private List<SpaceManage> subSpaceManage;

    private Date createTime;

    private Date updateTime;

    private List<String> deviceTypeList;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpaceGroup() {
        return spaceGroup;
    }

    public void setSpaceGroup(String spaceGroup) {
        this.spaceGroup = spaceGroup;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getxAxis() {
        return xAxis;
    }

    public void setxAxis(Integer xAxis) {
        this.xAxis = xAxis;
    }

    public Integer getyAxis() {
        return yAxis;
    }

    public void setyAxis(Integer yAxis) {
        this.yAxis = yAxis;
    }

    public Integer getzAxis() {
        return zAxis;
    }

    public void setzAxis(Integer zAxis) {
        this.zAxis = zAxis;
    }

    public String getSpaceLength() {
        return spaceLength;
    }

    public void setSpaceLength(String spaceLength) {
        this.spaceLength = spaceLength;
    }

    public List<Device> getChildren() {
        return children;
    }

    public void setChildren(List<Device> children) {
        this.children = children;
    }

    public List<SpaceManage> getSubSpaceManage() {
        return subSpaceManage;
    }

    public void setSubSpaceManage(List<SpaceManage> subSpaceManage) {
        this.subSpaceManage = subSpaceManage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<String> getDeviceTypeList() {
        return deviceTypeList;
    }

    public void setDeviceTypeList(List<String> deviceTypeList) {
        this.deviceTypeList = deviceTypeList;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
