package com.oraclechain.data.entity.device;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangyang
 * @since 2020-04-10
 */
public class DeviceParameter  implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 类型
     */
    private String type;

    private String name;

    /**
     * 设备属性
     */
    private String attribute;

    /**
     * 采集数据
     */
    private String gatheredData;

    /**
     * 协议
     */
    private String parameter;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getGatheredData() {
        return gatheredData;
    }

    public void setGatheredData(String gatheredData) {
        this.gatheredData = gatheredData;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
