package com.oraclechain.data.entity;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/19
 */
public class GreenEnergy {
    private Integer id;                 // 仓库Id
    private Integer voltage;            // 电压
    private Integer voltageAll;
    private Integer electric;           // 电流
    private Integer electricAll;
    private Integer power;              // 功率
    private Integer powerAll;
    private Integer electricNum;        // 电量
    private Integer electricNumAll;
    private Double factor;              // 功率因数
    private Integer factorAll;
    private List<Integer> powers;       // 功率数组
    private List<Double> factors;       // 功率因数

    public GreenEnergy(Integer voltage, Integer electric, Integer power, Integer electricNum, Double factor, List<Integer> powers, List<Double> factors) {
        this.voltage = voltage;
        this.electric = electric;
        this.power = power;
        this.electricNum = electricNum;
        this.factor = factor;
        this.powers = powers;
        this.factors = factors;
    }

    public GreenEnergy(Integer voltage, Integer voltageAll, Integer electric, Integer electricAll, Integer power,
                       Integer powerAll, Integer electricNum, Integer electricNumAll, Double factor, Integer factorAll, List<Integer> powers, List<Double> factors) {
        this.voltage = voltage;
        this.voltageAll = voltageAll;
        this.electric = electric;
        this.electricAll = electricAll;
        this.power = power;
        this.powerAll = powerAll;
        this.electricNum = electricNum;
        this.electricNumAll = electricNumAll;
        this.factor = factor;
        this.factorAll = factorAll;
        this.powers = powers;
        this.factors = factors;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVoltage() {
        return voltage;
    }

    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    public Integer getElectric() {
        return electric;
    }

    public void setElectric(Integer electric) {
        this.electric = electric;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getElectricNum() {
        return electricNum;
    }

    public void setElectricNum(Integer electricNum) {
        this.electricNum = electricNum;
    }

    public Double getFactor() {
        return factor;
    }

    public void setFactor(Double factor) {
        this.factor = factor;
    }

    public List<Integer> getPowers() {
        return powers;
    }

    public void setPowers(List<Integer> powers) {
        this.powers = powers;
    }

    public List<Double> getFactors() {
        return factors;
    }

    public void setFactors(List<Double> factors) {
        this.factors = factors;
    }

    public Integer getVoltageAll() {
        return voltageAll;
    }

    public void setVoltageAll(Integer voltageAll) {
        this.voltageAll = voltageAll;
    }

    public Integer getElectricAll() {
        return electricAll;
    }

    public void setElectricAll(Integer electricAll) {
        this.electricAll = electricAll;
    }

    public Integer getPowerAll() {
        return powerAll;
    }

    public void setPowerAll(Integer powerAll) {
        this.powerAll = powerAll;
    }

    public Integer getElectricNumAll() {
        return electricNumAll;
    }

    public void setElectricNumAll(Integer electricNumAll) {
        this.electricNumAll = electricNumAll;
    }

    public Integer getFactorAll() {
        return factorAll;
    }

    public void setFactorAll(Integer factorAll) {
        this.factorAll = factorAll;
    }
}
