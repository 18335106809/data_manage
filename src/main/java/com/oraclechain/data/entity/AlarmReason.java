package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/7
 */
public enum AlarmReason {
    BELOW_THRESHOLD("低于阈值"),ABOVE_THRESHOLD("高于阈值"),DEVICE_DAMAGE("设备损坏");
    private String name;

    private AlarmReason(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
