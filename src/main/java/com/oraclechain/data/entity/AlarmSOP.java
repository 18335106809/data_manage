package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/29
 */
public class AlarmSOP {

    private Integer id;
    private Integer alarmId;
    private String description;
    private Date createTime;

    public AlarmSOP() {
    }

    public AlarmSOP(Integer alarmId, String description, Date createTime) {
        this.alarmId = alarmId;
        this.description = description;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(Integer alarmId) {
        this.alarmId = alarmId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
