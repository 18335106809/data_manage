package com.oraclechain.data.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/7
 */
public enum AlarmType {
    ENVIRONMENT("环境报警", 1), DEVICE("设备故障", 2), MOCK("模拟报警", 3), STRUCTURAL("结构健康", 4), IRRUPT("防入侵", 5), ATMOSPHERIC("温湿度报警", 6),
    LEVEL("水位报警", 7), GAS("气体报警", 8);
    private String name;
    private int index;

    private AlarmType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(int index) {
        for (AlarmType alarmType : AlarmType.values()) {
            if (alarmType.index == index) {
                return alarmType.name;
            }
        }
        return null;
    }

    public static Integer getIndex(String name) {
        for (AlarmType alarmType : AlarmType.values()) {
            if (alarmType.name.equals(name)) {
                return alarmType.index;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static List<String> getAlarmType() {
        List<String> result = new ArrayList<>();
        for (AlarmType alarmType : AlarmType.values()) {
            result.add(alarmType.name);
        }
        return result;
    }

    public static List<Integer> getAlarmTypeIndex() {
        List<Integer> result = new ArrayList<>();
        for (AlarmType alarmType : AlarmType.values()) {
            result.add(alarmType.index);
        }
        return result;
    }
}
