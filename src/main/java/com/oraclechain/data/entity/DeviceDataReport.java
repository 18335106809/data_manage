package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
public class DeviceDataReport {

    private Integer id;
    private Long deviceId;
    private Long capsuleId;
    private String dataKey;
    private Double dataAvgValue;
    private Double dataMaxValue;
    private String reportTime;
    private String deviceType;
    private Long spaceId;

    private String startTime;
    private String endTime;

    public DeviceDataReport() {
    }

    public DeviceDataReport(Long deviceId, Long capsuleId, String dataKey, Double dataAvgValue, Double dataMaxValue, String reportTime, String deviceType, Long spaceId) {
        this.deviceId = deviceId;
        this.capsuleId = capsuleId;
        this.dataKey = dataKey;
        this.dataAvgValue = dataAvgValue;
        this.dataMaxValue = dataMaxValue;
        this.reportTime = reportTime;
        this.deviceType = deviceType;
        this.spaceId = spaceId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public Double getDataAvgValue() {
        return dataAvgValue;
    }

    public void setDataAvgValue(Double dataAvgValue) {
        this.dataAvgValue = dataAvgValue;
    }

    public Double getDataMaxValue() {
        return dataMaxValue;
    }

    public void setDataMaxValue(Double dataMaxValue) {
        this.dataMaxValue = dataMaxValue;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }
}
