package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/28
 */
public class DeviceStatus {
    private Integer startCount;
    private Integer badCount;
    private Integer AllCount;
    private String type;

    public Integer getStartCount() {
        return startCount;
    }

    public void setStartCount(Integer startCount) {
        this.startCount = startCount;
    }

    public Integer getBadCount() {
        return badCount;
    }

    public void setBadCount(Integer badCount) {
        this.badCount = badCount;
    }

    public Integer getAllCount() {
        return AllCount;
    }

    public void setAllCount(Integer allCount) {
        AllCount = allCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
