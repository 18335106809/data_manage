package com.oraclechain.data.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/10
 */
public enum AnalysisFileStatus {
    UNREVIEWED("未审核"), AUDITED("已审核"), DID_NOT_PASS("未通过");
    private String name;

    private AnalysisFileStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
