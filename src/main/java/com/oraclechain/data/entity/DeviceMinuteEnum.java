package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/27
 */
public enum DeviceMinuteEnum {
    TEMPERATURE("temperature", "atmospheric"), HUMIDITY("humidity", "atmospheric"), OXYGEN("oxygen", "oxygen"),
    METHANE("methane", "methane"), VERTICAL("vertical", "structural"), LEVEL("level", "gauge");

    private String paramName;
    private String deviceType;

    DeviceMinuteEnum(String paramName, String deviceType) {
        this.paramName = paramName;
        this.deviceType = deviceType;
    }

    public static String getDeviceType(String paramName) {
        for (DeviceMinuteEnum deviceMinuteEnum : DeviceMinuteEnum.values()) {
            if (deviceMinuteEnum.paramName.equals(paramName)) {
                return deviceMinuteEnum.deviceType;
            }
        }
        return null;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getParamName() {
        return paramName;
    }

    public String getDeviceType() {
        return deviceType;
    }
}
