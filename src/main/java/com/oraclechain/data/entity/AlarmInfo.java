package com.oraclechain.data.entity;

import com.oraclechain.data.entity.device.Device;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/3/4
 */
public class AlarmInfo implements Serializable, Comparable<AlarmInfo> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer level;
    private Long capsuleId;
    private Long deviceId;
    private Date alarmTime;
    private Integer type;
    private String status;
    private String reason;
    private String deviceType;
    private Integer dealId;
    private Long segmentId;
    private String alarmValue;

    private String hurt;
    private String accidentReason;

    private String capsuleName;
    private String segmentName;
    private String deviceName;
    private String typeName;
    private String dealName;
    private String alarmKey;

    private Device device;
    private Integer capsuleNumber;

    private AlarmConfDeal deal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Date getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public String getCapsuleName() {
        return capsuleName;
    }

    public void setCapsuleName(String capsuleName) {
        this.capsuleName = capsuleName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public AlarmConfDeal getDeal() {
        return deal;
    }

    public void setDeal(AlarmConfDeal deal) {
        this.deal = deal;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public Long getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(Long segmentId) {
        this.segmentId = segmentId;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getAlarmValue() {
        return alarmValue;
    }

    public void setAlarmValue(String alarmValue) {
        this.alarmValue = alarmValue;
    }

    public String getAccidentReason() {
        return accidentReason;
    }

    public void setAccidentReason(String accidentReason) {
        this.accidentReason = accidentReason;
    }

    public String getHurt() {
        return hurt;
    }

    public void setHurt(String hurt) {
        this.hurt = hurt;
    }

    public String getAlarmKey() {
        return alarmKey;
    }

    public void setAlarmKey(String alarmKey) {
        this.alarmKey = alarmKey;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Integer getCapsuleNumber() {
        return capsuleNumber;
    }

    public void setCapsuleNumber(Integer capsuleNumber) {
        this.capsuleNumber = capsuleNumber;
    }

    @Override
    public int compareTo(AlarmInfo alarmInfo) {
        return alarmInfo.id - this.id;
    }
}
