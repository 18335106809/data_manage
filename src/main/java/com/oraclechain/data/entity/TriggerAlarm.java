package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/12/21
 */
public class TriggerAlarm {
    private Boolean open;

    public TriggerAlarm(Boolean open) {
        this.open = open;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        open = open;
    }
}
