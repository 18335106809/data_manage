package com.oraclechain.data.schedule;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.RabbitMQ.TopicRabbitConfig;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.DeviceData;
import com.oraclechain.data.entity.DeviceDataReport;
import com.oraclechain.data.entity.DeviceDataResult;
import com.oraclechain.data.entity.device.Device;
import com.oraclechain.data.entity.device.DeviceDisplay;
import com.oraclechain.data.feign.ProviderClient;
import com.oraclechain.data.mongoDB.MongoTemplateUtil;
import com.oraclechain.data.redis.RedisKeyConfig;
import com.oraclechain.data.service.CommonService;
import com.oraclechain.data.service.DeviceAvgDataService;
import com.oraclechain.data.service.RedisGetDeviceService;
import com.oraclechain.data.util.ParameterUtil;
import com.oraclechain.data.util.StringToArrayUtil;
import com.oraclechain.data.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.DoubleToIntFunction;

/**
 * @author liuhualong
 * @Description: 设备健康状态定时监测
 * @date 2020/4/8
 */
@Component
@EnableScheduling
public class ScheduledTasks {

    static Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisGetDeviceService redisGetDeviceService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private DeviceAvgDataService deviceAvgDataService;

    @Autowired
    private CommonService commonService;

    static Random r = new Random();

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    @Value("${device.type.waterpump}")
    private String waterpump;

    @Value("${device.type.fan}")
    private String fan;

    @Value("${device.type.firefighting}")
    private String firefighting;

    @Value("${device.type.smoke}")
    private String smoke;

    @Value("${device.hall-current-sensor.power-consumption}")
    private String power_consumption;

    @Value("${device.type.hall-current-sensor}")
    private String hall;

    @Autowired
    private ProviderClient providerClient;

    @Value("${device.type.hall.AV}")
    private String av;

    @Value("${device.type.hall.BV}")
    private String bv;

    @Value("${device.type.hall.CV}")
    private String cv;

    @Value("${device.type.hall.AII}")
    private String aii;

    @Value("${device.type.hall.BII}")
    private String bii;

    @Value("${device.type.hall.CII}")
    private String cii;

    @Value("${device.type.hall.AI}")
    private String ai;

    @Value("${device.type.hall.BI}")
    private String bi;

    @Value("${device.type.hall.CI}")
    private String ci;

    @Value("${device.type.hall.AE}")
    private String ae;

    @Value("${device.type.hall.RE}")
    private String re;

    @Value("${device.type.hall.AP}")
    private String ap;

    @Value("${device.type.hall.BP}")
    private String bp;

    @Value("${device.type.hall.CP}")
    private String cp;

    @Value("${device.type.hall.ALLP}")
    private String allp;

    @Value("${device.type.hall.ALLE}")
    private String alle;

    @Value("${device.type.hall.PT}")
    private String pt;

    @Value("${device.type.hall.CT}")
    private String ct;

//    @Scheduled(cron = "0/20 * * * * *")
    public void mockData() {
        try {
            logger.info("执行模拟生产数据定时任务>>>>>>>>>>>>>>>>>");
            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            Device device;
            for (String key : keys) {
                device = redisGetDeviceService.getDataByKey(key, Device.class);
                if (device != null && StringUtils.isNotBlank(device.getType())) {
                    DeviceData deviceData = new DeviceData();
                    deviceData.setId(device.getId());
                    JSONObject jsonObject = new JSONObject();
                    List<String> list = StringToArrayUtil.toArray(device.getParameters());
                    DeviceCommon deviceCommon = new DeviceCommon(device);
                    JSONObject thresholdJson = deviceCommon.getThresholdJson();
                    for (String param : list) {
                        if (thresholdJson != null && thresholdJson.containsKey(param)) {
                            List<Double> doubleList = JSONArray.parseArray(thresholdJson.get(param).toString(), Double.class);
                            if (doubleList.size() > 1) {
                                Double first = doubleList.get(0);
                                Double second = doubleList.get(1);
                                Double value = (first + second) / 2;
                                jsonObject.put(param, Double.valueOf(decimalFormat.format(value)));
                            }
                        } else {
                            jsonObject.put(param, 0);
                        }
                    }
                    deviceData.setParam(jsonObject);
                    this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    // 每个小时的第10分钟执行一次，设置电表的各个值
    @Scheduled(cron = "30 */10 * * * *")
    public void setHallData() {
        Map<String, Object> param = new HashMap<>();
        param.put("type", hall);
        JSONObject jsonObject = JSONObject.parseObject(providerClient.getDeviceByType(param));
        if (Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
            List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
            for (JSONObject deviceJson : deviceJSONList) {
                Device device = JSONObject.parseObject(deviceJson.toJSONString(), Device.class);
                Float avValue = getValue(device.getId(), av);
                Float bvValue = getValue(device.getId(), bv);
                Float cvValue = getValue(device.getId(), cv);
                Float aiiValue = getValue(device.getId(), aii);
                Float biiValue = getValue(device.getId(), bii);
                Float ciiValue = getValue(device.getId(), cii);
                Float ctValue = getValue(device.getId(), ct);
                Float aiValue = setI(device.getId(), aiiValue, ai, ctValue);
                Float biValue = setI(device.getId(), biiValue, bi, ctValue);
                Float ciValue = setI(device.getId(), ciiValue, ci, ctValue);
                Float aeValue = getValue(device.getId(), ae);
                Float reValue = getValue(device.getId(), re);
                Float apValue = getValue(avValue, aiValue, device.getId(), ap);
                Float bpValue = getValue(bvValue, biValue, device.getId(), bp);
                Float cpValue = getValue(cvValue, ciValue, device.getId(), cp);
                Float allPValue = (apValue == null ? 0 : apValue) + (bpValue == null ? 0 : bpValue) + (cpValue == null ? 0 : cpValue);
                Float allEValue = (aeValue == null ? 0 : aeValue) + (reValue == null ? 0 : reValue);
                if (allPValue != null && allPValue > 0) {
                    redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), allp, allPValue.toString());
                }
                if (allEValue != null && allEValue > 0) {
                    redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), alle, allEValue.toString());
                }
                DeviceCommon deviceCommon = new DeviceCommon(device);
                JSONObject jsonObject1 = new JSONObject();
                for (String data : deviceCommon.getParams()) {
                    Object realValue = redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + device.getId(), data);
                    if (realValue != null) {
                        jsonObject1.put(data, Double.valueOf(realValue.toString()));
                    }
                }
                device.setValue(jsonObject1);
                device.setGatherTime(new Date().getTime());
                this.mongoTemplateUtil.save(DeviceDisplay.toDisplay(device), MongoTemplateUtil.COLLECTION_DEVICE);
            }
        }
    }

    public Float setI(Long deviceId, Float ivalue, String name, Float ctValue) {
        Float value = 0F;
        if (ivalue != null && ctValue != null) {
            value = ivalue * ctValue;
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, name, value.toString());
        }
        return value;
    }

    public Float getValue(Float v, Float i, Long deviceId, String dataKey) {
        Float p = null;
        if (v != null  && i != null) {
            p = v * i;
            redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey, p.toString());
        }
        return p;
    }

    public Float getValue(Long deviceId, String dataKey) {
        if (redisTemplate.opsForHash().hasKey(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey)) {
            return Float.valueOf(redisTemplate.opsForHash().get(RedisKeyConfig.DEVICE_REALTIME_DATA + deviceId, dataKey).toString());
        } else {
            return null;
        }
    }

    // 根据设备、类型、求每日平均值，每日凌晨一点求前一天的
    @Scheduled(cron = "0 0 1 * * *")
    public void getDataAvgByDay() {
        logger.info("定时任务，计算设备的每日平均值");
        try {
            List<Device> deviceList = commonService.getDeviceReportList();
            Long startTime = TimeUtil.beforeDayStart();
            Long endTime = TimeUtil.beforeDayEnd();
            for (Device device : deviceList) {
                DeviceDisplay deviceDisplay = new DeviceDisplay();
                deviceDisplay.setDeviceId(device.getId());
                deviceDisplay.setStartTime(startTime);
                deviceDisplay.setEndTime(endTime);
                Long spaceId = commonService.getSpaceId(device.getCapsule());
                for (String dataKey : StringToArrayUtil.toArray(device.getParameters())) {
                    DeviceDataResult deviceDataResult = mongoTemplateUtil.getAvgAndMax(device.getId(), startTime, endTime, dataKey);
                    Double avgValue = deviceDataResult.getAvg();
                    Double maxValue = deviceDataResult.getMax();
                    Double oldValue = null;
                    if (power_consumption.contains(dataKey)) {
                        oldValue = deviceAvgDataService.getMaxData(device.getId(), TimeUtil.beforeDay(), dataKey);
                        if (oldValue == null) {
                            if (maxValue != null) {
                                avgValue = Double.valueOf(decimalFormat.format(maxValue));
                                maxValue = Double.valueOf(decimalFormat.format(maxValue));
                            } else {
                                avgValue = 0.0;
                                maxValue = 0.0;
                            }
                        } else {
                            if (maxValue != null) {
                                avgValue = Double.valueOf(decimalFormat.format(maxValue - oldValue));
                                maxValue = Double.valueOf(decimalFormat.format(maxValue - oldValue));
                            } else {
                                avgValue = oldValue;
                                maxValue = oldValue;
                            }
                        }
                    } else {
                        if (avgValue != null) {
                            avgValue = Double.valueOf(decimalFormat.format(avgValue));
                        } else {
                            avgValue = 0.0;
                        }
                        if (maxValue != null) {
                            maxValue = Double.valueOf(decimalFormat.format(maxValue));
                        } else {
                            maxValue = 0.0;
                        }
                    }
                    DeviceDataReport deviceAvgData = new DeviceDataReport(device.getId(), device.getCapsule(), dataKey, avgValue, maxValue, TimeUtil.beforeDay(), device.getType().trim(), spaceId);
                    deviceAvgDataService.insertData(deviceAvgData);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getDataAvgByDay定时任务出错");
        }
    }

    public static void main(String[] args) {
        Double first = 30.3;
        Double second = 64.2;
        for (int i = 1; i <= 300; i++) {
            Double value = first + Math.random() * second % (second - first);
            if (value < first || value > second) {
                System.out.println("-------------------" + value);
            } else {
                System.out.println(value);
            }
        }
    }

}
