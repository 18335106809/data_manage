package com.oraclechain.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/6
 */
@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
@EnableSwagger2
@EnableAsync
@EnableResourceServer
@EnableFeignClients
public class DataManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataManageApplication.class);
    }
}
