package com.oraclechain.data.test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/27
 */
public class ThreadTest extends Thread {

    static Map<String, Thread> threadMap = new ConcurrentHashMap<>();

    @Override
    public void run() {
        int i = 1;
        while (true) {

            if(Thread.currentThread().isInterrupted()){
                break;
            }
            System.out.println(i);
            i++;
            try {
                Thread.currentThread().sleep(1000l);
            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public static void main(String[] args) {
        try {
            ThreadTest threadTest = new ThreadTest();
            threadTest.start();
            threadMap.put("qwe", threadTest);
            threadTest.sleep(10000l);
            threadMap.get("qwe").interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
