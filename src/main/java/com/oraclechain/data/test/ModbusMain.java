package com.oraclechain.data.test;

import com.oraclechain.data.RabbitMQ.ModbusData;
import io.netty.buffer.ByteBuf;
import org.springframework.data.redis.util.ByteUtils;

import java.util.BitSet;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/5/22
 */
public class ModbusMain {
    public static void main(String[] args) throws Exception {
        ModbusData modbusData = new ModbusData();
        modbusData.initModbusMaster();
//        int i=1;
//        int j =0;
        System.out.println(modbusData.readHoldingRegisters(510  , 4, 1).doubleValue());
//        modbusData.writeMultipleRegisters(510, 1, float2byte(345.2f), 1);
//        ByteBuf result = modbusData.readCoils(0, 8,0);
//        BitSet bitSet = byteArray2BitSet(bs);
//        System.out.println(bitSet);
//        System.out.println(bitSet.length());
//        for (int i = 0; i < bitSet.length(); i++) {
//            System.out.println(bitSet.get(i));
//        }
//        for (i=1;i<32;i++){
//            for (j=0;j<10;j++) {
//                System.out.println(modbusData.readHoldingRegisters(200 * i, 2 * i, j).intValue());
//                System.out.println(modbusData.readHoldingRegisters(202 * i, 2 * i, j).intValue());
//                System.out.println(modbusData.readHoldingRegisters(200 * i, 2 * i, j).doubleValue());
//                System.out.println(modbusData.readHoldingRegisters(202 * i, 2 * i, j).doubleValue());
//                System.out.println(modbusData.readInputRegisters(200 * i, 2 * i, j).doubleValue());
//                System.out.println(modbusData.readInputRegisters(202 * i, 2 * i, j).doubleValue());
//                System.out.println(modbusData.readInputRegisters(200 * i, 2 * i, j).intValue());
//                System.out.println(modbusData.readInputRegisters(202 * i, 2 * i, j).intValue());
//
//            }
//        }

//        modbusData.writeMultipleCoils(500, 1, float2byte(1f), 1);
//        for (int n = 0; n < 100; n++) {
//            modbusData.writeMultipleCoils(500, 1, float2byte(1f), 1);
//            System.out.println(modbusData.readCoils(500, 1, 0));
//        }
//        System.out.println(modbusData.readHoldingRegisters(500, 4, 1));
//        System.out.println(modbusData.readHoldingRegisters(504, 4, 1));
//        System.out.println(modbusData.readHoldingRegisters(508, 4, 1));
//        System.out.println(modbusData.readHoldingRegisters(510, 4, 1));
//        for (int n = 0; n < 49 ; n++) {
//            System.out.println(modbusData.readHoldingRegisters(510, 4, 1).floatValue());
//            System.out.println(modbusData.readHoldingRegisters(514, 4, 0).floatValue());
//            System.out.println(modbusData.readHoldingRegisters(518, 4, 0).floatValue());
//            System.out.println(modbusData.readHoldingRegisters(522, 4, 0).floatValue());
//            System.out.println(modbusData.readHoldingRegisters(526, 4, 0).floatValue());
//            System.out.println(modbusData.readHoldingRegisters(530, 4, 0).floatValue());
//            System.out.println(modbusData.readCoils(500, 1, 0));
//            System.out.println(modbusData.readCoils(500, 1, 1));
//            System.out.println(modbusData.readCoils(500, 1, 2));
//            System.out.println(modbusData.readCoils(500, 1, 3));
//            System.out.println(modbusData.readCoils(500, 1, 4));
//            System.out.println(modbusData.readCoils(500, 1, 5));
//            System.out.println(modbusData.readCoils(500, 1, 6));
//            System.out.println(modbusData.readCoils(500, 1, 7));
//
//            System.out.println(modbusData.readCoils(502, 1, 0));
//            System.out.println(modbusData.readCoils(502, 1, 1));
//            System.out.println();
//        }



    }

    public static byte[] float2byte(float f) {
        int fbit = Float.floatToIntBits(f);
        byte[] b = new byte[4];
        for (int i = 0; i<4; i++){
            b[i] = (byte) (fbit >> (24 - i * 8));
        }
        int len = b.length;
        byte[] dest = new byte[len];
        System.arraycopy(b, 0, dest, 0, len);
        byte temp;
        for (int  i = 0; i < len / 2; ++i) {
            temp = dest[i];
            dest[i] = dest[len - i - 1];
            dest[len - i - 1] = temp;
        }
        return dest;
    }

    public static BitSet byteArray2BitSet(byte[] bytes) {
        BitSet bitSet = new BitSet(bytes.length * 8);
        int index = 0;
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 7; j >= 0; j--) {
                bitSet.set(index++, (bytes[i] & (1 << j)) >> j == 1 ? true
                        : false);
            }
        }
        return bitSet;
    }

}
