package com.oraclechain.data.test;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oraclechain.data.entity.DeviceCommon;
import com.oraclechain.data.entity.MockAccident;
import com.oraclechain.data.entity.device.Device;
import jdk.nashorn.internal.runtime.arrays.NumericElements;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/17
 */
public class TestMain {

    private String id;
    private Long sleep;
    private Integer time;
    private Boolean isMock = false;
    private Integer count;

    public TestMain(String id, Long sleep, Integer time) {
        this.id = id;
        this.sleep = sleep;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getSleep() {
        return sleep;
    }

    public void setSleep(Long sleep) {
        this.sleep = sleep;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Boolean getMock() {
        return isMock;
    }

    public void setMock(Boolean mock) {
        isMock = mock;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public static void main(String[] args) {
        Device device = new Device();
        device.setParameters("[temperature,humidity]");
        device.setThreshold("{temperature:[11,28],humidity:[16,18]}");
        DeviceCommon deviceCommon = new DeviceCommon(device);
        System.out.println(deviceCommon.getParams());
        System.out.println(deviceCommon.getThresholdJson());
        List<Integer> list = JSONObject.parseObject(deviceCommon.getThresholdJson().get("humidity").toString(), List.class);
        System.out.println(list);


        List<Double> thresholdList = JSONObject.parseObject(deviceCommon.getThresholdJson().get("temperature").toString(), List.class);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Double> list1 = objectMapper.convertValue(deviceCommon.getThresholdJson().get("temperature"), new TypeReference<List<Double>>(){ });
        Double value = 1d;
        System.out.println(value > list1.get(0));
    }
}
