package com.oraclechain.data.feign;

import com.oraclechain.data.entity.device.Device;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
@FeignClient("device-center")
public interface ProviderClient {

    @RequestMapping(value = "/device/listByCapsule", method = RequestMethod.POST)
    String getDeviceByCapsuleId(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/device/listByType", method = RequestMethod.POST)
    String getDeviceByType(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/SpaceManage/list", method = RequestMethod.POST)
    String getSpaceManage();

    @RequestMapping(value = "/device/querySpaceDeviceIds", method = RequestMethod.GET)
    String querySpaceDeviceIds(@RequestParam("spaceId") String spaceId);

    // TODO: 2020/7/30 需要设备管理端暴露一个判断舱下是否有某类设备的接口
    // 参数 spaceId、type
    @RequestMapping(value = "/device/querySpaceDeviceByType", method = RequestMethod.GET)
    String getDeviceBySpaceType(@RequestParam("spaceId") String spaceId, @RequestParam("type") String type);

    // 查询出设备的好坏
    @RequestMapping(value = "/device/listByStatus", method = RequestMethod.POST)
    String getDeviceStatus();

    // 查询设备的单位
    @RequestMapping(value = "/api-assets/Paramter/findByName", method = RequestMethod.GET)
    String getDeviceUnit(@RequestParam("name") String name);

    // 查询摄像头的设备
    @RequestMapping(value = "/VideoInfo/listByType", method = RequestMethod.POST)
    String getVideoDeviceByCapsule(@RequestBody Map<String, Object> map);

    // 更新设备的内容
    @RequestMapping(value = "/device/saveOrUpdate", method = RequestMethod.POST)
    String updateDevice(@RequestBody Device device);
}
